<?php
	$this->Html->addCrumb('Grupos',
		array(
			'action' => 'index',
			'controller' => 'groups',
			'admin' => true
			)
		);

	$this->set('activeMenu', 'groups');

	$this->Html->addCrumb('Ver', null);

	$this->Html->addCrumb($group['Group']['name'],
		array(
			'action' => 'view',
			'controller' => 'groups',
			$group['Group']['id'],
			'admin' => true
			)
		);
?>
<div class="row-fluid">
	<div class="span3 offset9">
		<div class="actions">
			<?php echo $this->Html->link('<i class="icon-edit"></i> Editar',
				array(
					'action' => 'edit',
					'controller' => 'groups',
					$group['Group']['id'],
					'admin' => true
					),
				array(
					'class' => 'btn grey',
					'escape' => false)
				); 
			?>
			<?php 
				echo $this->Form->postLink('<i class="icon-trash"></i> Eliminar',
					array('action' => 'delete', $group['Group']['id']),
					array('class'=>'btn red', 'escape'=>false),
					__('Confirma que desea eliminar al grupo %s?', $group['Group']['name'])); 
			?>
			<?php echo $this->Html->link('<i class="icon-group"></i> Grupos',
				array(
					'action' => 'index',
					'controller' => 'groups',
					'admin' => true
					),
				array(
					'class' => 'btn blue',
					'escape' => false)
				); 
			?>
		</div>
	</div>
</div>
<div class="profile-classic row-fluid">
	<ul class="unstyled span12">
		<li><span>Nombre: </span><?php echo $group['Group']['name'] ?></li>
		<li><span>Creado: </span> <?php echo $group['Group']['created'] ?></li>
		<li><span>Modificado: </span> <?php echo $group['Group']['modified'] ?></li>
	</ul>
</div>