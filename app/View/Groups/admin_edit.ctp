<?php
	echo $this->Html->css(array(
		'admin/gritter/css/jquery.gritter',
		'admin/jquery-tags-input/jquery.tagsinput',
		'admin/data-tables/DT_bootstrap'
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		'admin/jquery-tags-input/jquery.tagsinput.min',
		'admin/jquery-validation/dist/jquery.validate.min',
		'admin/jquery-validation/dist/additional-methods.min'
		), array('inline' => false)
	);

	$group = $this->request->data;

	$this->set('activeMenu', 'groups');

	$this->Html->addCrumb('Grupos',
		array(
			'action' => 'index',
			'controller' => 'groups',
			'admin' => true
			)
		);

	$this->Html->addCrumb('Editar', null);

	$this->Html->addCrumb($group['Group']['name'],
		array(
			'action' => 'edit',
			'controller' => 'groups',
			$group['Group']['id'],
			'admin' => true
			)
		);
?>
<div class="portlet box blue">
	<div class="portlet-title">
		<h4><i class="icon-group"></i>Editar Grupo</h4>
		<div class="actions">
			<?php echo $this->Html->link('<i class="icon-group"></i> Grupos</a>',
				array(
					'action' => 'index',
					'controller' => 'groups',
					'admin' => true
					),
				array(
					'class' => 'btn blue',
					'escape' => false)
				);
			?>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('Group',
			array(
				'inputDefaults' => array(
					'div' => array('class' => 'control-group'),
					'class' => array('class' => 'm-wrap span12')
					),
				'class' => 'form-horizontal form-row-seperated',
				'novalidate' => true
				)
			); 
		?>
			<div class="alert alert-error hide">
				<button class="close" data-dismiss="alert"></button>
				Existen errores en el formulario. Favor de revisar los datos.
			</div>
			<div class="alert alert-success hide">
				<button class="close" data-dismiss="alert"></button>
				Los datos son válidos!
			</div>
		<?php
			$div = $this->Html->tag('div', null, array('class'=>'controls input-icon'));
			echo $this->Form->input('name', 
				array(
					'between' => $div,
					'placeholder'=>'Nombre del grupo',
					'data-required'=>1,
					'label' => array('text'=>'Nombre<span class="required">*</span>', 'class'=>'control-label')
					)
				);
			echo '</div>';

			echo '<div class="form-actions">';
			echo $this->Form->button('<i class="icon-ok"></i> Guardar', 
				array('type' => 'submit', 'class' => 'btn green')
				);
			echo '&nbsp;';

			$cancelOptions = array('action' => 'index', 'controller' => 'groups', 'admin' => true);
			echo $this->Form->button('Cancelar', 
				array(
					'type' => 'button',
					'class' => 'btn red',
					'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
					)
				);
			echo '</div>';
		?>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->  
	</div>
</div>