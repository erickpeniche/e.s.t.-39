<?php
	echo $this->Html->css(array(
		'admin/fancybox/source/jquery.fancybox',
		'admin/data-tables/DT_bootstrap',
		), null, array('inline' => false)
	);

	$this->set('activeMenu', 'groups');

	$this->Html->addCrumb('Grupos',
		array(
			'action' => 'index',
			'controller' => 'groups',
			'admin' => true
			)
		);
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
	<div class="span12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box light-grey">
			<div class="portlet-title">
				<h4><i class="icon-group"></i>Grupos</h4>
				<div class="actions">
					<?php echo $this->Html->link('<i class="icon-plus"></i> Nuevo</a>',
						array(
							'action' => 'add',
							'controller' => 'groups'
							),
						array(
							'class' => 'btn green',
							'escape' => false)
						); 
					?>
					<?php echo $this->Html->link('<i class="icon-print"></i> Imprimir</a>',
						array(
							'action' => null,
							'controller' => null,
							'admin' => true
							),
						array(
							'class' => 'btn yellow',
							'escape' => false)
						); 
					?>
					<div class="btn-group">
						<button class="btn dropdown-toggle" data-toggle="dropdown"><i class="icon-cog"></i> Herramientas <i class="icon-angle-down"></i>
						</button>
						<ul class="dropdown-menu">
							<li><a href="#">Guardar como PDF</a></li>
							<li><a href="#">Exportar a Excel</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover" id="sample_3">
					<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('name', 'Nombre'); ?></th>
							<th><?php echo $this->Paginator->sort('created', 'Creado'); ?></th>
							<th><?php echo $this->Paginator->sort('modified', 'Modificado'); ?></th>
							<th class="actions"><?php echo __('Acciones'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($groups as $group): ?>
						<tr class="odd gradeX">
							<td><?php echo h($group['Group']['name']); ?>&nbsp;</td>
							<td><?php echo h($group['Group']['created']); ?>&nbsp;</td>
							<td><?php echo h($group['Group']['modified']); ?>&nbsp;</td>
							<td class="actions">
								<?php echo $this->Html->link('<i class="icon-info-sign"></i> Ver', array('action' => 'view', $group['Group']['id']), array('class'=>'btn mini blue', 'escape'=>false)); ?>
								<?php echo $this->Html->link('<i class="icon-edit"></i> Editar', array('action' => 'edit', $group['Group']['id']), array('class'=>'btn mini grey', 'escape'=>false)); ?>
								<?php echo $this->Form->postLink('<i class="icon-trash"></i> Eliminar', array('action' => 'delete', $group['Group']['id']), array('class'=>'btn mini red', 'escape'=>false), __('Confirma que desea eliminar al grupo %s?', $group['Group']['name'])); ?>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT-->
