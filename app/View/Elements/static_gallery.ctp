<div class="row-fluid">
	<div class="span12">
		<div class="portlet box blue">
			<div class="portlet-title">
				<h4><i class="icon-picture"></i> <?php echo $dataTitle; ?></h4>
			</div>
			<div class="portlet-body">
				<div class="scroller" data-height="600px" data-always-visible="1" data-rail-visible="1">
					<div class="row-fluid" id='image-gallery'>
					<?php $i = 0;?>
					<?php for ($j=0; $j<$totalImages; $j++): ?>
						<?php $i++ ?>
						<?php if ($i%4 == 1) : ?>
						<div class="row-fluid">
						<?php endif ?>
							<div class="span3">
								<div class="item">
									<a class="fancybox-button" data-rel="fancybox-button" href="<?php echo '/img/images/'.$imageFolder.'/'.$j.'.jpg'; ?>">
										<div class="zoom">
											<?php echo $this->Html->image('images/'.$imageFolder.'/'.$j.'.jpg'); ?>
											<div class="zoom-icon"></div>
										</div>
									</a>
								</div>
							</div>
						<?php if ($i%4 == 0 || $totalImages == $i) : ?>
						</div>
						<?php endif ?>
						<?php endfor; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
