<?php
  /**
   * Dynamic navigation highlighting
   * Each page sets a variable $this->set('name_of_page', 'active');
   * If the variable is set, then we are on that page, so the active class is set for that link
   */
  $home = isset($home) ? $home : '';
  $about_us = isset($about_us) ? $about_us : '';
  $plans_programs = isset($plans_programs) ? $plans_programs : '';
  $projects_plans = isset($projects_plans) ? $projects_plans : '';
  $events_menu = isset($events_menu) ? $events_menu : '';
  $contact = isset($contact) ? $contact : '';
?>
  <nav <?php if($home) echo 'style="margin-bottom:7px;"'?>>
    <ul class="sf-menu">
      <li class="<?php echo $home;?>">
        <?php
          echo $this->Html->link(
            $this->Html->tag('span', 'Inicio'),
            array(
              'controller' => 'pages',
              'action' => 'home'),
            array('escape' => false)
            );
        ?>
      </li>
      <li class="<?php echo $about_us;?>">
        <?php
          echo $this->Html->link(
            $this->Html->tag('span', 'Nosotros'),
            array(
              'controller' => 'pages',
              'action' => 'about_us'),
            array('escape' => false)
          );
        ?>
        <ul>
          <li><?php echo $this->Html->link('Misión y Visión', array('controller'=>'pages', 'action'=>'vision')); ?></li>
          <li><a href="#">Plantel</a>
            <ul>
              <li><?php echo $this->Html->link('Baños', array('controller'=>'pages', 'action'=>'bathrooms')); ?></li>
              <li><?php echo $this->Html->link('Canchas deportivas', array('controller'=>'pages', 'action'=>'sports_courts')); ?></li>
              <li><?php echo $this->Html->link('Conservación e industrialización de alimentos', array('controller'=>'pages', 'action'=>'food_conservation')); ?></li>
              <li><?php echo $this->Html->link('Construyendo jardín temático', array('controller'=>'pages', 'action'=>'theme_garden')); ?></li>
              <li><?php echo $this->Html->link('Cooperativa escolar', array('controller'=>'pages', 'action'=>'school_shop')); ?></li>
              <li><?php echo $this->Html->link('Entrada', array('controller'=>'pages', 'action'=>'entrance')); ?></li>
              <li><?php echo $this->Html->link('Escudo escolar', array('controller'=>'pages', 'action'=>'school_emblem')); ?></li>
              <li><?php echo $this->Html->link('Jardines entrada', array('controller'=>'pages', 'action'=>'entry_garden')); ?></li>
              <li><?php echo $this->Html->link('Jardines externos', array('controller'=>'pages', 'action'=>'outside_garden')); ?></li>
              <li><?php echo $this->Html->link('Jardines y salones', array('controller'=>'pages', 'action'=>'classrooms')); ?></li>
              <li><?php echo $this->Html->link('Personal de la escuela', array('controller'=>'pages', 'action'=>'school_staff')); ?></li>
              <li><?php echo $this->Html->link('Plaza cívica y dirección', array('controller'=>'pages', 'action'=>'civic_plaza')); ?></li>
              <li><?php echo $this->Html->link('Prefectura y trabajo social', array('controller'=>'pages', 'action'=>'social_work')); ?></li>
            </ul>
          </li>
          <li><?php echo $this->Html->link('Servicio Social', array('controller'=>'pages', 'action'=>'social_service')); ?></li>
          <li>
            <?php echo $this->Html->link('Tecnologías', array('controller'=>'pages', 'action'=>'technologies')); ?>
            <ul>
              <li><?php echo $this->Html->link('Agricultura', array('controller'=>'pages', 'action'=>'agriculture')); ?></li>
            </ul>
          </li>
          <li>
            <?php echo $this->Html->link('Sociedad de Alumnos', array('controller'=>'pages', 'action'=>'student_society')); ?>
          </li>
        </ul>
      </li>
      <li class="<?php echo $plans_programs;?>">
        <?php
          echo $this->Html->link(
            $this->Html->tag('span', 'Planes y Programas'),
            array(
              'controller' => 'documents',
              'action' => 'index'),
            array('escape' => false)
          );
        ?>
      </li>
      <li class="<?php echo $projects_plans;?>">
        <?php
          echo $this->Html->link(
            $this->Html->tag('span', 'Proyectos y Mejoras'),
            '#',
            array('escape' => false)
          );
        ?>
        <ul>
          <li><a href="#">Proyectos</a>
            <ul>
              <li><?php echo $this->Html->link('Refranes/Valores', array('controller'=>'pages', 'action'=>'proverbs')); ?></li>
            </ul>
          </li>
          <li><a href="#">Planes de mejora</a>
            <ul>
              <li><?php echo $this->Html->link('Lectura y comprensión', array('controller'=>'pages', 'action'=>'lecture')); ?></li>
              <li><?php echo $this->Html->link('Sabucanes', array('controller'=>'pages', 'action'=>'travel_bags')); ?></li>
            </ul>
          </li>
        </ul>
      </li>
      <li class="<?php echo $events_menu;?>">
        <?php
          echo $this->Html->link(
            $this->Html->tag('span', 'Eventos'),
            array(
              'controller' => 'events',
              'action' => 'index'),
            array('escape' => false)
          );
        ?>
      </li>
      <li class="<?php echo $contact;?>">
        <?php
          echo $this->Html->link(
            $this->Html->tag('span', 'Contacto'),
            array(
              'controller' => 'pages',
              'action' => 'contact'),
            array('escape' => false)
          );
        ?>
      </li>
    </ul>
    <div class="clear"></div>
  </nav>