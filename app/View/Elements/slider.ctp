<script type="text/javascript">
  $(window).load(function() {
    $('.flexslider').flexslider({
      animation: "slide"
    });
   });
</script>
<div class="slider_box">
  <div class="flexslider">
    <ul class="slides">
    <?php foreach ($mainImages as $mainImage): ?>
      <li>
        <?php echo $this->Html->image('images/main_img_'.$mainImage['MainImage']['image']);?>
        <div class="banner">
          <div class="inner"><?php echo $mainImage['MainImage']['name']; ?></div>
        </div>
      </li>
    <?php endforeach; ?>
    </ul>
  </div>
</div>