<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
	<div class="span12">
		<!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">
			<?php echo isset($viewTitle) ? $viewTitle : 'Blank Page'; ?>&nbsp;<small><?php echo isset($viewSubTitle) ? $viewSubTitle : ''; ?></small>
		</h3>
		<ul class="breadcrumb">
			<?php $crumbs = $this->Html->getCrumbs2(
				array(
					'text' => 'Inicio',
					'url' => array('controller' => 'pages', 'action' => 'home', 'admin' => true)
					)
				);
			?>
			<?php foreach ($crumbs as $key => $crumb):?>
			<li>
				<?php
					echo $key==0 ? '<i class="icon-home"></i>' : '';
					echo $crumb;
					echo $key < count($crumbs)-1 ? '<i class="icon-angle-right"></i>' : '';
				?>
			</li>
			<?php endforeach; ?>
			<li class="pull-right no-text-shadow">
				<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" style="display: block;">
					<i class="icon-calendar"></i>
					<span><?php echo utf8_encode(strftime('%a %#d de %B del %Y, %H:%M', time())); ?></span>
				</div>
			</li>
		</ul>
		<!-- END PAGE TITLE & BREADCRUMB-->
	</div>
</div>
<!-- END PAGE HEADER-->