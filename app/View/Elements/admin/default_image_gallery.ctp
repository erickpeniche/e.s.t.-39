<div class="row-fluid">
	<div class="span12">
		<div class="portlet box blue">
			<div class="portlet-title">
				<h4><i class="icon-picture"></i> Galería de imágenes</h4>
				<div class="tools">
					<a href="javascript:;" class="collapse"></a>
				</div>
				<div class="actions">
					<?php echo $this->Html->link('<i class="icon-plus"></i> Añadir</a>',
						array(
							'action' => 'add',
							'controller' => 'default_images',
							$this->params['controller'],
							$modelId,
							Format::clean($dataTitle)
							),
						array(
							'class' => 'btn green',
							'escape' => false)
						);
					?>
				</div>
			</div>
			<div class="portlet-body">
				<div class="scroller" data-height="440px" data-always-visible="1" data-rail-visible="1">
					<div class="row-fluid" id='image-gallery'>
						<?php if(!isset($data) || empty($data)): ?>
						<div class="alert alert-block alert-info fade in">
							<button type="button" class="close" data-dismiss="alert"></button>
							<h4 class="alert-heading">No hay imágenes!</h4>
							<p>
								Aún no se han agregado imágenes. Haz click en el botón 'Añadir' para agregar una nueva imagen.
							</p>
						</div>
						<?php else: ?>
							<?php $i = 0; ?>
							<?php foreach ($data as $image): ?>
								<?php $i++ ?>

								<?php if ($i%4 == 1) : ?>
								<div class="row-fluid">
								<?php endif ?>
									<div class="span3">
										<div class="item">
											<a class="fancybox-button" data-rel="fancybox-button" title="<?php echo (isset($image['description']) && !empty($image['description'])) ? $image['description'] : ''; ?>" href="<?php echo '/img/images/'.$imagePrefix.'_extra_img_'.$image['file']; ?>">
												<div class="zoom">
													<?php echo $this->Html->image('images/'.$imagePrefix.'_extra_img_'.$image['file']); ?>
													<div class="zoom-icon"></div>
												</div>
											</a>
											<div class="details">
												<?php $link = !empty($image['link']) ? $image['link'] : '';?>
												<?php echo !empty($link) ? $this->Html->link('<i class="icon-link"></i>', $link, array('class'=>'icon', 'escape'=>false)) : '' ?>
												<?php
													echo $this->Form->postLink(
														'<i class="icon-remove"></i>',
														array(
															'action' => 'delete',
															'controller' => 'default_images',
															$image['id'],
															'admin' => true
															),
														array(
															'class' => 'icon',
															'escape' => false,
															'confirm' => 'Estas seguro de borrar esta imagen?',
															'data' => array('controller' => $this->params['controller'], $modelId)
															)
														);
												?>
											</div>
										</div>
									</div>
								<?php if ($i%4 == 0 || sizeof($data) == $i) : ?>
								</div>
								<?php endif ?>
								<?php endforeach; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>