<?php
	$activeMenus = array(
		'home' => array(
			'active' => $activeMenu == 'home' ? 'active' : '',
			'selected' => $activeMenu == 'home' ? '<span class="selected"></span>' : ''
			),
		'mainImages' => array(
			'active' => $activeMenu == 'mainImages' ? 'active' : '',
			'selected' => $activeMenu == 'mainImages' ? '<span class="selected"></span>' : ''
			),
		'news' => array(
			'active' => $activeMenu == 'news' ? 'active' : '',
			'selected' => $activeMenu == 'news' ? '<span class="selected"></span>' : ''
			),
		'defaultImages' => array(
			'active' => $activeMenu == 'defaultImages' ? 'active' : '',
			'selected' => $activeMenu == 'defaultImages' ? '<span class="selected"></span>' : ''
			),
		'events' => array(
			'active' => $activeMenu == 'events' ? 'active' : '',
			'selected' => $activeMenu == 'events' ? '<span class="selected"></span>' : ''
			),
		'users' => array(
			'active' => $activeMenu == 'users' ? 'active' : '',
			'selected' => $activeMenu == 'users' ? '<span class="selected"></span>' : ''
			),
		'configurations' => array(
			'active' => $activeMenu == 'configurations' ? 'active' : '',
			'selected' => $activeMenu == 'configurations' ? '<span class="selected"></span>' : ''
			),
		'documents' => array(
			'active' => $activeMenu == 'documents' ? 'active' : '',
			'selected' => $activeMenu == 'documents' ? '<span class="selected"></span>' : ''
			),
	);
 ?>
<!-- BEGIN SIDEBAR MENU -->
<ul>
	<li>
		<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
		<div class="sidebar-toggler hidden-phone"></div>
		<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
	</li>
	<li>
		&nbsp;
		<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
		<!-- <form class="sidebar-search">
			<div class="input-box">
				<a href="javascript:;" class="remove"></a>
				<input type="text" placeholder="Buscar..." />
				<input type="button" class="submit" value=" " />
			</div>
		</form> -->
		<!-- END RESPONSIVE QUICK SEARCH FORM -->
	</li>
	<li class="start <?php echo $activeMenus['home']['active']; ?>">
		<?php
			echo $this->Html->link(
				'<i class="icon-home"></i><span class="title">Inicio</span>'.$activeMenus['home']['selected'],
				array(
					'controller' => 'pages',
					'action' => 'home',
					'admin' => true
					),
				array(
					'escape' => false
					)
				);
		?>
	</li>
	<li class="<?php echo $activeMenus['mainImages']['active']; ?>">
		<?php
			echo $this->Html->link(
				'<i class="icon-picture"></i><span class="title">Imágenes principales</span>'.$activeMenus['mainImages']['selected'],
				array(
					'action' => 'index',
					'controller' => 'main_images',
					'admin' => true
					),
				array(
					'escape' => false
					)
				);
		?>
	</li>
	<li class="<?php echo $activeMenus['news']['active']; ?>">
		<?php
			echo $this->Html->link(
				'<i class="icon-list-alt"></i><span class="title">Noticias</span>'.$activeMenus['news']['selected'],
				array(
					'action' => 'index',
					'controller' => 'news',
					'admin' => true
					),
				array(
					'escape' => false
					)
				);
		?>
	</li>
	<li class="<?php echo $activeMenus['events']['active']; ?>">
		<?php
			echo $this->Html->link(
				'<i class="icon-calendar"></i><span class="title">Eventos</span>'.$activeMenus['events']['selected'],
				array(
					'action' => 'index',
					'controller' => 'events',
					'admin' => true
					),
				array(
					'escape' => false
					)
				);
		?>
	</li>
	<li class="<?php echo $activeMenus['documents']['active']; ?>">
		<?php
			echo $this->Html->link(
				'<i class="icon-book"></i><span class="title">Planes y Programas</span>'.$activeMenus['documents']['selected'],
				array(
					'action' => 'index',
					'controller' => 'documents',
					'admin' => true
					),
				array(
					'escape' => false
					)
				);
		?>
	</li>
	<li class="<?php echo $activeMenus['defaultImages']['active']; ?>">
		<?php
			echo $this->Html->link(
				'<i class="icon-th-large"></i><span class="title">Galería de Imágenes</span>'.$activeMenus['defaultImages']['selected'],
				array(
					'action' => 'index',
					'controller' => 'default_images',
					'admin' => true
					),
				array(
					'escape' => false
					)
				);
		?>
	</li>
	<?php if($isSuperUser): ?>
	<li class="<?php echo $activeMenus['users']['active']; ?>">
		<?php
			echo $this->Html->link(
				'<i class="icon-user"></i><span class="title">Usuarios</span>'.$activeMenus['users']['selected'],
				array(
					'action' => 'index',
					'controller' => 'users',
					'admin' => true
					),
				array(
					'escape' => false
					)
				);
		?>
	</li>
	<li class="<?php echo $activeMenus['configurations']['active']; ?>">
		<?php
			echo $this->Html->link(
				'<i class="icon-cog"></i><span class="title">Configuraciones</span>'.$activeMenus['configurations']['selected'],
				array(
					'controller' => 'configurations',
					'action' => 'index',
					'admin' => true
					),
				array(
					'escape' => false
					)
				);
		?>
	</li>
	<?php endif; ?>
</ul>
<!-- END SIDEBAR MENU -->