<?php
	echo $this->Html->css(array(
		'admin/fancybox/source/jquery.fancybox',
		'admin/data-tables/DT_bootstrap',
		), null, array('inline' => false)
	);

	$this->set('activeMenu', 'mainImages');

	$this->Html->addCrumb('Imagenes Principales',
		array(
			'action' => 'index',
			'controller' => 'main_images',
			'admin' => true
			)
		);
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
	<div class="span12">
		<!-- BEGIN PORTLET-->
		<div class="portlet box light-grey">
			<div class="portlet-title">
				<h4><i class="icon-picture"></i>Imágenes principales</h4>
				<div class="actions">
					<?php echo $this->Html->link('<i class="icon-plus"></i> Añadir</a>',
						array(
							'action' => 'add',
							'controller' => 'main_images',
							'admin' => true
							),
						array(
							'class' => 'btn green',
							'escape' => false)
						);
					?>
					<!-- <a href="#" class="btn green"> -->
				</div>
			</div>
			<div class="portlet-body">
			<!-- BEGIN GALLERY MANAGER LISTING-->
			<?php if(!empty($images)): ?>
				<?php $i = 0; ?>
				<?php foreach ($images as $image): ?>
					<?php $i++ ?>

					<?php if ($i%4 == 1) : ?>
					<div class="row-fluid">
					<?php endif ?>
						<div class="span3">
							<div class="item">
								<a class="fancybox-button" data-rel="fancybox-button" title="<?php echo $image['MainImage']['name']; ?>" href="<?php echo '/img/images/main_img_'.$image['MainImage']['image']; ?>">
									<div class="zoom">
										<?php echo $this->Html->image('images/main_img_'.$image['MainImage']['image']); ?>
										<div class="zoom-icon"></div>
									</div>
								</a>
								<div class="details">
									<?php $link = !empty($image['MainImage']['link']) ? $image['MainImage']['link'] : '';?>
									<?php echo !empty($link) ? $this->Html->link('<i class="icon-link"></i>', $link, array('class'=>'icon', 'escape'=>false)) : '' ?>
									<?php echo $this->Html->link('<i class="icon-pencil"></i>', array('action' => 'edit', 'controller' => 'main_images', $image['MainImage']['id'],'admin' => true), array('class'=>'icon', 'escape'=>false)); ?>
									<?php
										echo $this->Form->postLink(
											'<i class="icon-remove"></i>',
											array(
												'action' => 'delete',
												'controller' => 'main_images',
												$image['MainImage']['id'],
												'admin' => true
												),
											array(
												'class' => 'icon',
												'escape' => false,
												'confirm' => 'Estas seguro de borrar esta imagen?'
												)
											);
									?>
								</div>
							</div>
						</div>
					<?php if ($i%4 == 0 || sizeof($images) == $i) : ?>
					</div>
					<?php endif ?>
					<?php endforeach; ?>
			<?php else: ?>
				<div class="alert alert-block alert-info fade in">
					<button type="button" class="close" data-dismiss="alert"></button>
					<h4 class="alert-heading">No hay imágenes!</h4>
					<p>
						Aún no se han agregado imágenes principales. Haz click en el botón <strong>Añadir</strong> para agregar imágenes.
					</p>
				</div>
			<?php endif; ?>
			</div>
		</div>
		<!-- END PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT-->