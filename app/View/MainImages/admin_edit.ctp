<?php
	echo $this->Html->css(array(
		'admin/bootstrap-fileupload/bootstrap-fileupload',
		'admin/gritter/css/jquery.gritter',
		'admin/jquery-tags-input/jquery.tagsinput',
		'admin/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons',
		'admin/data-tables/DT_bootstrap',
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		//'admin/js/jquery-1.8.3.min',
		'admin/bootstrap-fileupload/bootstrap-fileupload',
		'admin/jquery-tags-input/jquery.tagsinput.min',
		'admin/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons'
		), array('inline' => false)
	);

	$this->set('activeMenu', 'mainImages');

	$this->Html->addCrumb('Imagenes Principales',
		array(
		'action' => 'index',
		'controller' => 'main_images',
		'admin' => true
		)
	);

	$this->Html->addCrumb('Imagenes Principales',
		array(
		'action' => 'index',
		'controller' => 'main_images',
		'admin' => true
		)
	);

	$mainImage = $this->request->data;
	$this->Html->addCrumb($mainImage['MainImage']['name'], null);

	$this->Html->addCrumb('Editar', null);
?>
<div class="portlet box blue">
   <div class="portlet-title">
	  <h4><i class="icon-picture"></i>Editar imagen</h4>
	  <div class="actions">
		 <?php echo $this->Html->link('<i class="icon-picture"></i> Imágenes Principales</a>',
			array(
			   'action' => 'index',
			   'controller' => 'main_images',
			   'admin' => true
			   ),
			array(
			   'class' => 'btn blue',
			   'escape' => false)
			);
		 ?>
		 <!-- <a href="#" class="btn green"> -->
	  </div>
   </div>
   <div class="portlet-body form">
	  <!-- BEGIN FORM-->
	  <?php echo $this->Form->create('MainImage',
		 array(
			'inputDefaults' => array(
			   'div' => false,
			   'class' => 'm-wrap span8'
			   ),
			'class' => 'form-horizontal form-row-seperated',
			'enctype' => 'multipart/form-data',
			'novalidate' => true
			)
		 );
		 echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$mainImage['MainImage']['id']));
	  ?>
		<div class="control-group <?php echo isset($this->validationErrors['MainImage']['name']) ? 'error' : ''; ?>">
			<?php
				echo $this->Form->input('name',
					array(
						'between' => '<div class="controls input-icon">',
						'label' => array('text'=>'Nombre<span class="required">*</span>', 'class'=>'control-label'),
						'placeholder'=>'Nombre de la imagen',
						'error' => array(
							'attributes' => array('escape' => false, 'wrap' => false)
							)
						)
					);
				echo $this->Html->tag('span', 'Es el nombre o palabra que aparecerá en el slide de la página principal', array('class'=>'help-block'));
				echo '</div>';
			?>
		</div>
		<div class="control-group <?php echo isset($this->validationErrors['MainImage']['link']) ? 'error' : ''; ?>">
			<?php
				echo $this->Form->input('link',
					array(
						'between' => '<div class="controls input-icon">',
						'label' => array('text'=>'URL', 'class'=>'control-label'),
						'placeholder'=>'http://www.ejemplo.com',
						'error' => array(
							'attributes' => array('escape' => false, 'wrap' => false)
							)
						)
					);
				echo $this->Html->tag('span', '*Opcional', array('class'=>'help-block'));
				echo '</div>';
			?>
		</div>

		 <div class="control-group">
			<label class="control-label">Imagen</label>
			<div class="controls">
			   <div class="fileupload fileupload-new" data-provides="fileupload">
				  <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
					 <?php echo $this->Html->image('images/main_thn_'.$mainImage['MainImage']['image'], array('alt' => $mainImage['MainImage']['name'])); ?>
				  </div>
				  <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">
				  </div>
				  <div>
					 <span class="btn btn-file"><span class="fileupload-new">Seleccionar</span>
					 <span class="fileupload-exists">Cambiar</span>
					 <input type="file" name="data[MainImage][image]" class="default" id="MainImageImage"></span>
					 <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Eliminar</a>
				  </div>
			   </div>
			   <span class="label label-important">NOTA!</span>
			   <span>La imagen debe pesar menos de 20 MB</span>
			</div>
		 </div>

	  <?php
		 echo '<div class="form-actions">';
		 echo $this->Form->button('<i class="icon-ok"></i> Guardar',
			array('type' => 'submit', 'class' => 'btn green')
			);
		 echo '&nbsp;';

		 $cancelOptions = array('action' => 'index', 'controller' => 'main_images', 'admin' => true);
		 echo $this->Form->button('Cancelar',
			array(
			   'type' => 'button',
			   'class' => 'btn red',
			   'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
			   )
			);
		 echo '</div>';
	  ?>

	  <?php echo $this->Form->end(); ?>
	  <!-- END FORM-->
   </div>
</div>