<?php
	echo $this->Html->css(
		array(
			'admin/fancybox/source/jquery.fancybox',
			'admin/data-tables/DT_bootstrap'
			),
		null,
		array('inline' => false)
	);

	echo $this->Html->script(
		array(
			'admin/jquery-slimscroll/jquery.slimscroll.min.js',
			),
		array('inline' => false)
	);

	$img = isset($event['Event']['image']) && !empty($event['Event']['image']) ? '/img/images/events_img_'.$event['Event']['image'] : 'http://www.placehold.it/820x674/EFEFEF/AAAAAA&amp;text=Sin+imagen+de+perfil';
	$this->Html->addCrumb('Eventos',
		array(
			'action' => 'index',
			'controller' => 'events',
			'admin' => true
			)
		);

	$this->set('activeMenu', 'events');

	$this->Html->addCrumb('Ver', null);

	$this->Html->addCrumb($this->Text->truncate($event['Event']['name'], 30, array('ellipsis' => '...', 'exact' => false)),
		array(
			'action' => 'view',
			'controller' => 'events',
			$event['Event']['id'],
			Format::clean($event['Event']['name']),
			'admin' => true
			)
		);
?>
<div class="row-fluid">
	<div class="span3 offset9">
		<div class="actions">
			<?php echo $this->Html->link('<i class="icon-edit"></i> Editar',
				array(
					'action' => 'edit',
					'controller' => 'events',
					$event['Event']['id'],
					Format::clean($event['Event']['name']),
					'admin' => true
					),
				array(
					'class' => 'btn grey',
					'escape' => false)
				);
			?>
			<?php
				echo $this->Form->postLink('<i class="icon-trash"></i> Eliminar',
					array('action' => 'delete', $event['Event']['id']),
					array('class'=>'btn red', 'escape'=>false),	__('Confirma que desea eliminar este evento?'));
			?>
			<?php echo $this->Html->link('<i class="icon-list-alt"></i> Eventos',
				array(
					'action' => 'index',
					'controller' => 'events',
					'admin' => true
					),
				array(
					'class' => 'btn brown',
					'escape' => false)
				);
			?>
		</div>
	</div>
</div>

<div class="row-fluid active">
	<ul class="unstyled profile-nav span3">
		<li>
			<?php $img = isset($event['Event']['image']) && !empty($event['Event']['image']) ? 'images/events_img_'.$event['Event']['image'] : 'http://www.placehold.it/378x311/EFEFEF/AAAAAA&amp;text=Sin+foto'; ?>
			<?php echo $this->Html->image($img); ?>
		</li>
		<li>
			<br/>
			<div>
				<div class="portlet sale-summary">
					<div class="portlet-title">
						<h4>Detalles del evento</h4>
					</div>
					<ul class="unstyled">
						<li>
							<span class="sale-info">LUGAR </span>
							<span class="sale-num">
								<?php echo !empty($event['Event']['place']) ? utf8_encode(strtoupper($event['Event']['place'])) : '<em>'.utf8_encode(strtoupper('No se estableció lugar')).'</em>'; ?>
							</span>
						</li>
						<li>
							<span class="sale-info">FECHA DE CREACIÓN </span>
							<span class="sale-num">
								<?php echo utf8_encode(strtoupper(strftime('%A %#d de %B del %Y, %H:%M', strtotime($event['Event']['created'])))); ?>
							</span>
						</li>
						<li>
							<span class="sale-info">FECHA DE MODIFICACIÓN</span>
							<span class="sale-num">
								<?php echo utf8_encode(strtoupper(strftime('%A %#d de %B del %Y, %H:%M', strtotime($event['Event']['modified'])))); ?>
							</span>
						</li>
					</ul>
				</div>
			</div>
			<!--end span4-->
		</li>
	</ul>
	<div class="span9">
		<div class="row-fluid">
			<div class="span9 profile-info">
				<h1><?php echo $event['Event']['name']; ?></h1>
				<ul class="unstyled inline">
					<strong>Inicia:</strong>&nbsp;
					<li>
						<i class="icon-calendar"></i>
						<?php echo !empty($event['Event']['start_date']) ? utf8_encode(strtoupper(strftime('%A %#d de %B del %Y', strtotime($event['Event']['start_date'])))) : '<em>No hay fecha</em>';?>
					</li>
					<li>
						<i class="icon-time"></i>
						<?php echo !empty($event['Event']['start_time']) ? $event['Event']['start_time'] : '<em>No hay hora</em>';?>
					</li>
					&nbsp;|&nbsp;&nbsp;&nbsp;<strong>Termina:</strong>&nbsp;
					<li>
						<i class="icon-calendar"></i>
						<?php echo !empty($event['Event']['end_date']) ? utf8_encode(strtoupper(strftime('%A %#d de %B del %Y', strtotime($event['Event']['end_date'])))) : '<em>No hay fecha</em>';?>
					</li>
					<li>
						<i class="icon-time"></i>
						<?php echo !empty($event['Event']['end_time']) ? $event['Event']['end_time'] : '<em>No hay hora</em>';?>
					</li>
				</ul>
				<?php echo $event['Event']['description']; ?>
				<hr>
				<strong>Enlace: </strong>
				<?php echo !empty($event['Event']['link']) ? $this->Html->link($event['Event']['link']) : '<em>No hay enlace</em>'?>
			</div>
			<!--end span8-->
		</div>
		<!--end row-fluid-->
	</div>
	<!--end span9-->
</div><hr>

<?php
	echo $this->element(
		'admin/default_image_gallery',
		array(
			'data' => $event['DefaultImage'],
			'imagePrefix' => 'events',
			'modelId' => $event['Event']['id'],
			'dataTitle' => $event['Event']['name']
			)
		);
?>