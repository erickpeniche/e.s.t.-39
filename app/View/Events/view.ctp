<?php
	echo $this->Html->css(
		array(
			//'admin/bootstrap/css/bootstrap.min',
			'admin/bootstrap/css/bootstrap-responsive.min',
			'admin/fancybox/source/jquery.fancybox',
			'admin/font-awesome/css/font-awesome',
			'gallery',
			),
		null,
		array('inline' => false)
	);

	echo $this->Html->script(
		array(
			'admin/jquery-slimscroll/jquery-ui-1.9.2.custom.min',
			'admin/jquery-slimscroll/jquery.slimscroll.min.js',
			'admin/fancybox/source/jquery.fancybox.pack'
			),
		array('inline' => false)
	);

	$date = !empty($event['Event']['start_date']) ? substr($event['Event']['start_date'], -2) : '-';
	$img = isset($event['Event']['image']) && !empty($event['Event']['image']) ? 'images/news_main_prev_'.$event['Event']['image'] : 'http://www.placehold.it/210x229/EFEFEF/AAAAAA&amp;text=Sin+foto';
?>
<div class="wrapper">
	<article class="grid_8">
		<div class="events_box">
			<h2><?php echo $event['Event']['name']; ?></h2>
			<ul class="ext_list news">
				<li>
					<figure>
						<time datetime="<?php echo !empty($event['Event']['start_date']) ? $event['Event']['start_date'] : ''; ?>">
							<?php echo $date;  ?>
							<span><?php echo !empty($event['Event']['start_date']) ? utf8_encode(strtoupper(strftime('%b', strtotime($event['Event']['start_date'])))) : '...'; ?></span>
						</time>
					</figure>
					<div>
						<strong class="white">
							<?php echo !empty($event['Event']['start_date']) ? utf8_encode(strtoupper(strftime('%A %#d de %B del %Y, %H:%M', strtotime($event['Event']['start_date'].' '.$event['Event']['start_time'])))) : '<em>No hay fecha</em>'; ?>
						</strong><br>
						<?php echo $event['Event']['description']; ?><br>
						Link: <em><?php echo !empty($event['Event']['link']) ? $this->Html->link($event['Event']['link']) : 'No hay enlace' ; ?></em>
					</div>
				</li>
			</ul>
		</div>
	</article>
	<article class="grid_4 last-col">
		<h2>Información</h2>
		<p class="p3">
			<div class="img_wrap1"><?php echo !empty($event['Event']['image']) ? $this->Html->image('images/events_main_prev_'.$event['Event']['image']) : $this->Html->image('http://www.placehold.it/290x163/EFEFEF/AAAAAA&amp;text=Sin+foto'); ?></div>
		</p>
		<em>Fecha de inicio:</em>
		<ul class="list1">
			<li><?php echo !empty($event['Event']['start_date']) ? utf8_encode(strtoupper(strftime('%A %#d de %B del %Y, %H:%M', strtotime($event['Event']['start_date'].' '.$event['Event']['start_time'])))) : 'No hay fecha de inicio'; ?></li>
		</ul>
		<em>Fecha de finalización:</em>
		<ul class="list1">
			<li><?php echo !empty($event['Event']['end_date']) ? utf8_encode(strtoupper(strftime('%A %#d de %B del %Y, %H:%M', strtotime($event['Event']['end_date'].' '.$event['Event']['end_time'])))) : 'No hay fecha de finalización'; ?></li>
		</ul>
	</article>
</div>
<div class="wrapper">
	<article class="grid_12">
		<?php
			echo $this->element(
				'image_gallery',
				array(
					'data' => $event['DefaultImage'],
					'imagePrefix' => 'events',
					'modelId' => $event['Event']['id'],
					'dataTitle' => $event['Event']['name']
					)
				);
		?>
	</article>
</div>