<?php
	echo $this->Html->css(array(
		'admin/bootstrap-fileupload/bootstrap-fileupload',
		'admin/gritter/css/jquery.gritter',
		'admin/jquery-tags-input/jquery.tagsinput',
		'admin/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons',
		'admin/data-tables/DT_bootstrap',
		'admin/bootstrap-wysihtml5/bootstrap-wysihtml5',
		'admin/bootstrap-datepicker/css/datepicker',
		'admin/bootstrap-timepicker/compiled/timepicker'
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		'admin/bootstrap-fileupload/bootstrap-fileupload',
		'admin/jquery-tags-input/jquery.tagsinput.min',
		'admin/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons',
		'admin/jquery-validation/dist/jquery.validate.min',
		'admin/jquery-validation/dist/additional-methods.min',
		'admin/ckeditor/ckeditor',
		'admin/bootstrap-wysihtml5/wysihtml5-0.3.0',
		'admin/bootstrap-wysihtml5/bootstrap-wysihtml5',
		'admin/bootstrap-datepicker/js/bootstrap-datepicker',
		'admin/bootstrap-timepicker/js/bootstrap-timepicker'
		), array('inline' => false)
	);

	$this->set('activeMenu', 'events');

	$this->Html->addCrumb('Eventos',
		array(
			'action' => 'index',
			'controller' => 'events',
			'admin' => true
			)
		);

	$event = $this->request->data;
	$this->Html->addCrumb($event['Event']['name'],
		array(
			'action' => 'view',
			'controller' => 'events',
			$event['Event']['id'],
			Format::clean($event['Event']['name']),
			'admin' => true
			)
		);

	$this->Html->addCrumb('Editar', null);
?>
<div class="portlet box blue">
   <div class="portlet-title">
	  <h4><i class="icon-calendar"></i>Editar Evento</h4>
	  <div class="actions">
		 <?php echo $this->Html->link('<i class="icon-calendar"></i> Eventos</a>',
			array(
			   'action' => 'index',
			   'controller' => 'events',
			   'admin' => true
			   ),
			array(
			   'class' => 'btn brown',
			   'escape' => false)
			);
		 ?>
	  </div>
   </div>
   <div class="portlet-body form">
	  <!-- BEGIN FORM-->
	  <?php echo $this->Form->create('Event',
		 array(
			'inputDefaults' => array(
			   'div' => false,
			   'class' => 'm-wrap span8'
			   ),
			'class' => 'form-horizontal form-row-seperated',
			'enctype' => 'multipart/form-data',
			'novalidate' => true
			)
		 );
		echo $this->Form->input('id', array('hiddenField' => true));
	  ?>
		<div class="control-group <?php echo isset($this->validationErrors['Event']['name']) ? 'error' : ''; ?>">
			<?php
				echo $this->Form->input('name',
					array(
						'between' => '<div class="controls input-icon">',
						'label' => array('text'=>'Nombre<span class="required">*</span>', 'class'=>'control-label'),
						'placeholder'=>'Nombre o título del evento',
						'error' => array(
							'attributes' => array('escape' => false, 'wrap' => false)
							)
						)
					);
				echo '</div>';
			?>
		</div>
		<div class="control-group <?php echo isset($this->validationErrors['Event']['description']) ? 'error' : ''; ?>">
			<?php
				echo $this->Form->input('description',
					array(
						'type'=>'textarea',
						'class'=>'span12 ckeditor m-wrap',
						'rows'=>6,
						'between' => '<div class="controls input-icon">',
						'label' => array('text'=>'Contenido<span class="required">*</span>', 'class'=>'control-label'),
						'error' => array(
							'attributes' => array('escape' => false, 'wrap' => false)
							)
						)
					);
				echo '</div>';
			?>
		</div>
		<div class="control-group">
			<label class="control-label">Fecha de comienzo</label>
			<div class="controls">
				<div class="input-append date date-picker" data-date="'.date("Y-m-d").'" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
					<?php
						echo $this->Form->input('start_date',
							array(
								'label' => false,
								'type' => 'text',
								'class' => 'm-wrap m-ctrl-medium date-picker',
								'size' => 16,
								'placeholder' => 'Escoge la fecha de inicio...',
								'data-date-format' => 'yyyy-mm-dd',
								'readonly' => true
							)
						);
					?>
					<span class="add-on"><i class="icon-calendar"></i></span>
				</div>
				<?php
					echo $this->Html->tag('span', '*Opcional', array('class'=>'help-block'));
				?>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Hora de comienzo</label>
			<div class="controls">
				<div class="input-append bootstrap-timepicker-component">
					<?php
						echo $this->Form->input('start_time',
							array(
								'label' => false,
								'type' => 'text',
								'class' => 'm-wrap m-ctrl-small timepicker-24',
								'placeholder' => 'Escoge la hora de inicio...',
								'readonly' => true
							)
						);
					?>
					<span class="add-on"><i class="icon-time"></i></span>
				</div>
				<?php
					echo $this->Html->tag('span', '*Opcional', array('class'=>'help-block'));
				?>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Fecha de finalización</label>
			<div class="controls">
				<div class="input-append date date-picker" data-date="'.date("Y-m-d").'" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
					<?php
						echo $this->Form->input('end_date',
							array(
								'label' => false,
								'type' => 'text',
								'class' => 'm-wrap m-ctrl-medium date-picker',
								'size' => 16,
								'placeholder' => 'Escoge la fecha de finalización...',
								'data-date-format' => 'yyyy-mm-dd',
								'readonly' => true
							)
						);
					?>
					<span class="add-on"><i class="icon-calendar"></i></span>
				</div>
				<?php
					echo $this->Html->tag('span', '*Opcional', array('class'=>'help-block'));
				?>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Hora de finalización</label>
			<div class="controls">
				<div class="input-append bootstrap-timepicker-component">
					<?php
						echo $this->Form->input('end_time',
							array(
								'label' => false,
								'type' => 'text',
								'class' => 'm-wrap m-ctrl-small timepicker-24',
								'placeholder' => 'Escoge la hora de finalización...',
								'readonly' => true
							)
						);
					?>
					<span class="add-on"><i class="icon-time"></i></span>
				</div>
				<?php
					echo $this->Html->tag('span', '*Opcional', array('class'=>'help-block'));
				?>
			</div>
		</div>
		<div class="control-group <?php echo isset($this->validationErrors['Event']['place']) ? 'error' : ''; ?>">
			<?php
				echo $this->Form->input('place',
					array(
						'between' => '<div class="controls input-icon">',
						'label' => array('text'=>'Lugar', 'class'=>'control-label'),
						'placeholder'=>'Dirección o lugar del evento',
						'error' => array(
							'attributes' => array('escape' => false, 'wrap' => false)
							)
						)
					);
				echo '</div>';
			?>
		</div>
		<?php
			$imagePrev = isset($event['Event']['image']) && !empty($event['Event']['image']) ? '/img/images/events_img_edt_'.$event['Event']['image'] : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=sin+imagen';
		?>
		<div class="control-group">
			<label class="control-label">Foto principal</label>
			<div class="controls">
				<div class="fileupload fileupload-new" data-provides="fileupload">
					<div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
						<img src="<?php echo $imagePrev; ?>" alt="" />
					</div>
					<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
					<div>
						<span class="btn btn-file"><span class="fileupload-new">Seleccionar</span>
						<span class="fileupload-exists">Cambiar</span>
						<input type="file" name="data[Event][image]" class="default" id="EventImage"></span>
						<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Eliminar</a>
					</div>
				</div>
				<?php
					echo $this->Html->tag('span', '*Opcional', array('class'=>'help-block'));
				?>
			</div>
		</div>
		<div class="control-group <?php echo isset($this->validationErrors['Event']['link']) ? 'error' : ''; ?>">
			<?php
				echo $this->Form->input('link',
					array(
						'between' => '<div class="controls input-icon">',
						'label' => array('text'=>'URL', 'class'=>'control-label'),
						'placeholder'=>'http://www.ejemplo.com',
						'error' => array(
							'attributes' => array('escape' => false, 'wrap' => false)
							)
						)
					);
				echo $this->Html->tag('span', '*Opcional', array('class'=>'help-block'));
				echo '</div>';
			?>
		</div>
	<?php
		 echo '<div class="form-actions">';
		 echo $this->Form->button('<i class="icon-ok"></i> Guardar',
			array('type' => 'submit', 'class' => 'btn green')
			);
		 echo '&nbsp;';

		 $cancelOptions = array('action' => 'index', 'controller' => 'events', 'admin' => true);
		 echo $this->Form->button('Cancelar',
			array(
			   'type' => 'button',
			   'class' => 'btn red',
			   'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
			   )
			);
		 echo '</div>';
	  ?>
	  <?php echo $this->Form->end(); ?>
	  <!-- END FORM-->
   </div>
</div>