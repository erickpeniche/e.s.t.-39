<?php
	echo $this->Html->css(array(
		'admin/fancybox/source/jquery.fancybox',
		'admin/data-tables/DT_bootstrap',
		), null, array('inline' => false)
	);

	$this->set('activeMenu', 'events');

	$this->Html->addCrumb('Eventos',
		array(
			'action' => 'index',
			'controller' => 'events',
			'admin' => true
			)
		);
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
	<div class="span12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box light-grey">
			<div class="portlet-title">
				<h4><i class="icon-calendar"></i>Eventos</h4>
				<div class="actions">
					<?php echo $this->Html->link('<i class="icon-plus"></i> Nuevo</a>',
						array(
							'action' => 'add',
							'controller' => 'events'
							),
						array(
							'class' => 'btn green',
							'escape' => false)
						);
					?>
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover" id="sample_3">
					<thead>
						<tr>
							<th width="5%">Foto</th>
							<th width="15%"><?php echo $this->Paginator->sort('name', 'Nombre'); ?></th>
							<th width="30%"><?php echo $this->Paginator->sort('description', 'Descripción'); ?></th>
							<th width="10%"><?php echo $this->Paginator->sort('start_date', 'Fecha de inicio'); ?></th>
							<th width="10%"><?php echo $this->Paginator->sort('start_time', 'Hora de inicio'); ?></th>
							<th width="10%"><?php echo $this->Paginator->sort('end_date', 'Fecha de finalización'); ?></th>
							<th width="10%"><?php echo $this->Paginator->sort('end_time', 'Hora de finalización'); ?></th>
							<th class="actions" width="10%"><?php echo __('Acciones'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php if (empty($events)) : ?>
						<tr>
							<td colspan="8">
								<div class="alert alert-block alert-info fade in">
									<button type="button" class="close" data-dismiss="alert"></button>
									<h4 class="alert-heading">No hay eventos!</h4>
									<p>
										Aún no se han agregado eventos. Haz click en el botón 'Nuevo' para agregar un nuevo evento.
									</p>
								</div>
							</td>
						</tr>
						<?php else: ?>
						<?php foreach ($events as $event): ?>
						<?php $img = isset($event['Event']['image']) && !empty($event['Event']['image']) ? '/img/images/events_tbl_prev_'.$event['Event']['image'] : 'http://www.placehold.it/100x100/EFEFEF/AAAAAA&amp;text=Sin+foto' ?>
						<tr class="odd gradeX">
							<td><?php echo $this->Html->image($img); ?></td>
							<td>
								<?php
									echo $this->Html->link(
										$this->Text->truncate(strip_tags($event['Event']['name']), 100, array('ellipsis' => '...', 'exact' => false)),
										array(
											'action' => 'view',
											$event['Event']['id'],
											Format::clean($event['Event']['name'])
											)
										);
								?>
								&nbsp;
							</td>
							<td>
								<?php echo $this->Text->truncate(strip_tags($event['Event']['description']), 300, array('ellipsis' => '...', 'exact' => false)); ?>
								&nbsp;
							</td>
							<td><?php echo h($event['Event']['start_date']); ?>&nbsp;</td>
							<td><?php echo h($event['Event']['start_time']); ?>&nbsp;</td>
							<td><?php echo h($event['Event']['end_date']); ?>&nbsp;</td>
							<td><?php echo h($event['Event']['end_time']); ?>&nbsp;</td>
							<td class="actions">
								<?php echo $this->Html->link('<i class="icon-info-sign"></i> Ver', array('action' => 'view', $event['Event']['id'], Format::clean($event['Event']['name'])), array('class'=>'btn mini blue', 'escape'=>false)); ?>
								<?php echo $this->Html->link('<i class="icon-edit"></i> Editar', array('action' => 'edit', $event['Event']['id'], Format::clean($event['Event']['name'])), array('class'=>'btn mini grey', 'escape'=>false)); ?>
								<?php echo $this->Form->postLink('<i class="icon-trash"></i> Eliminar', array('action' => 'delete', $event['Event']['id']), array('class'=>'btn mini red', 'escape'=>false), __('¿Confirma que desea eliminar este evento?')); ?>
							</td>
						</tr>
						<?php endforeach; ?>
						<?php endif; ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT-->
