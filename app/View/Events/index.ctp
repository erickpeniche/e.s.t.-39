<?php
	$totalEvents = count($events);
?>
<div class="wrapper">
	<article class="grid_12">
		<div class="events_box">
			<h2 class="ind">Eventos</h2>
			<ul class="ext_list events">
				<?php foreach($events as $index => $event): ?>
				<li class="<?php echo $index == ($totalEvents-1) ? 'last' : ''; ?>">
					<figure>
						<?php $img = !empty($event['Event']['image']) ? $this->Html->image('images/events_main_prev_'.$event['Event']['image']) : $this->Html->image('http://www.placehold.it/290x163/EFEFEF/AAAAAA&amp;text=Sin+foto'); ?>
						<?php
							echo $this->Html->link($img ,
								array(
									'controller' => 'events',
									'action' => 'view',
									$event['Event']['id'],
									Format::clean($event['Event']['name'])
								),
								array('class'=>'img_wrap1', 'escape' => false)
							);
						?>
					</figure>
					<div>
						<h2>
						<?php
							echo $this->Html->link($event['Event']['name'],
								array(
									'controller' => 'events',
									'action' => 'view',
									$event['Event']['id'],
									Format::clean($event['Event']['name'])
								),
								array('escape' => false)
							);
						?>
						</h2>
						<time datetime="<?php echo !empty($event['Event']['start_time']) ? $event['Event']['start_time'] : ''; ?>"><?php echo !empty($event['Event']['start_date']) ? utf8_encode(strtoupper(strftime('%A %#d de %B del %Y', strtotime($event['Event']['start_date'])))) : '<em>No hay fecha</em>';?></time>
						<br>
						<?php echo $this->Text->truncate(strip_tags($event['Event']['description']), 380, array('ellipsis' => '...', 'exact' => false)); ?>
						<br>
						<?php
							echo $this->Html->link('<span>Leer más...</span>',
								array(
									'controller' => 'events',
									'action' => 'view',
									$event['Event']['id'],
									Format::clean($event['Event']['name'])
								),
								array('class' => 'button', 'escape' => false)
							);
						?>

					</div>
				</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</article>
</div>