<div class="wrapper">
	<article class="grid_12">
		<h2>Correo de contacto</h2>
		<p>
			<strong>Nombre:</strong><br>
			<?php echo $contactName; ?>
		</p>
		<p>
			<strong>Correo electrónico:</strong><br>
			<?php echo $contactMail; ?>
		</p>
		<p>
			<strong>Teléfono:</strong><br>
			<?php echo $contactPhone; ?>
		</p>
		<p>
			<strong>Mensaje:</strong><br>
			<?php echo $contactMessage; ?>
		</p>
	</article>
</div>