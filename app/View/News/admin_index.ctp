<?php
	echo $this->Html->css(array(
		'admin/fancybox/source/jquery.fancybox',
		'admin/data-tables/DT_bootstrap',
		), null, array('inline' => false)
	);

	$this->set('activeMenu', 'news');

	$this->Html->addCrumb('Noticias',
		array(
			'action' => 'index',
			'controller' => 'news',
			'admin' => true
			)
		);
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
	<div class="span12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box light-grey">
			<div class="portlet-title">
				<h4><i class="icon-list-alt"></i>Noticias</h4>
				<div class="actions">
					<?php echo $this->Html->link('<i class="icon-plus"></i> Nueva</a>',
						array(
							'action' => 'add',
							'controller' => 'news'
							),
						array(
							'class' => 'btn green',
							'escape' => false)
						);
					?>
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover" id="sample_3">
					<thead>
						<tr>
							<th width='10%'>Foto</th>
							<th width='25%'><?php echo $this->Paginator->sort('name', 'Título'); ?></th>
							<th width='40%'><?php echo $this->Paginator->sort('description', 'Descripción'); ?></th>
							<th width='10%'><?php echo $this->Paginator->sort('created', 'Fecha de creación'); ?></th>
							<th class="actions" width='15%'><?php echo __('Acciones'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php if (empty($news)) : ?>
						<tr>
							<td colspan="5">
								<div class="alert alert-block alert-info fade in">
									<button type="button" class="close" data-dismiss="alert"></button>
									<h4 class="alert-heading">No hay noticias!</h4>
									<p>
										Aún no se han agregado noticias. Haz click en el botón 'Nuevo' para agregar una nueva noticia.
									</p>
								</div>
							</td>
						</tr>
						<?php else: ?>
						<?php foreach ($news as $newsInfo): ?>
						<?php $img = isset($newsInfo['News']['image']) && !empty($newsInfo['News']['image']) ? '/img/images/news_tbl_prev_'.$newsInfo['News']['image'] : 'http://www.placehold.it/100x100/EFEFEF/AAAAAA&amp;text=Sin+foto' ?>
						<tr class="odd gradeX">
							<td><?php echo $this->Html->image($img); ?></td>
							<td>
								<?php
									echo $this->Html->link(
										$this->Text->truncate(strip_tags($newsInfo['News']['title']), 100, array('ellipsis' => '...', 'exact' => false)),
										array(
											'action' => 'view',
											$newsInfo['News']['id'],
											Format::clean($newsInfo['News']['title'])
											)
										);
								?>
								&nbsp;
							</td>
							<td>
								<?php echo $this->Text->truncate(strip_tags($newsInfo['News']['description']), 300, array('ellipsis' => '...', 'exact' => false)); ?>
								&nbsp;
							</td>
							<td><?php echo h($newsInfo['News']['created']); ?>&nbsp;</td>
							<td class="actions">
								<?php echo $this->Html->link('<i class="icon-info-sign"></i> Ver', array('action' => 'view', $newsInfo['News']['id'], Format::clean($newsInfo['News']['title'])), array('class'=>'btn mini blue', 'escape'=>false)); ?>
								<?php echo $this->Html->link('<i class="icon-edit"></i> Editar', array('action' => 'edit', $newsInfo['News']['id'], Format::clean($newsInfo['News']['title'])), array('class'=>'btn mini grey', 'escape'=>false)); ?>
								<?php echo $this->Form->postLink('<i class="icon-trash"></i> Eliminar', array('action' => 'delete', $newsInfo['News']['id']), array('class'=>'btn mini red', 'escape'=>false), __('Confirma que desea eliminar esta noticia?')); ?>
							</td>
						</tr>
						<?php endforeach; ?>
						<?php endif; ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT-->
