<?php
	echo $this->Html->css(
		array(
			'admin/fancybox/source/jquery.fancybox',
			'admin/data-tables/DT_bootstrap'
			),
		null,
		array('inline' => false)
	);

	echo $this->Html->script(
		array(
			'admin/jquery-slimscroll/jquery.slimscroll.min.js',
			),
		array('inline' => false)
	);

	$img = isset($news['News']['image']) && !empty($news['News']['image']) ? '/img/images/news_img_'.$news['News']['image'] : 'http://www.placehold.it/820x674/EFEFEF/AAAAAA&amp;text=Sin+imagen+de+perfil';
	$this->Html->addCrumb('Noticias',
		array(
			'action' => 'index',
			'controller' => 'news',
			'admin' => true
			)
		);

	$this->set('activeMenu', 'news');

	$this->Html->addCrumb('Ver', null);

	$this->Html->addCrumb($this->Text->truncate($news['News']['title'], 30, array('ellipsis' => '...', 'exact' => false)),
		array(
			'action' => 'view',
			'controller' => 'news',
			$news['News']['id'],
			Format::clean($news['News']['title']),
			'admin' => true
			)
		);
?>
<div class="row-fluid">
	<div class="span3 offset9">
		<div class="actions">
			<?php echo $this->Html->link('<i class="icon-edit"></i> Editar',
				array(
					'action' => 'edit',
					'controller' => 'news',
					$news['News']['id'],
					Format::clean($news['News']['title']),
					'admin' => true
					),
				array(
					'class' => 'btn grey',
					'escape' => false)
				);
			?>
			<?php
				echo $this->Form->postLink('<i class="icon-trash"></i> Eliminar',
					array('action' => 'delete', $news['News']['id']),
					array('class'=>'btn red', 'escape'=>false),	__('Confirma que desea eliminar esta noticia?'));
			?>
			<?php echo $this->Html->link('<i class="icon-list-alt"></i> Noticias',
				array(
					'action' => 'index',
					'controller' => 'news',
					'admin' => true
					),
				array(
					'class' => 'btn yellow',
					'escape' => false)
				);
			?>
		</div>
	</div>
</div>

<div class="row-fluid active">
	<ul class="unstyled profile-nav span3">
		<li>
			<?php $img = isset($news['News']['image']) && !empty($news['News']['image']) ? 'images/news_img_'.$news['News']['image'] : 'http://www.placehold.it/378x311/EFEFEF/AAAAAA&amp;text=Sin+foto'; ?>
			<?php echo $this->Html->image($img); ?>
		</li>
		<li>
			<br/>
			<div>
				<div class="portlet sale-summary">
					<div class="portlet-title">
						<h4>Detalles de la noticia</h4>
					</div>
					<ul class="unstyled">
						<li>
							<span class="sale-info">FECHA DE CREACIÓN </span>
							<span class="sale-num">
								<?php echo utf8_encode(strtoupper(strftime('%A %#d de %B del %Y, %H:%M', strtotime($news['News']['created'])))); ?>
							</span>
						</li>
						<li>
							<span class="sale-info">FECHA DE MODIFICACIÓN</span>
							<span class="sale-num">
								<?php echo utf8_encode(strtoupper(strftime('%A %#d de %B del %Y, %H:%M', strtotime($news['News']['modified'])))); ?>
							</span>
						</li>
					</ul>
				</div>
			</div>
			<!--end span4-->
		</li>
	</ul>
	<div class="span9">
		<div class="row-fluid">
			<div class="span9 profile-info">
				<h1><?php echo $news['News']['title']; ?></h1>
				<ul class="unstyled inline">
					<li>
						<i class="icon-calendar"></i>
						<?php echo !empty($news['News']['date']) ? utf8_encode(strtoupper(strftime('%A %#d de %B del %Y', strtotime($news['News']['date'])))) : '<em>No hay fecha</em>';?>
					</li>
					<li>
						<i class="icon-time"></i>
						<?php echo !empty($news['News']['time']) ? $news['News']['time'] : '<em>No hay hora</em>';?>
					</li>
				</ul>
				<?php echo $news['News']['description']; ?>
				<hr>
				<strong>Enlace: </strong>
				<?php echo !empty($event['News']['link']) ? $this->Html->link($event['News']['link']) : '<em>No hay enlace</em>'?>
			</div>
			<!--end span8-->
		</div>
		<!--end row-fluid-->
	</div>
	<!--end span9-->
</div><hr>
<?php
	echo $this->element(
		'admin/default_image_gallery',
		array(
			'data' => $news['DefaultImage'],
			'imagePrefix' => 'news',
			'modelId' => $news['News']['id'],
			'dataTitle' => $news['News']['title']
			)
		);
?>