<?php
	echo $this->Html->css(
		array(
			//'admin/bootstrap/css/bootstrap.min',
			'admin/bootstrap/css/bootstrap-responsive.min',
			'admin/fancybox/source/jquery.fancybox',
			'admin/font-awesome/css/font-awesome',
			'gallery',
			),
		null,
		array('inline' => false)
	);

	echo $this->Html->script(
		array(
			'admin/jquery-slimscroll/jquery-ui-1.9.2.custom.min',
			'admin/jquery-slimscroll/jquery.slimscroll.min.js',
			'admin/fancybox/source/jquery.fancybox.pack'
			),
		array('inline' => false)
	);

	$date = !empty($news['News']['date']) ? substr($news['News']['date'], -2) : '-';
	$img = isset($news['News']['image']) && !empty($news['News']['image']) ? 'images/news_main_prev_'.$news['News']['image'] : 'http://www.placehold.it/210x229/EFEFEF/AAAAAA&amp;text=Sin+foto';
?>
<div class="wrapper">
	<article class="grid_8">
		<div class="events_box">
			<h2><?php echo $news['News']['title']; ?></h2>
			<ul class="ext_list news">
				<li>
					<figure>
						<time datetime="<?php echo !empty($news['News']['date']) ? $news['News']['date'] : ''; ?>">
							<?php echo $date;  ?>
							<span><?php echo !empty($news['News']['date']) ? utf8_encode(strtoupper(strftime('%b', strtotime($news['News']['date'])))) : '...'; ?></span>
						</time>
					</figure>
					<div>
						<strong class="white">
						<?php
							echo !empty($news['News']['date']) ? utf8_encode(strtoupper(strftime('%A %#d de %B del %Y, %H:%M', strtotime($news['News']['date'].' '.$news['News']['time'])))) : '<em>No hay fecha</em>';
						?>
						</strong><br>
						<?php echo $news['News']['description']; ?><br>
						Link: <em><?php echo !empty($news['News']['link']) ? $this->Html->link($news['News']['link']) : 'No hay enlace' ; ?></em>
					</div>
				</li>
			</ul>
		</div>
	</article>
	<article class="grid_4 last-col">
		<h2>Información</h2>
		<p class="p3">
			<div class="img_wrap1"><?php echo !empty($news['News']['image']) ? $this->Html->image('images/news_main_prev_'.$news['News']['image']) : 'http://www.placehold.it/210x229/EFEFEF/AAAAAA&amp;text=Sin+foto'; ?></div>
		</p>
		<em>Fecha:</em>
		<ul class="list1">
			<li><?php echo !empty($news['News']['date']) ? utf8_encode(strtoupper(strftime('%A %#d de %B del %Y, %H:%M', strtotime($news['News']['date'].' '.$news['News']['time'])))) : 'No hay fecha'; ?>
			</li>
		</ul>
	</article>
	<!-- <article class="grid_12"> -->
		<?php
			echo $this->element(
				'image_gallery',
				array(
					'data' => $news['DefaultImage'],
					'imagePrefix' => 'news',
					'modelId' => $news['News']['id'],
					'dataTitle' => $news['News']['title']
					)
				);
		?>
	<!-- </article> -->
</div>