<?php 
	echo $this->Html->css(array(
		'admin/glyphicons/css/glyphicons',
		), null, array('inline' => false)
	);
	$this->set('activeMenu', 'configurations'); 

	$this->Html->addCrumb('Configuraciones',
		array(
			'action' => 'index',
			'controller' => 'configurations',
			'admin' => true
			)
		);

	$configuration = $this->request->data;
	$this->Html->addCrumb($configuration['Configuration']['name'], null);
	$this->Html->addCrumb('Editar', null);
?>

<div class="portlet box blue">
   <div class="portlet-title">
	  <h4><i class="icon-cog"></i>Editar configuración</h4>
	  <div class="actions">
		 <?php echo $this->Html->link('<i class="icon-cog"></i> Configuraciones</a>',
			array(
			   'action' => 'index',
			   'controller' => 'configurations',
			   'admin' => true
			   ),
			array(
			   'class' => 'btn yellow',
			   'escape' => false)
			);
		 ?>
	  </div>
   </div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php 
			echo $this->Form->create('Configuration',
				array(
					'inputDefaults' => array(
						'div' => array('class' => 'control-group'),
						'class' => array('class' => 'm-wrap span12')
						),
					'class' => 'form-horizontal form-row-seperated',
					'novalidate' => true
					)
				);
			echo $this->Form->input('id', array('type' => 'hidden'));
		?>
			<div class="alert alert-error hide">
				<button class="close" data-dismiss="alert"></button>
				Existen errores en el formulario. Favor de revisar los datos.
			</div>
			<div class="alert alert-success hide">
				<button class="close" data-dismiss="alert"></button>
				Los datos son válidos!
			</div>
		<?php
			$div = $this->Html->tag('div', null, array('class'=>'controls input-icon'));
			echo $this->Form->input('value', 
				array(
					'between' => $div,
					'placeholder'=>'Escriba un valor para esta configuración',
					'data-required'=>1,
					'label' => array(
						'text'=>$configuration['Configuration']['name'].'</a><span class="required">*</span>', 
						'class'=>'control-label'
					)
				)
			);
			echo '</div>';

			echo '<div class="form-actions">';
			echo $this->Form->button('<i class="icon-ok"></i> Guardar', 
				array('type' => 'submit', 'class' => 'btn green')
				);
			echo '&nbsp;';

			$cancelOptions = array('action' => 'index', 'controller' => 'configurations', 'admin' => true);
			echo $this->Form->button('Cancelar', 
				array(
					'type' => 'button',
					'class' => 'btn red',
					'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
					)
				);
			echo '</div>';
		?>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->  
	</div>
</div>