<?php 
	echo $this->Html->css(array(
		'admin/glyphicons/css/glyphicons',
		), null, array('inline' => false)
	);
	$this->set('activeMenu', 'configurations'); 

	$this->Html->addCrumb('Configuraciones',
		array(
			'action' => 'index',
			'controller' => 'configurations',
			'admin' => true
			)
		);
?>

<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
	<div class="span12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box light-grey">
			<div class="portlet-title">
				<h4><i class="icon-cogs"></i>Configuraciones</h4>
			</div>
			<div class="portlet-body">
				<div class="row-fluid">
					<div class="span12">
						<!--BEGIN TABS-->
						<div class="tabbable tabbable-custom tabs-left">
							<!-- Only required for left/right tabs -->
							<ul class="nav nav-tabs tabs-left">
								<li class="active"><a href="#tab_3_1" data-toggle="tab"><i class="icon-envelope"></i>&nbsp;&nbsp;Contacto</a></li>
								<li><a href="#tab_3_2" data-toggle="tab"><i class="icon-globe"></i>&nbsp;&nbsp;Sitio Web</a></li>
								<li><a href="#tab_3_3" data-toggle="tab"><i class="icon-group"></i>&nbsp;&nbsp;Redes Sociales</a></li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="tab_3_1">
									<!-- <h3>Contacto</h3> -->
									<!-- BEGIN CONTACT TABLE PORTLET-->
									<table class="table table-striped table-bordered table-advance table-hover">
										<thead>
											<tr>
												<th><?php echo 'Nombre'; ?></th>
												<th ><?php echo 'Valor'; ?></th>
												<th class="actions"><?php echo __('Acciones'); ?></th>
											</tr>
										</thead>
										<tbody>
											<?php for ($i=0; $i<3; $i++): ?>
											<tr class="odd gradeX">
												<td>
													<a href="#" class="glyphicons no-js <?php echo $configurations[$i]['Configuration']['icon'];?>"><i></i><?php echo h($configurations[$i]['Configuration']['name']); ?>&nbsp;</a>
												</td>
												<td><?php echo !empty($configurations[$i]['Configuration']['value']) ? h($configurations[$i]['Configuration']['value']) : "<i>No hay valor para este campo</i>"; ?>&nbsp;</td>
												<td class="actions">
													<?php 
														echo $this->Html->link(
															'<i class="icon-edit"></i> Editar', 
															array(
																'action' => 'edit', 
																$configurations[$i]['Configuration']['id'],
																Format::clean($configurations[$i]['Configuration']['name'])
															),
															array('class'=>'btn mini green-stripe', 'escape'=>false)
															); 
													?>
												</td>
											</tr>
											<?php endfor; ?>
										</tbody>
									</table>
									<!-- END CONTACT TABLE PORTLET-->
								</div>
								<div class="tab-pane" id="tab_3_2">
									<!-- BEGIN WEBSITE TABLE PORTLET-->
									<table class="table table-striped table-bordered table-advance table-hover">
										<thead>
											<tr>
												<th><?php echo 'Nombre'; ?></th>
												<th ><?php echo 'Valor'; ?></th>
												<th class="actions"><?php echo __('Acciones'); ?></th>
											</tr>
										</thead>
										<tbody>
											<?php for ($i=3; $i<6; $i++): ?>
											<tr class="odd gradeX">
												<td>
													<a href="#" class="glyphicons no-js <?php echo $configurations[$i]['Configuration']['icon'];?>"><i></i><?php echo h($configurations[$i]['Configuration']['name']); ?>&nbsp;</a>
												</td>
												<td><?php echo !empty($configurations[$i]['Configuration']['value']) ? h($configurations[$i]['Configuration']['value']) : "<i>No hay valor para este campo</i>"; ?>&nbsp;</td>
												<td class="actions">
													<?php 
														echo $this->Html->link(
															'<i class="icon-edit"></i> Editar', 
															array(
																'action' => 'edit', 
																$configurations[$i]['Configuration']['id'],
																Format::clean($configurations[$i]['Configuration']['name'])
															),
															array('class'=>'btn mini green-stripe', 'escape'=>false)
															); 
													?>
												</td>
											</tr>
											<?php endfor; ?>
										</tbody>
									</table>
									<!-- END WEBSITE TABLE PORTLET-->
								</div>
								<div class="tab-pane" id="tab_3_3">
									<!-- BEGIN SOCIALS TABLE PORTLET-->
									<table class="table table-striped table-bordered table-advance table-hover">
										<thead>
											<tr>
												<th><?php echo 'Nombre'; ?></th>
												<th ><?php echo 'Valor'; ?></th>
												<th class="actions"><?php echo __('Acciones'); ?></th>
											</tr>
										</thead>
										<tbody>
											<?php for ($i=6; $i<9; $i++): ?>
											<tr class="odd gradeX">
												<td>
													<a href="#" class="glyphicons no-js <?php echo $configurations[$i]['Configuration']['icon'];?>"><i></i><?php echo h($configurations[$i]['Configuration']['name']); ?>&nbsp;</a>
												</td>
												<td><?php echo !empty($configurations[$i]['Configuration']['value']) ? h($configurations[$i]['Configuration']['value']) : "<i>No hay valor para este campo</i>"; ?>&nbsp;</td>
												<td class="actions">
													<?php 
														echo $this->Html->link(
															'<i class="icon-edit"></i> Editar', 
															array(
																'action' => 'edit', 
																$configurations[$i]['Configuration']['id'],
																Format::clean($configurations[$i]['Configuration']['name'])
															),
															array('class'=>'btn mini green-stripe', 'escape'=>false)
															); 
													?>
												</td>
											</tr>
											<?php endfor; ?>
										</tbody>
									</table>
									<!-- END SOCIALS TABLE PORTLET-->
								</div>
							</div>
						</div>
						<!--END TABS-->
					</div>
					<div class="space10 visible-phone"></div>
				</div>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT-->