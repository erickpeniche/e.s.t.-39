<!DOCTYPE html>
<html lang="es">
<head>
	<?php echo $this->Html->charset(); ?>
	<title><?php echo Configure::read('App.configurations.website-title'); ?> | <?php echo isset($viewTitle) ? $viewTitle : ''; ?></title>
	<?php
		echo $this->Html->meta('favicon.ico', '/img/favicon.ico', array('type' => 'icon', 'rel' => 'icon'));

		//Template CSS's
		echo $this->Html->css('style');

		//Template JavaScript's
		echo $this->Html->script(array(
			'jquery',
			'superfish',
			'jquery.responsivemenu',
			'jquery.mobilemenu',
			'jquery.flexslider',
			'jquery.easing.1.3',
			'jquery.ui.totop',
			'script')
		);

		echo $this->fetch('script');
		echo $this->Js->writeBuffer();

		echo $this->fetch('meta');
		echo $this->fetch('css');
	?>
	<meta content="<?php echo Configure::read('App.configurations.website-description'); ?>" name="description" />
	<meta name="keywords" content="<?php echo Configure::read('App.configurations.website-keywords'); ?>">
	<meta content="Erick Peniche Castro" name="author" />
</head>
<body>
	<!--==============================header=================================-->
	<header>
		<div class="container_12">
			<div class="grid_12">
				<div class="top_block wrapper">
					<h1>
						<?php echo $this->Html->link('E.S.T. No. 39', array('controller' => 'pages', 'action' => 'home'), array('class' => 'logo')); ?>
					</h1>
					<div class="social">
						<a href="<?php echo Configure::read('App.configurations.social-networks-twitter'); ?>" title="Twitter"><?php echo $this->Html->image('soc1.jpg');?></a>
						<a href="<?php echo Configure::read('App.configurations.social-networks-facebook'); ?>" title="Facebook"><?php echo $this->Html->image('soc2.jpg');?></a>
					</div>
				</div>
				<?php echo $this->element('top_nav');?>
				<?php
					$this->startIfEmpty('slider');
					$this->end();
					echo $this->fetch('slider');
				?>

			</div>
			<div class="clear"></div>
		</div>
	</header>

	<!--==============================content================================-->
	<section id="content" class="<?php if(isset($home) || isset($about_us)){echo 'cont_pad';}?>">
		<div class="content_inner">
			<div class="container_12">
				<?php echo $this->Session->flash(); ?>
				<?php echo $this->fetch('content'); ?>
			</div>
		</div>
	</section>

	<!--==============================footer=================================-->
	<footer>
		<div class="container_12">
			<div class="wrapper">
				<article class="grid_12">
					<div class="wrapper">
						<div class="privacy">
							<?php echo $this->Html->image('new_f_logo.png');?>&nbsp;
							<span>&copy; <?php echo date("Y");?></span><br>
						</div>
						<div class="call">contacto@est39temozon.com</div>
					</div>
				</article>
			</div>
		</div>
	</footer>
	<?php echo $this->element('sql_dump'); ?>
	<script>
		jQuery(document).ready(function() {
			// initiate layout and plugins
			App.init();
		});
	</script>
</body>
</html>