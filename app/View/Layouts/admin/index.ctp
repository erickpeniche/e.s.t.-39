<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="es"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php
		$user = $this->Session->read('Auth.User');
		$userFullName = $user['first_name'].' '.$user['last_name'];
		$isSuperUser = AppController::isSuperUser($this->Session->read('Auth.User'));
	?>
	<?php echo $this->Html->charset('utf-8'); ?>
	<title>Panel de Administración | <?php echo Configure::read('App.configurations.website-title'); ?></title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="<?php echo Configure::read('App.configurations.website-description'); ?>" name="description" />
	<meta name="keywords" content="<?php echo Configure::read('App.configurations.website-keywords'); ?>">
	<meta content="Erick Peniche Castro" name="author" />

	<!-- Template CSS's -->
	<?php
		echo $this->Html->css(array(
			'admin/bootstrap/css/bootstrap',
			'admin/css/metro',
			'admin/bootstrap/css/bootstrap-responsive.min',
			'admin/font-awesome/css/font-awesome',
			'admin/fullcalendar/fullcalendar/bootstrap-fullcalendar',
			'admin/css/style',
			'admin/css/style_responsive'));
		echo $this->Html->css('admin/css/style_default', null, array('id'=>'style_color'));
		echo $this->Html->css(array(
			'admin/chosen-bootstrap/chosen/chosen.css',
			'admin/gritter/css/jquery.gritter',
			'admin/glyphicons/css/glyphicons',
			'admin/uniform/css/uniform.default'));

		echo $this->fetch('meta');
		echo $this->fetch('css');
	?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<?php
					echo $this->Html->link(
						$this->Html->image('admin/img/logo.png'),
						array('controller' => 'pages',
							'action' => 'home',
							'admin' => true),
						array('class' => 'brand',
							'escape' => false)
					);
				?>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
					<?php echo $this->Html->image('admin/img/menu-toggler'); ?>
				</a>
				<!-- END RESPONSIVE MENU TOGGLER -->
				<!-- BEGIN TOP NAVIGATION MENU -->
				<ul class="nav pull-right">
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<li class="dropdown user">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<?php
							$avatar = !empty($user['image']) ? 'images/avatar_'.$user['image'] : 'admin/img/avatar_small' ;
							echo $this->Html->image($avatar);
						?>
						<span class="username"><?php echo $userFullName;?></span>
						<i class="icon-angle-down"></i>
						</a>
						<ul class="dropdown-menu">
							<li>
								<?php
									echo $this->Html->link(
										'<i class="icon-user-md"></i> Mi cuenta',
										array('controller' => 'users',
											'action' => 'view',
											'admin' => true,
											$user['id'],
											Format::clean($user['first_name']. ' ' .$user['last_name'])
											),
										array('escape' => false)
									);
								?>
							</li>
							<li class="divider"></li>
							<li>
								<?php
									echo $this->Html->link(
										'<i class="icon-signout"></i> Cerrar sesión',
										array('controller' => 'users',
											'action' => 'logout',
											'admin' => false),
										array('escape' => false)
									);
								?>
							</li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
				<!-- END TOP NAVIGATION MENU -->
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<?php echo $this->element('admin/sidebar_menu', array('activeMenu' => empty($activeMenu) ? NULL : $activeMenu, 'isSuperUser' => $isSuperUser)); ?>
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<?php echo $this->element('admin/content_header');?>
				<?php echo $this->Session->flash(); ?>
				<?php echo $this->fetch('content'); ?>
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php echo date('Y') ?> &copy;
		<div class="span pull-right">
			<span class="go-top"><i class="icon-angle-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->

	<!-- Template JavaScript's -->
	<?php
		echo $this->Html->script(array(
			'admin/js/jquery-1.8.3.min',
			'admin/breakpoints/breakpoints',
			'admin/jquery-slimscroll/jquery-ui-1.9.2.custom.min',
			'admin/bootstrap/js/bootstrap.min',
			'admin/js/jquery.blockui',
			'admin/js/jquery.cookie',
			'admin/fullcalendar/fullcalendar/fullcalendar.min',
			'admin/uniform/jquery.uniform.min',
			'admin/gritter/js/jquery.gritter',
			'admin/chosen-bootstrap/chosen/chosen.jquery.min',
			'admin/fancybox/source/jquery.fancybox.pack',
			'admin/js/app')
		);

		echo $this->fetch('script');
		echo $this->Js->writeBuffer();
	?>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script>
		jQuery(document).ready(function() {
			// initiate layout and plugins
			App.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
