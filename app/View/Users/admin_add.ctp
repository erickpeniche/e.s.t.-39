<?php
	echo $this->Html->css(array(
		'admin/bootstrap-fileupload/bootstrap-fileupload',
		'admin/gritter/css/jquery.gritter',
		'admin/jquery-tags-input/jquery.tagsinput',
		'admin/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons',
		'admin/data-tables/DT_bootstrap'
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		'admin/bootstrap-fileupload/bootstrap-fileupload',
		'admin/jquery-tags-input/jquery.tagsinput.min',
		'admin/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons',
		'admin/jquery-validation/dist/jquery.validate.min',
		'admin/jquery-validation/dist/additional-methods.min'
		), array('inline' => false)
	);

	$this->set('activeMenu', 'users');

	$this->Html->addCrumb('Usuarios',
		array(
			'action' => 'index',
			'controller' => 'users',
			'admin' => true
			)
		);

	$this->Html->addCrumb('Nuevo',
		array(
			'action' => 'add',
			'controller' => 'users',
			'admin' => true
			)
		);
?>
<div class="portlet box blue">
	<div class="portlet-title">
		<h4><i class="icon-user"></i>Nuevo Usuario</h4>
		<div class="actions">
			<?php echo $this->Html->link('<i class="icon-user"></i> Usuarios</a>',
				array(
					'action' => 'index',
					'controller' => 'users',
					'admin' => true
					),
				array(
					'class' => 'btn blue',
					'escape' => false)
				);
			?>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('User',
			array(
				'inputDefaults' => array(
					'div' => false,
					'class' => 'm-wrap span8'
					),
				'class' => 'form-horizontal form-row-seperated',
				'enctype' => 'multipart/form-data',
				'novalidate' => true
				)
			);
		?>

		<div class="control-group <?php echo isset($this->validationErrors['User']['first_name']) ? 'error' : ''; ?>">
			<?php
				echo $this->Form->input('first_name',
					array(
						'between' => '<div class="controls input-icon">',
						'label' => array('text'=>'Nombre<span class="required">*</span>', 'class'=>'control-label'),
						'placeholder'=>'Nombre',
						'error' => array(
							'attributes' => array('escape' => false, 'wrap' => false)
							)
						)
					);
				echo '</div>';
			?>
		</div>

		<div class="control-group <?php echo isset($this->validationErrors['User']['last_name']) ? 'error' : ''; ?>">
			<?php
				echo $this->Form->input('last_name',
					array(
						'between' => '<div class="controls input-icon">',
						'label' => array('text'=>'Apellido<span class="required">*</span>', 'class'=>'control-label'),
						'placeholder'=>'Apellido',
						'error' => array(
							'attributes' => array('escape' => false, 'wrap' => false)
							)
						)
					);
				echo '</div>';
			?>
		</div>

		<div class="control-group <?php echo isset($this->validationErrors['User']['email']) ? 'error' : ''; ?>">
			<?php
				echo $this->Form->input('email',
					array(
						'between' => '<div class="controls input-icon">',
						'label' => array('text'=>'Correo electrónico<span class="required">*</span>', 'class'=>'control-label'),
						'placeholder'=>'ejemplo@mail.com',
						'error' => array(
							'attributes' => array('escape' => false, 'wrap' => false)
							)
						)
					);
				echo '</div>';
			?>
		</div>

		<div class="control-group <?php echo isset($this->validationErrors['User']['passwd']) ? 'error' : ''; ?>">
			<?php
				echo $this->Form->input('passwd',
					array(
						'between' => '<div class="controls input-icon">',
						'class'=>'m-wrap span3',
						'label' => array('text'=>'Contraseña<span class="required">*</span>', 'class'=>'control-label'),
						'error' => array(
							'attributes' => array('escape' => false, 'wrap' => false)
							)
						)
					);
				echo '</div>';
			?>
		</div>

		<div class="control-group <?php echo isset($this->validationErrors['User']['passwd_confirm']) ? 'error' : ''; ?>">
			<?php
				echo $this->Form->input('passwd_confirm',
					array(
						'between' => '<div class="controls input-icon">',
						'class'=>'m-wrap span3',
						'type'=>'password',
						'label' => array('text'=>'Confirmar contraseña<span class="required">*</span>', 'class'=>'control-label'),
						'error' => array(
							'attributes' => array('escape' => false, 'wrap' => false)
							)
						)
					);
				echo '</div>';
			?>
		</div>

		<div class="control-group">
			<?php
				$roles = array('Super User' => 'Super Usuario', 'Administrator' => 'Administrador');
				echo $this->Form->input('role',
					array(
						'between' => '<div class="controls input-icon">',
						'options' => $roles,
						'default' => 'Administrator',
						'class'=>'m-wrap span3',
						'label' => array('text'=>'Rol<span class="required">*</span>', 'class'=>'control-label')
						)
					);
				echo '</div>';
			?>
		</div>

		<div class="control-group required">
			<label for="UserActive" class="control-label">Activo</label>
			<div class="controls">
				<div class="success-toggle-button">
					<input type="checkbox" id="UserActive" class="toggle" name="data[User][active]" checked="checked" />
				</div>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Imagen</label>
			<div class="controls">
				<div class="fileupload fileupload-new" data-provides="fileupload">
					<div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
						<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=sin+imagen" alt="" />
					</div>
					<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
					<div>
						<span class="btn btn-file"><span class="fileupload-new">Seleccionar</span>
						<span class="fileupload-exists">Cambiar</span>
						<input type="file" name="data[User][image]" class="default" id="MainImageImage"></span>
						<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Eliminar</a>
					</div>
				</div>
			</div>
		</div>

		<div class="form-actions">
			<?php
				echo $this->Form->button('<i class="icon-ok"></i> Guardar',
					array('type' => 'submit', 'class' => 'btn green')
					);
				echo '&nbsp;';

				$cancelOptions = array('action' => 'index', 'controller' => 'users', 'admin' => true);
				echo $this->Form->button('Cancelar',
					array(
						'type' => 'button',
						'class' => 'btn red',
						'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
						)
					);
			?>
		</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>