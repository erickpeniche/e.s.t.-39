<!-- BEGIN LOGIN FORM -->
<?php
  echo $this->Form->create(
    'User',
    array(
      'action' => 'login',
      'class' => 'form-vertical login-form',
      'novalidate'=>true,
      'inputDefaults'=> array('label'=>false, 'div'=>false)
    )
  );
?>
<!-- Usuario -->
<div class="control-group">
  <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
  <label class="control-label visible-ie8 visible-ie9">Correo Electrónico</label>
  <div class="controls">
    <div class="input-icon left">
      <i class="icon-envelope"></i>
      <?php echo $this->Form->input('email', array('placeholder'=>'Correo Electrónico', 'class'=>'m-wrap placeholder-no-fix')); ?>
    </div>
  </div>
</div>
<!-- Password -->
<div class="control-group">
  <label class="control-label visible-ie8 visible-ie9">Contraseña</label>
  <div class="controls">
    <div class="input-icon left">
      <i class="icon-key"></i>
      <?php echo $this->Form->input('password', array('placeholder'=>'Contraseña', 'class'=>'m-wrap placeholder-no-fix')); ?>
    </div>
  </div>
</div>
<!-- Remember me checkbox and submit button -->
<div class="form-actions">
  <?php
    echo $this->Form->button('Iniciar '.'<i class="m-icon-swapright m-icon-white"></i>', array('type'=> 'submit', 'class'=>'btn green pull-right'));
    echo $this->Form->end();
  ?>
</div>
