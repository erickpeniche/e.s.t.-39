<?php
	echo $this->Html->css(array(
		'admin/fancybox/source/jquery.fancybox',
		'admin/data-tables/DT_bootstrap',
		), null, array('inline' => false)
	);

	$this->set('activeMenu', 'users');

	$this->Html->addCrumb('Usuarios',
		array(
			'action' => 'index',
			'controller' => 'users',
			'admin' => true
			)
		);
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
	<div class="span12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box light-grey">
			<div class="portlet-title">
				<h4><i class="icon-user"></i>Usuarios</h4>
				<div class="actions">
					<?php echo $this->Html->link('<i class="icon-plus"></i> Nuevo</a>',
						array(
							'action' => 'add',
							'controller' => 'users'
							),
						array(
							'class' => 'btn green',
							'escape' => false)
						);
					?>
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover" id="sample_3">
					<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('first_name', 'Nombre'); ?></th>
							<th><?php echo $this->Paginator->sort('last_name', 'Apellido'); ?></th>
							<th><?php echo $this->Paginator->sort('role', 'Rol'); ?></th>
							<th><?php echo $this->Paginator->sort('email', 'E-mail'); ?></th>
							<th><?php echo $this->Paginator->sort('active', 'Estado'); ?></th>
							<th><?php echo $this->Paginator->sort('created', 'Creado'); ?></th>
							<th><?php echo $this->Paginator->sort('modified', 'Modificado'); ?></th>
							<th class="actions"><?php echo __('Acciones'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($users as $user): ?>
						<tr class="odd gradeX">
							<td><?php echo h($user['User']['first_name']); ?>&nbsp;</td>
							<td><?php echo h($user['User']['last_name']); ?>&nbsp;</td>
							<td><?php echo h($user['User']['role']); ?>&nbsp;</td>
							<td><?php echo h($user['User']['email']); ?>&nbsp;</td>
							<td>
								<?php
									if($user['User']['active']==1){
										echo '<span class="label label-success">Activo</span>';
									}else{
										echo '<span class="label label-important">Inactivo</span>';
									}
								?>
							</td>
							<td><?php echo h($user['User']['created']); ?>&nbsp;</td>
							<td><?php echo h($user['User']['modified']); ?>&nbsp;</td>
							<td class="actions">
								<?php echo $this->Html->link('<i class="icon-info-sign"></i> Ver', array('action' => 'view', $user['User']['id'], Format::clean($user['User']['first_name']. ' ' .$user['User']['last_name'])), array('class'=>'btn mini blue', 'escape'=>false)); ?>
								<?php echo $this->Html->link('<i class="icon-edit"></i> Editar', array('action' => 'edit', $user['User']['id'], Format::clean($user['User']['first_name']. ' ' .$user['User']['last_name'])), array('class'=>'btn mini grey', 'escape'=>false)); ?>
								<?php echo $loggedUser['id'] != $user['User']['id'] ? $this->Form->postLink('<i class="icon-trash"></i> Eliminar', array('action' => 'delete', $user['User']['id']), array('class'=>'btn mini red', 'escape'=>false), __('Confirma que desea eliminar a %s?', $user['User']['first_name']. ' ' .$user['User']['last_name'])) : ''; ?>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT-->
