<?php
	echo $this->Html->css(array(
	   'admin/bootstrap-fileupload/bootstrap-fileupload',
	   'admin/gritter/css/jquery.gritter',
	   'admin/jquery-tags-input/jquery.tagsinput',
	   'admin/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons',
	   'admin/data-tables/DT_bootstrap'
	   ), null, array('inline' => false)
	);

	echo $this->Html->script(array(
	   'admin/bootstrap-fileupload/bootstrap-fileupload',
	   'admin/jquery-tags-input/jquery.tagsinput.min',
	   'admin/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons',
	   'admin/View/Users/admin_edit'
	   ), array('inline' => false)
	);

	$img = isset($user['User']['image']) && !empty($user['User']['image']) ? '/img/images/user_img_edt_'.$user['User']['image'] : 'http://www.placehold.it/291x170/EFEFEF/AAAAAA&amp;text=Sin+imagen+de+perfil';

	$this->set('activeMenu', 'users');

	$this->Html->addCrumb('Usuarios',
		array(
			'action' => 'index',
			'controller' => 'users',
			'admin' => true
			)
		);

	$this->Html->addCrumb('Editar', null);

	$this->Html->addCrumb($user['User']['first_name']. ' ' . $user['User']['last_name'],
		array(
			'action' => 'edit',
			'controller' => 'users',
			$user['User']['id'],
			Format::clean($user['User']['first_name']. ' ' .$user['User']['last_name']),
			'admin' => true
			)
		);
?>
<div class="row-fluid">
	<div class="span1 offset11">
		<div class="actions">
			<?php echo $this->Html->link('<i class="icon-user"></i> Usuarios</a>',
				array(
					'action' => 'index',
					'controller' => 'users',
					'admin' => true
					),
				array(
					'class' => 'btn blue',
					'escape' => false)
				);
			?>
		</div>
	</div>
</div>
<div class="row-fluid profile-account">
	<div class="row-fluid">
		<div class="span12">
			<div class="span3">
				<ul class="ver-inline-menu tabbable margin-bottom-10">
					<li class="active">
						<a data-toggle="tab" href="#tab_1-1">
						<i class="icon-cog"></i>
						Información Personal
						</a>
						<span class="after"></span>
					</li>
					<?php if($ownAccount): ?>
					<li class=""><a data-toggle="tab" href="#tab_2-2"><i class="icon-picture"></i> Cambiar imagen</a></li>
					<li class=""><a data-toggle="tab" href="#tab_3-3"><i class="icon-lock"></i> Cambiar contraseña</a></li>
					<?php endif; ?>
				</ul>
			</div>
			<div class="span9">
				<div class="tab-content">
					<div id="tab_1-1" class="tab-pane active">
						<div style="height: auto;" id="accordion1-1" class="accordion collapse">
							<?php echo $this->Form->create('User',
								array(
									'inputDefaults' => array(
										'div' => false,
										'label' => array('class' => 'control-label'),
										'class'=> 'm-wrap span8'
										),
									'novalidate' => true,
									'class'=>'submit-basic'
									)
								);
							?>
							<?php echo $this->Form->input('id', array('type' => 'hidden')); ?>
							<div class="control-group <?php echo isset($this->validationErrors['User']['first_name']) ? 'error' : ''; ?>">
								<?php
									echo $this->Form->input('first_name',
										array(
											'between' => '<div class="controls input-icon">',
											'label' => array('text'=>'Nombre<span class="required">*</span>', 'class'=>'control-label'),
											'error' => array(
												'attributes' => array('escape' => false, 'wrap' => false)
												)
											)
										);
									echo '</div>';
								?>
							</div>

							<div class="control-group <?php echo isset($this->validationErrors['User']['last_name']) ? 'error' : ''; ?>">
								<?php
									echo $this->Form->input('last_name',
										array(
											'between' => '<div class="controls input-icon">',
											'label' => array('text'=>'Apellido<span class="required">*</span>', 'class'=>'control-label'),
											'error' => array(
												'attributes' => array('escape' => false, 'wrap' => false)
												)
											)
										);
									echo '</div>';
								?>
							</div>

							<div class="control-group <?php echo isset($this->validationErrors['User']['email']) ? 'error' : ''; ?>">
								<?php
									echo $this->Form->input('email',
										array(
											'between' => '<div class="controls input-icon">',
											'label' => array('text'=>'E-mail<span class="required">*</span>', 'class'=>'control-label'),
											'error' => array(
												'attributes' => array('escape' => false, 'wrap' => false)
												)
											)
										);
									echo '</div>';
								?>
							</div>
							<?php if($isSuperUser): ?>
							<div class="control-group">
								<label for="UserActive" class="control-label">Activo</label>
								<div class="controls">
									<div class="success-toggle-button">
										<?php
											echo $this->Form->input('active',
												array(
													'type' => 'checkbox',
													'class' => 'toggle',
													'label' => false
												)
											);
										?>
									</div>
								</div>
							</div>

							<div class="control-group">
								<?php
									$roles = array('Super User' => 'Super Usuario', 'Administrator' => 'Administrador');
									echo $this->Form->input('role',
										array(
											'between' => '<div class="controls input-icon">',
											'options' => $roles,
											'default' => 'Administrator',
											'class'=>'m-wrap span3',
											'label' => array('text'=>'Rol<span class="required">*</span>', 'class'=>'control-label')
											)
										);
									echo '</div>';
								?>
							</div>
							<?php endif; ?>

							<div class="submit-btn">
								<?php
								echo $this->Form->button('<i class="icon-ok"></i> Guardar',
									array('type' => 'submit', 'class' => 'btn green save')
									);
								echo '&nbsp;';

								$cancelOptions = array('action' => 'index', 'controller' => 'users', 'admin' => true);
								echo $this->Form->button('Cancelar',
									array(
										'type' => 'button',
										'class' => 'btn red',
										'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
										)
									);
								?>
							</div>
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
					<?php if($ownAccount): ?>
					<div id="tab_2-2" class="tab-pane">
						<div style="height: auto;" id="accordion2-2" class="accordion collapse">
							<?php
								echo $this->Form->create('User',
									array(
										'inputDefaults' => array(
											'div' => false,
											'label' => false,
											'class'=> 'm-wrap span8'
										),
										'enctype' => 'multipart/form-data',
										'novalidate' => true,
										'class'=>'submit-profile-pic'
									)
								);
								echo $this->Form->input('id', array('type' => 'hidden'));
							?>
							<div class="controls">
								<div class="fileupload fileupload-new" data-provides="fileupload">
									<div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
										<?php
											$img = isset($user['User']['image']) && !empty($user['User']['image']) ? 'images/user_img_edt_'.$user['User']['image'] : 'http://www.placehold.it/300x250/EFEFEF/AAAAAA&amp;text=Sin+imagen+de+perfil';
										?>
										<?php echo $this->Html->image($img); ?>
									</div>
									<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">
									</div>
									<div>
										<span class="btn btn-file">
											<span class="fileupload-new">Seleccionar</span>
											<span class="fileupload-exists">Cambiar</span>
											<?php echo $this->Form->input('image', array('type'=>'file', 'class'=>'default')); ?>
										</span>
										<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Eliminar</a>
									</div>
								</div>
								<span class="label label-important">NOTA!</span>
								<span>La imagen debe pesar menos de <strong>20 MB</strong></span>
							</div>
								<div class="clearfix"></div>
								<div class="space10"></div>
								<?php
									echo '<div class="submit-btn">';
									echo $this->Form->button('<i class="icon-ok"></i> Guardar',
									   array('type' => 'submit', 'class' => 'btn green save')
									   );
									echo '&nbsp;';

									$cancelOptions = array('action' => 'index', 'controller' => 'users', 'admin' => true);
									echo $this->Form->button('Cancelar',
									   array(
									      'type' => 'button',
									      'class' => 'btn red',
									      'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
									      )
									   );
									echo '</div>';
								?>
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
					<div id="tab_3-3" class="tab-pane">
						<div style="height: auto;" id="accordion3-3" class="accordion collapse">
							<?php
								echo $this->Form->create('User',
								   array(
								      'inputDefaults' => array(
								         'div' => false,
								         'label' => array('class' => 'control-label'),
								         'class'=> 'm-wrap span8'
								         ),
								      'novalidate' => true
								      )
								   );
							?>
							<?php echo $this->Form->input('id', array('type' => 'hidden', 'value'=>$user['User']['id'])); ?>
							<div class="control-group <?php echo isset($this->validationErrors['User']['pass']) ? 'error' : ''; ?>">
								<?php
									echo $this->Form->input('pass',
										array(
											'between' => '<div class="controls input-icon">',
											'label' => array('text'=>'Contraseña actual', 'class'=>'control-label'),
											'type' => 'password',
											'error' => array(
												'attributes' => array('escape' => false, 'wrap' => false)
												)
											)
										);
									echo '</div>';
								?>
							</div>

							<div class="control-group <?php echo isset($this->validationErrors['User']['passwd']) ? 'error' : ''; ?>">
								<?php
									echo $this->Form->input('passwd',
										array(
											'between' => '<div class="controls input-icon">',
											'label' => array('text'=>'Nueva contraseña', 'class'=>'control-label'),
											'type' => 'password',
											'error' => array(
												'attributes' => array('escape' => false, 'wrap' => false)
												)
											)
										);
									echo '</div>';
								?>
							</div>

							<div class="control-group <?php echo isset($this->validationErrors['User']['passwd_confirm']) ? 'error' : ''; ?>">
								<?php
									echo $this->Form->input('passwd_confirm',
										array(
											'between' => '<div class="controls input-icon">',
											'label' => array('text'=>'Confirmar nueva contraseña', 'class'=>'control-label'),
											'type' => 'password',
											'error' => array(
												'attributes' => array('escape' => false, 'wrap' => false)
												)
											)
										);
									echo '</div>';
								?>
							</div>

								<?php
									echo '<div class="submit-btn">';
									echo $this->Form->button('<i class="icon-ok"></i> Guardar',
									   array('type' => 'submit', 'class' => 'btn green', 'id'=>'submit-basic')
									   );
									echo '&nbsp;';

									$cancelOptions = array('action' => 'index', 'controller' => 'users', 'admin' => true);
									echo $this->Form->button('Cancelar',
									   array(
									      'type' => 'button',
									      'class' => 'btn red',
									      'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
									      )
									   );
									echo '</div>';
								?>
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
					<?php endif; ?>
				</div>
			</div>
			<!--end span9-->
		</div>
	</div>
</div>

<?php
$successMessage = '$.gritter.add({
						title: '."'Éxito!'".',
						text: '."'El usuario ha sido editado.'".',
						image: '."'/img/icons/check.png'".',
						sticky: false,
						time: '."''".'
					});';
$errorMessage = '
$.gritter.add({
	title: '."'Error!'".',
	text: '."'El usuario no ha podido ser editado.\\nError: '+XMLHttpRequest.responseText".',
	image: '."'/img/icons/no.png'".',
	sticky: false,
	time: '."''".'
});
console.log(XMLHttpRequest);
';

/**
 * Form: Basic personal information
 * Form Class: .submit-basic
 */
$formBasic = $this->Js->get('.submit-basic');
$data = $formBasic->serializeForm(array('isForm' => true, 'inline' => true));

$formBasic->event(
	'submit',
	$this->Js->request(
		array('controller' => 'users', 'action' => 'edit', 'admin' => true, $user['User']['id']),
		array(
			'data' => $data,
			'success' => $successMessage,
			'error' => $errorMessage,
			'async' => true,
			'dataExpression'=>true,
			'method' => 'POST'
		)
	)
);
?>