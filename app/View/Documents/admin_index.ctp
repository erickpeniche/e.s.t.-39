<?php
	echo $this->Html->css(array(
		'admin/fancybox/source/jquery.fancybox',
		'admin/data-tables/DT_bootstrap',
		), null, array('inline' => false)
	);

	$this->set('activeMenu', 'documents');

	$this->Html->addCrumb('Planes y Programas',
		array(
			'action' => 'index',
			'controller' => 'documents',
			'admin' => true
			)
		);
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
	<div class="span12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box light-grey">
			<div class="portlet-title">
				<h4><i class="icon-book"></i>Planes y Programas</h4>
				<div class="actions">
					<?php echo $this->Html->link('<i class="icon-plus"></i> Nuevo</a>',
						array(
							'action' => 'add',
							'controller' => 'documents'
							),
						array(
							'class' => 'btn green',
							'escape' => false)
						);
					?>
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover" id="sample_3">
					<thead>
						<tr>
							<th width="10%">Tipo</th>
							<th width="20%"><?php echo $this->Paginator->sort('name', 'Nombre'); ?></th>
							<th width="10%"><?php echo $this->Paginator->sort('type', 'Categoría'); ?></th>
							<th width="15%"><?php echo $this->Paginator->sort('created', 'Fecha de creación'); ?></th>
							<th width="15%"><?php echo $this->Paginator->sort('modified', 'Fecha de modificación'); ?></th>
							<th class="actions" width="30%"><?php echo __('Acciones'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php if (empty($documents)) : ?>
						<tr>
							<td colspan="6">
								<div class="alert alert-block alert-info fade in">
									<button type="button" class="close" data-dismiss="alert"></button>
									<h4 class="alert-heading">No hay Planes o Programas!</h4>
									<p>
										Aún no se han agregado planes o programas. Haz click en el botón 'Nuevo' para agregar un nuevo plan o programa.
									</p>
								</div>
							</td>
						</tr>
						<?php else: ?>
						<?php foreach ($documents as $document): ?>
						<?php
							switch ($document['Document']['extension']) {
								case 'doc':
									$img = $this->Html->image('icons/doc.png');
									break;

								case 'docx':
									$img = $this->Html->image('icons/doc.png');
									break;

								case 'pdf':
									$img = $this->Html->image('icons/pdf.png');
									break;

								case 'ppt':
									$img = $this->Html->image('icons/ppt.png');
									break;

								case 'pptx':
									$img = $this->Html->image('icons/ppt.png');
									break;

								case 'xls':
									$img = $this->Html->image('icons/xls.png');
									break;

								case 'txt':
									$img = $this->Html->image('icons/txt.png');
									break;
							}
						?>
						<tr class="odd gradeX">
							<td><?php echo $img; ?></td>
							<td>
								<?php
									echo $this->Text->truncate(strip_tags($document['Document']['name']), 100, array('ellipsis' => '...', 'exact' => false));
								?>
								&nbsp;
							</td>
							<td><?php echo $document['Document']['type']=='Plan' ? 'Plan' : 'Programa'; ?></td>
							<td><?php echo $document['Document']['created']; ?>&nbsp;</td>
							<td><?php echo $document['Document']['modified']; ?>&nbsp;</td>
							<td class="actions">
								<?php
									echo $this->Html->link('<i class="icon-download-alt"></i> Descargar',
										array(
											'controller'=>'documents',
											'action'=>'download',
											$document['Document']['id'],
											'admin'=>false
										),
										array('class'=>'btn mini green', 'escape'=>false)
									);
								?>
								<?php echo $this->Html->link('<i class="icon-edit"></i> Editar', array('action' => 'edit', $document['Document']['id'], Format::clean($document['Document']['name'])), array('class'=>'btn mini grey', 'escape'=>false)); ?>
								<?php echo $this->Form->postLink('<i class="icon-trash"></i> Eliminar', array('action' => 'delete', $document['Document']['id']), array('class'=>'btn mini red', 'escape'=>false), __('¿Confirma que desea eliminar este documento?')); ?>
							</td>
						</tr>
						<?php endforeach; ?>
						<?php endif; ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT-->
