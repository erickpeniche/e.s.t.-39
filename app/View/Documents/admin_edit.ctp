<?php
	echo $this->Html->css(array(
		'admin/bootstrap-fileupload/bootstrap-fileupload',
		'admin/gritter/css/jquery.gritter',
		'admin/jquery-tags-input/jquery.tagsinput',
		'admin/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons',
		'admin/data-tables/DT_bootstrap'
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		'admin/bootstrap-fileupload/bootstrap-fileupload',
		'admin/jquery-tags-input/jquery.tagsinput.min',
		'admin/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons',
		'admin/jquery-validation/dist/jquery.validate.min',
		'admin/jquery-validation/dist/additional-methods.min'
		), array('inline' => false)
	);

	$this->set('activeMenu', 'documents');

	$this->Html->addCrumb('Planes y Programas',
		array(
			'action' => 'index',
			'controller' => 'documents',
			'admin' => true
			)
		);

	$document = $this->request->data;
	$this->Html->addCrumb($document['Document']['name'], null);

	$this->Html->addCrumb('Editar', null);
?>
<div class="portlet box blue">
	<div class="portlet-title">
		<h4><i class="icon-book"></i>Editar Plan o Programa</h4>
		<div class="actions">
			<?php echo $this->Html->link('<i class="icon-book"></i> Planes y Programas</a>',
				array(
					'action' => 'index',
					'controller' => 'documents',
					'admin' => true
					),
				array(
					'class' => 'btn blue',
					'escape' => false)
				);
			?>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('Document',
			array(
				'inputDefaults' => array(
					'div' => false,
					'class' => 'm-wrap span8'
					),
				'class' => 'form-horizontal form-row-seperated',
				'enctype' => 'multipart/form-data',
				'novalidate' => true
				)
			);
			echo $this->Form->input('id', array('hiddenField' => true));
		?>

		<div class="control-group <?php echo isset($this->validationErrors['Document']['name']) ? 'error' : ''; ?>">
			<?php
				echo $this->Form->input('name',
					array(
						'between' => '<div class="controls input-icon">',
						'label' => array('text'=>'Nombre<span class="required">*</span>', 'class'=>'control-label'),
						'placeholder'=>'Nombre',
						'error' => array(
							'attributes' => array('escape' => false, 'wrap' => false)
							)
						)
					);
				echo '</div>';
			?>
		</div>

		<div class="control-group">
			<?php
				$types = array('Plan' => 'Plan', 'Program' => 'Programa');
				echo $this->Form->input('type',
					array(
						'between' => '<div class="controls input-icon">',
						'options' => $types,
						'default' => 'Plan',
						'class'=>'m-wrap span3',
						'label' => array('text'=>'Tipo<span class="required">*</span>', 'class'=>'control-label')
						)
					);
				echo '</div>';
			?>
		</div>
		<?php 
		//debug($this->validationErrors);
		//exit(); 
		?>
		<div class="control-group <?php echo isset($this->validationErrors['Document']['file']) ? 'error' : ''; ?>">
			<label class="control-label">Documento<span class="required">*</span></label>
			<div class="controls">
				<div class="fileupload fileupload-new" data-provides="fileupload">
					<div class="input-append">
						<div class="uneditable-input">
							<i class="icon-file fileupload-exists"></i>
							<span class="fileupload-preview"></span>
						</div>
						<span class="btn btn-file">
							<span class="fileupload-new">Seleccionar</span>
							<span class="fileupload-exists">Cambiar</span>
							<?php
								echo $this->Form->input('file',
									array(
										'label'=>false,
										'div'=>false,
										'type'=>'file'
									),
									array('class'=>'default')
								);
							?>
						</span>
						<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remover</a>
					</div>
					<?php echo isset($this->validationErrors['Document']['file']) ? '<span class="help-inline">Seleccione un archivo válido</span>' : ''; ?>
				</div>
				<span class="label label-important">NOTA!</span>
				<span>Los archivos admitidos son: <strong>PDF, Word, PowerPoint y Excel</strong></span>
			</div>
		</div>

		<div class="form-actions">
			<?php
				echo $this->Form->button('<i class="icon-ok"></i> Guardar',
					array('type' => 'submit', 'class' => 'btn green')
					);
				echo '&nbsp;';

				$cancelOptions = array('action' => 'index', 'controller' => 'documents', 'admin' => true);
				echo $this->Form->button('Cancelar',
					array(
						'type' => 'button',
						'class' => 'btn red',
						'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
						)
					);
			?>
		</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>