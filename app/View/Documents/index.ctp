<div class="wrapper">
	<article class="grid_12">
		<div class="events_box">
			<h2 class="ind">Planes y programas</h2>
			<div class="wrapper">
				<div class="grid_12">
					<ul class="list1">
						<?php foreach($documents as $index => $document): ?>
						<li class="<?php echo $index == count($document)-1 ? 'last' : ''; ?>"><?php echo $this->Html->link($document['Document']['name'], array('controller' => 'documents', 'action'=>'download', $document['Document']['id'])); ?></li>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>
		</div>
	</article>
</div>