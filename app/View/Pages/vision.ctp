<div class="wrapper">
	<article class="grid_12">
		<h2>NUESTRA MISIÓN</h2>
		<p style="text-indent: 3em;">Esperamos romper con el paradigma tradicional de inteligencia como única y general, la cual señala su condición de pluralidad y permite reivindicar la condición humana con relación a sus múltiples capacidades de cognición.
		</p><br>
		<h2>NUESTRA VISIÓN</h2>
		<p style="text-indent: 3em;">Brindar un servicio de excelencia y modernidad educativa, orientando a la formación inicial de alumnos para aportar a la sociedad, basada a plenitud de valores y virtudes que posibiliten elaborar sus propios proyectos de vida y desarrollar diferentes habilidades.</p>
	</article>
</div>