<div class="wrapper">
	<div class="grid_12">
		<div class="facts">
			<h2>Servicio Social y Trabajo Social</h2>
			<div class="wrapper">
				<div class="grid_12 alpha">
					<ul class="list2">
						<li>
							<strong class="white">Meta</strong>
							<br>
							<p>Siendo el alumno el actor principal del proceso de “enseñanza-aprendizaje”, lograr el conocimiento de sus necesidades, de su comunidad escolar y de la sociedad en la que vive.</p>
						</li>
						<li>
							<strong class="white">Objetivo</strong>
							<br>
							<p>Promover el desarrollo integral del alumno para que, de acuerdo a su personalidad, se desenvuelva en el ambiente familiar y social, de una manera plena, equilibrada y constructiva.</p>
						</li>
						<li>
							<strong class="white">Funciones</strong>
							<br>
							<p>Según la “Organización y funcionamiento de los servicios educativos complementarios en las escuelas secundarias técnicas” (SEP, 2012), Capítulo III, Artículo 12º, apartado I, las funciones del trabajador social escolar son:</p>
							<ul class="list1">
								<li>Colaborar en la identificación de situaciones de riesgo</li>
								<li>Participar en y para la elaboración, ejecución y evaluación de programas preventivos</li>
								<li>Hacer el llenado del registro de documentación de becas</li>
								<li>Establecer mecanismos de vinculación extraescolar</li>
								<li>Propiciar acciones de participación de la comunidad escolar y su entorno, a fin de prevenir problemas que obstaculicen el proceso educativo</li>
								<li>Realizar visitas domiciliarias</li>
								<li>Efectuar el estudio socioeconómico de los alumnos que requieran atención individualizada</li>
								<li>En ausencia de algún docente, atender a los alumnos del grupo correspondiente</li>
							</ul>
						</li>
					</ul><br>
					<span><strong>Responsable: </strong>LIC. FÁTIMA DEL ROSARIO PACAB ANGUAS</span>
				</div>
			</div>
		</div>
	</div>
</div>