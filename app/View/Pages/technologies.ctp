<div class="wrapper">
	<div class="grid_12">
		<div class="facts">
			<h2>Tecnologías</h2>
			<div class="wrapper">
				<div class="grid_12 alpha">
					<p style="text-indent: 3em;">La asignatura de Tecnología pretende promover una visión amplia del campo de estudio al considerar los aspectos instrumentales de las técnicas, sus procesos de cambio, gestión e innovación y su relación con la sociedad y la naturaleza; además, recurre a la participación social en el uso, creación y mejora de los productos técnicos, así como de las implicaciones de éstos en el entorno.</p>
					<ul class="list2">
						<li>
							<strong class="white">Técnica</strong>
							<br>
							<p>Es el proceso de creación de medios o acciones instrumentales, estratégicas y de control para satisfacer necesidades e intereses; incluye formas de organización y gestión, así como procedimientos para utilizar herramientas, instrumentos y máquinas.</p>
						</li>
						<li>
							<strong class="white">La tecnología,</strong>
							<br>
							<p>por su parte, se entiende como el campo encargado del estudio de la técnica, así como de la reflexión sobre los medios, las acciones y sus interacciones con los contextos natural y social.</p>
						</li>
						<li>
							<strong class="white">En la asignatura de Tecnología</strong>
							<br>
							<p>se concibe como un espacio integrador de saberes, en tanto se interrelacionan con diferentes aspectos de la técnica, la naturaleza y la sociedad.( El saber, El saber hacer y El saber ser).</p>
						</li>
					</ul>
					<p style="text-indent: 3em;">La adquisición de estos saberes busca alcanzar el Perfil de Egreso de la Educación Básica y agregar valor y posibilidades al proceso educativo mediante la articulación de contenidos con las diversas asignaturas del mapa curricular en la formación integral de los estudiantes de la educación secundaria.</p>
				</div>
			</div>
			<div class="wrapper offers">
				<div class="grid_12">
					<article class="grid_3">
						<div class="offer offer2">
							<div class="title">Primer Año de curso</div>
							<p>en la secundaria se realiza la producción de pollos de engorda (Aves) con actividades complementarias en la producción.</p>
						</div>
					</article>
					<article class="grid_3">
						<div class="offer offer3">
							<div class="title">Segundo Año de curso</div>
							<p>en la secundaria se realiza la producción de pie de cría de borregas (Ovinos) Y elaboración de micro silos para la alimentación de los animales.</p>
						</div>
					</article>
					<article class="grid_5 last-col">
						<div class="offer offer4">
							<div class="title">Tercer Año de curso</div>
							<p>en la secundaria se realiza la producción de toros de engorda (Bovinos) y la elaboración de Bloques Activadores del Rumen (Bloques Nutricionales) para suplementar la alimentación de los animales y otras actividades complementarias.</p>
						</div>
					</article>
				</div>
				<div class="grid_12">
					<br>
					<p>En esta asignatura busca promover el estudio de los aspectos instrumentales de la técnica, sus procesos de cambio, gestión e innovación, y su relación con la sociedad y la naturaleza para la toma de decisiones en contextos diferentes.</p>
				</div>
			</div>
		</div>
	</div>
</div>