<?php
	echo $this->Html->css(
		array(
			//'admin/bootstrap/css/bootstrap.min',
			'admin/bootstrap/css/bootstrap-responsive.min',
			'admin/fancybox/source/jquery.fancybox',
			'admin/font-awesome/css/font-awesome',
			'gallery',
			),
		null,
		array('inline' => false)
	);

	echo $this->Html->script(
		array(
			'admin/jquery-slimscroll/jquery-ui-1.9.2.custom.min',
			'admin/jquery-slimscroll/jquery.slimscroll.min.js',
			'admin/fancybox/source/jquery.fancybox.pack'
			),
		array('inline' => false)
	);
?>
<div class="wrapper">
	<div class="grid_12">
		<div class="facts">
			<h2>Sabucanes viajeras</h2>
			<div class="wrapper">
				<div class="grid_12 alpha">
					<p>
						<strong class="white">Responsables</strong><br>
						Promotora de Lectura y Bibliotecario escolar
					</p>
					<p>
						<strong class="white">Dimensión curricular</strong><br>
						Pedagógica Curricular
					</p>
					<p>
						<strong class="white">Participantes</strong><br>
						Alumnos, docentes de español, biblioteca escolar
					</p>
					<p>
						<strong class="white">Propósito</strong><br>
						Poner en circulación los libros de la biblioteca del aula y escolar además de promover la gran diversidad de textos con las que cuentan dichas bibliotecas.
					</p>
				</div>
			</div>
		</div>
	</div>
	<article class="grid_12">
		<div class="facts">
			<div class="wrapper">
				<div class="grid_6 alpha">
					<ul class="list2">
						<li>
							<strong class="white">Metas</strong><br>Que los alumnos y padres de familia  conozcan la diversidad de textos con los que cuenta la escuela y fomentar la lectura de estos.
						</li>
						<li>
							<strong class="white">Actividades</strong><br>
							<ul class="list2">
								<li>Selección de textos</li>
								<li>Elaboración de los sabucanes</li>
								<li>Información a los responsables del comité de lectura de cada salón</li>
								<li>Entrega de sabucanes con tres libros</li>
								<li>Recepcionar los sabucanes</li>
							</ul>
						</li>
						<li class="last">
							<strong class="white">Actitudes a asumir</strong><br>Responsabilidad, Cooperación, Iniciativa y Compromiso.
						</li>
					</ul>
				</div>
				<div class="grid_6 omega last-col">
					<ul class="list2">
						<li>
							<strong class="white">Estrategias / Formas de acción</strong><br>
							<ul class="list2">
								<li>Se trabaja con la colaboración de los responsables de comité de lectura de cada salón</li>
								<li>Los sabucanes son entregados a los responsables de lectura para entregarlos a los alumnos que lo soliciten llevar esa semana</li>
								<li>Los alumnos tendrán bajo su responsabilidad los libros durante el fin de semana, compartirá con su familia los libros que contienen su bolsa</li>
								<li>Entregar un escrito relacionado con el texto que le llamó más la atención</li>
							</ul>
						</li>
						<li class="last">
							<strong class="white">Productos</strong><br>
							<ul class="list2">
								<li>Escritos de los alumnos</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</article>
</div>
<div class="wrapper">
	<!-- <article class="grid_12"> -->
		<?php
			echo $this->element(
				'static_gallery',
				array(
					'imageFolder' => 'travel_bags',
					'dataTitle' => 'Sabucanes viajeras',
					'totalImages' => 4
					)
				);
		?>
	<!-- </article> -->
</div>