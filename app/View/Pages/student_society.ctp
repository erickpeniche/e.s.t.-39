<div class="wrapper">
	<article class="grid_12">
		<h2>REPRESENTANTE DE LA SOCIEDAD DE ALUMNOS</h2>
		<p style="text-indent: 3em;">
			Las elecciones del Representan de la sociedad de alumnos se realizaron  el día 27 de septiembre, contando con el respaldo del IFE para verificar las votaciones, las planillas concursantes fueron:
		</p>
		<ul class="list1">
			<li><strong>VERDE: </strong> Luisa Poot Castillo del 3° B</li>
			<li><strong>MORADA: </strong> Goretti Colonia Viana del 3° A</li>
		</ul>
		<p>El día 26 de septiembre a las 10:40 am se realizó el mitin en las instalaciones de la escuela donde presentaron a los integrantes de cada planilla así como sus propuestas a todos los alumnos.
		</p>
		<p>El día 27 de septiembre a las 10:40 se realizó la jornada electoral en la que los alumnos de manera individual y directa emitieron su voto libre y secreto para elegir a su representante estudiantil. Las CASILLAS se establecieron de la siguiente manera con un maestro de la academia como responsable:</p>
		<ul class="list1">
			<li><strong>En la cooperativa: </strong> YAZMI SANCHEZ ARJONA</li>
			<li><strong>Pasillo por los baños: </strong> PABLO ALCARAZ TORRES</li>
			<li><strong>Pasillo de la entrada: </strong> MARGARITA HAU DZIB</li>
		</ul>
		<p style="text-indent: 3em;">Dando como resultado 163 votos para la planilla VERDE representada por la alumna Luisa del 3° B y 165 votos para la planilla MORADA representada por la alumna Goretti del 3° A, quedando como ganadora con una diferencia de 2 votos. Lo que representó una votación efectiva del 98 %.</p>
	</article>
</div>