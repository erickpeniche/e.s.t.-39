<div class="wrapper">
	<div class="grid_12">
		<div class="facts">
			<h2>Lectura rapidez, fluidez y comprensión</h2>
			<div class="wrapper">
				<div class="grid_12 alpha">
					<p>
						<strong class="white">Responsables</strong><br>
						Academia de español, grupo  SEC, docentes
					</p>
					<p>
						<strong class="white">Dimensión curricular</strong><br>
						Pedagógica Curricular
					</p>
					<p>
						<strong class="white">Participantes</strong><br>
						Alumnos de todos los grados
					</p>
					<p>
						<strong class="white">Propósito</strong><br>
						Favorecer que los padres de familia lean con sus hijos, los incentiven y los apoyen en la comprensión de la lectura.
					</p>
				</div>
			</div>
		</div>
	</div>
	<article class="grid_12">
		<div class="facts">
			<div class="wrapper">
				<div class="grid_6 alpha">
					<ul class="list2">
						<li>
							<strong class="white">Metas</strong><br>Que los alumnos compartan la lectura con sus padres, mejoren su comprensión y fluidez en los textos que leen.
						</li>
						<li>
							<strong class="white">Actividades</strong><br>
							<ul class="list2">
								<li>Seleccionar la lectura de la semana</li>
								<li>Medir la rapidez lectora</li>
								<li>Realizar la lectura en casa acompañado de un familiar y registrar el número de palabras leídas por minuto</li>
								<li>El tutor tendrá la tarea de redactar un texto reflexivo acerca de la lectura</li>
							</ul>
						</li>
						<li class="last">
							<strong class="white">Actitudes a asumir</strong><br>Compromiso del padre de familia, docentes y alumnos  para apoyar a sus hijos y a la escuela con los proyectos. Responsabilidad y cumplimiento.
						</li>
					</ul>
				</div>
				<div class="grid_6 omega last-col">
					<ul class="list2">
						<li>
							<strong class="white">Estrategias / Formas de acción</strong><br>
							<ul class="list2">
								<li>Realizar la primera lectura con los alumnos en el salón de clases</li>
								<li>Registrar en un cuadro las palabras leídas por minuto por cada día que lea</li>
								<li>Contestar las preguntas reflexivas relacionadas con la lectura</li>
								<li>Recopilar la lectura junto con el texto que elaboró el tutor o algún familiar  y las respuestas del alumno</li>
								<li>Revisar junto con los alumnos la comprensión lectora</li>
							</ul>
						</li>
						<li class="last">
							<strong class="white">Productos</strong><br>
							<ul class="list2">
								<li>Respuestas de las preguntas</li>
								<li>Texto redactado por el padre de familia</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</article>
</div>