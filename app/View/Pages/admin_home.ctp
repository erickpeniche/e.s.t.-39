<?php
	echo $this->Html->css(array('admin/gritter/css/jquery.gritter'), null, array('inline' => false));

	$this->set('activeMenu', 'home');
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
	<?php
		echo $this->element('admin/dashboard/dashboard_stat',
			array(
				'color' => 'blue',
				'icon' => 'picture',
				'number' => $totalMainImages,
				'title' => 'Imágenes principales',
				'ctrlr' => 'main_images'
				)
			);
	?>
	<?php
		echo $this->element('admin/dashboard/dashboard_stat',
			array(
				'color' => 'yellow',
				'icon' => 'list-alt',
				'number' => $totalNews,
				'title' => 'Noticias',
				'ctrlr' => 'news'
				)
			);
	?>
	<?php
		echo $this->element('admin/dashboard/dashboard_stat',
			array(
				'color' => 'purple',
				'icon' => 'calendar',
				'number' => $totalEvents,
				'title' => 'Eventos',
				'ctrlr' => 'events'
				)
			);
	?>
	<?php
		echo $this->element('admin/dashboard/dashboard_stat',
			array(
				'color' => 'brown',
				'icon' => 'camera',
				'number' => $totalImages,
				'title' => 'Imágenes',
				'ctrlr' => 'default_images'
				)
			);
	?>
</div>
<div class="row-fluid">
	<?php
		echo $isSuperUser ? $this->element('admin/dashboard/dashboard_stat',
			array(
				'color' => 'green',
				'icon' => 'group',
				'number' => $totalUsers,
				'title' => 'Usuarios',
				'ctrlr' => 'users'
				)
			) : '';
	?>
	<?php
		echo $isSuperUser ? $this->element('admin/dashboard/dashboard_stat',
			array(
				'color' => 'bg-grey',
				'icon' => 'cogs',
				'number' => '',
				'title' => 'Configuraciones',
				'ctrlr' => 'configurations'
				)
			) : '';
	?>
</div>
<!-- END PAGE CONTENT-->