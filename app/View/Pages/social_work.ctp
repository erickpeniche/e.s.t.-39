<?php
	echo $this->Html->css(
		array(
			//'admin/bootstrap/css/bootstrap.min',
			'admin/bootstrap/css/bootstrap-responsive.min',
			'admin/fancybox/source/jquery.fancybox',
			'admin/font-awesome/css/font-awesome',
			'gallery',
			),
		null,
		array('inline' => false)
	);

	echo $this->Html->script(
		array(
			'admin/jquery-slimscroll/jquery-ui-1.9.2.custom.min',
			'admin/jquery-slimscroll/jquery.slimscroll.min.js',
			'admin/fancybox/source/jquery.fancybox.pack'
			),
		array('inline' => false)
	);
?>
<div class="wrapper">
	<article class="grid_12">
		<h2>Departamento de Prefectura</h2>
		<p style="text-indent: 3em";>
			Este departamento se encarga de vigilar el orden y la disciplina de los alumnos; así como el cumplimiento de las funciones del personal docente y administrativo.
		</p>
		<p style="text-indent: 3em";>
			Está compuesto por dos personas a las que se les denomina PREFECTA DE ALUMNOS y PREFECTA DEL PERSONAL.
		</p>
		<p>Prefecta de Alumnos:</p>
		<ul class="list1">
			<li>Vigilar el orden y el buen comportamiento dentro del plantel escolar</li>
			<li>Supervisar la conducta a la hora de entrada (filtro), del descanso y la salida</li>
			<li>Vigilar que se cumpla el reglamento escolar</li>
			<li>Apoyar al docente en la integración y participación de los alumnos en las aulas y en el entorno de la escuela (educativo, social y familiar)</li>
			<li>Apoyar en la realización de proyectos escolares</li>
		</ul><br>
		<p>Prefecta del personal: (administrativo-docente)</p>
		<ul class="list1">
			<li>Vigilar que el personal docente y administrativo cumpla sus funciones</li>
			<li>Llevar un control sobre la asistencia e inasistencias del personal</li>
			<li>Brindar apoyo al docente en la integración de los alumnos</li>
		</ul><br>
	</article>
	<!-- <article class="grid_12"> -->
		<?php
			echo $this->element(
				'static_gallery',
				array(
					'imageFolder' => 'social_work',
					'dataTitle' => 'Prefectura y trabajo social',
					'totalImages' => 10
					)
				);
		?>
	<!-- </article> -->
</div>