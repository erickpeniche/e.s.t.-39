<div class="wrapper">
	<div class="grid_12">
		<div class="facts">
			<h2>PROYECTO DE REFRANES/VALORES</h2>
			<div class="wrapper">
				<div class="grid_12 alpha">
					<p>
						<strong class="white">Responsables</strong><br>
						ACADEMIA DE CIENCIAS SOCIALES
					</p>
					<p>
						<strong class="white">Dimensión curricular</strong><br>
						DIMENSIÓN PEDAGÓGICA CURRICULAR
					</p>
					<p>
						<strong class="white">Participantes</strong><br>
						LOS ALUMNOS DE TODOS LOS GRADOS Y GRUPOS
					</p>
					<p>
						<strong class="white">Propósito</strong><br>
						QUE LOS ALUMNOS  PARTICIPEN REFLEXIONANDO  CON LOS REFRANES  Y COMPARTAN SUS ESCRITOS, SUS IDEAS, VALORES, ETC. ADEMÁS QUE DISFRUTEN DE ESA ACTIVIDAD YA QUE ES UNA EXPERIENCIA QUE  FORTALECE Y FOMENTA  LA LECTURA Y LA ESCRITURA.
					</p>
				</div>
			</div>
		</div>
	</div>
	<article class="grid_12">
		<div class="facts">
			<div class="wrapper">
				<div class="grid_6 alpha">
					<ul class="list2">
						<li>
							<strong class="white">Metas</strong><br>Lograr en un 100 % la Participación de los alumnos de manera activa, creativa y  amena. Para Fortalecer  los Valores, a través de la Reflexión de refranes. Durante el curso escolar  2012-2013.
						</li>
						<li class="last">
							<strong class="white">Actividades</strong><br>
							<ul class="list2">
								<li>Nombrar un responsable de cada mes</li>
								<li>Seleccionar un refrán por semana</li>
								<li>Fotocopiar</li>
								<li>Entregar en el homenaje</li>
								<li>Recepcionar  y registrar los resultados</li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="grid_6 omega last-col">
					<ul class="list2">
						<li>
							<strong class="white">Estrategias / Formas de acción</strong><br>El  refrán se lee en el  homenaje. Los  maestros   del primer modulo lo analizan con los alumnos. Se realiza el trabajo en sus casas con ayuda de su tutor. Lo entregan los  miércoles. Un responsable de la academia  recoge, lee y selecciona los mejores trabajos. (Uno por Grado) Se leen en el homenaje.

						</li>
						<li>
							<strong class="white">Productos</strong><br>Los trabajos de análisis del refrán hechos por los alumnos, donde  se hace evidente lo reflexionado sobre el valor que al que se hace referencia esa semana.
						</li>
						<li class="last">
							<strong class="white">Actitudes a asumir</strong><br>Cambio de conducta en los alumnos en la vida cotidiana escolar, fortaleciendo los pilares del alumnado, ser mejores cada día logrando una mejor convivencia.
						</li>
					</ul>
				</div>
			</div>
		</div>
	</article>
</div>