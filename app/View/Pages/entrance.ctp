<?php
	echo $this->Html->css(
		array(
			//'admin/bootstrap/css/bootstrap.min',
			'admin/bootstrap/css/bootstrap-responsive.min',
			'admin/fancybox/source/jquery.fancybox',
			'admin/font-awesome/css/font-awesome',
			'gallery',
			),
		null,
		array('inline' => false)
	);

	echo $this->Html->script(
		array(
			'admin/jquery-slimscroll/jquery-ui-1.9.2.custom.min',
			'admin/jquery-slimscroll/jquery.slimscroll.min.js',
			'admin/fancybox/source/jquery.fancybox.pack'
			),
		array('inline' => false)
	);
?>
<div class="wrapper">
	<!-- <article class="grid_12"> -->
		<?php
			echo $this->element(
				'static_gallery',
				array(
					'imageFolder' => 'entrance',
					'dataTitle' => 'Entrada',
					'totalImages' => 10
					)
				);
		?>
	<!-- </article> -->
</div>