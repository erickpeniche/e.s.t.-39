<?php
	echo $this->Html->script(
		array(
			'forms'
			),
		array('inline' => false)
	);
?>
<div class="wrapper">
	<article class="grid_8">
		<h2 class="ind">Información de Contacto</h2>
		<div class="ext_box info">
			<figure>
				<span class="map_wrapper img_wrap1">
					<iframe id="map_canvas" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=97740 Temozon, YUC&amp;aq=0&amp;sll=37.0625,-95.677068&amp;sspn=61.282355,146.513672&amp;ie=UTF8&amp;hq=&amp;hnear=Brooklyn,+Kings,+New+York&amp;ll=20.804776,-88.202057&amp;spn=0.01628,0.025663&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
				</span>
			</figure>
			<div>
				<dl class="adress">
					<dt><strong class="white">Escuela Secundaria Técnica No. 39</strong></dt>
					<dd><?php echo $confs['contact-address'] ?></dd>
					<dd><span>Teléfono:</span><?php echo $confs['contact-phone'] ?></dd>
					<dd>E-mail: <a href="#" class="demo"><?php echo $confs['contact-email'] ?></a></dd>
				</dl>
			</div>
		</div>
	</article>
	<article class="grid_4 last-col">
		<h2 class="ind">Escríbenos</h2>
		<form id="contact-form">
			<div class="success">Se ha enviado el mensaje!<br>
				<strong>Nos pondremos en contacto a la brevedad posible</strong>
			</div>
			<fieldset>
				<label class="name">
					<input type="text" name="name" value="Nombre:">
					<span class="error">*No es un nombre válido.</span> <span class="empty">*Escribe tu nombre.</span>
				</label>
				<label class="email">
					<input type="text" name="e-mail" value="E-mail:">
					<span class="error">*No es una dirección de e-mail válida.</span> <span class="empty">*Escribe tu correo electrónico.</span>
				</label>
				<label class="phone">
					<input type="text" name="phone" value="Teléfono:">
					<span class="error">*No es un teléfono válido.</span> <span class="empty">*Escribe tu teléfono.</span>
				</label>
				<label class="message">
					<textarea name="message">Mensaje:</textarea>
					<span class="error">*El mensaje debe contener al menos.</span> <span class="empty">*Escribe un mensaje.</span>
				</label>
				<div class="buttons2">
					<a href="#" data-type="reset" class="button"><span>Limpiar</span></a>
					<a href="#" data-type="submit" class="button" id="submit-button"><span>Enviar</span></a>
				</div>
			</fieldset>
		</form>
	</article>
</div>