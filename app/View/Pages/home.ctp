<?php
	$this->startIfEmpty('slider');
	echo $this->element('slider', $mainImages);
	$this->end();

	$allowedTags = '<a><strong><em><br><strike>';
?>

<div class="wrapper">
	<article class="grid_8">
		<h2 class="ind">Noticias</h2>
		<?php foreach($news as $newsInfo): ?>
		<?php
			$img = isset($newsInfo['News']['image']) && !empty($newsInfo['News']['image']) ? 'images/news_main_prev_'.$newsInfo['News']['image'] : 'http://www.placehold.it/210x229/EFEFEF/AAAAAA&amp;text=Sin+foto';
		?>
		<div class="ext_box advantage">
			<figure>
				<?php
					echo $this->Html->link($this->Html->image($img),
						array(
							'controller' => 'news',
							'action' => 'view',
							$newsInfo['News']['id'],
							Format::clean($newsInfo['News']['title'])
						),
						array('class' => 'img_wrap1', 'escape' => false)
					);
				?>
			</figure>
			<div>
			<strong class="white"><?php echo  $newsInfo['News']['title']; ?></strong><br>
			<?php echo $this->Text->truncate(strip_tags($newsInfo['News']['description'], $allowedTags), 420, array('ellipsis' => '...', 'exact' => false)); ?><br>
			<?php
				echo $this->Html->link('<span>Leer más...</span>',
					array(
						'controller' => 'news',
						'action' => 'view',
						$newsInfo['News']['id'],
						Format::clean($newsInfo['News']['title'])
					),
					array('class' => 'button', 'escape' => false)
				);
			?>
			</div>
		</div>
		<?php endforeach; ?>
		<h2 class="ind1">Planes y Programas</h2>
		<div class="wrapper">
			<?php if(!empty($plans) || !empty($programs)): ?>
			<div class="grid_4 alpha">
				<ul class="list1">
					<?php foreach($plans as $index => $plan): ?>
					<li class="<?php echo $index == count($plans)-1 ? 'last' : ''; ?>"><?php echo $this->Html->link($plan['Document']['name'], array('controller' => 'documents', 'action'=>'download', $plan['Document']['id'])); ?></li>
					<?php endforeach; ?>
				</ul>
			</div>
			<div class="grid_4 omega last-col">
				<ul class="list1">
					<?php foreach($programs as $index => $program): ?>
					<li class="<?php echo $index == count($programs)-1 ? 'last' : ''; ?>"><?php echo $this->Html->link($program['Document']['name'], array('controller' => 'documents', 'action'=>'download', $program['Document']['id'])); ?></li>
					<?php endforeach; ?>
				</ul>
			</div>
			<?php else: ?>
			<div class="grid_8">
				<em>No hay planes o programas</em>
			</div>
			<?php endif; ?>
		</div>
	</article>
	<article class="grid_4 last-col">
		<h2 class="ind">Eventos</h2>
		<?php foreach ($events as $event): ?>
		<p class="p3">
			<?php $img = !empty($event['Event']['image']) ? $this->Html->image('images/events_main_prev_'.$event['Event']['image']) : $this->Html->image('<img src="http://www.placehold.it/290x163/EFEFEF/AAAAAA&amp;text=Sin+foto"/>'); ?>
			<?php
				echo $this->Html->link($img,
					array('controller'=>'events', 'action'=>'view', $event['Event']['id'], Format::clean($event['Event']['name'])),
					array('class'=>'img_wrap1', 'escape'=>false)
				);
			?>
		</p>
		<?php echo $this->Html->link('<strong class="white">'.$event['Event']['name'].'</strong>', array('controller' => 'events', 'action' => 'view', $event['Event']['id'], Format::clean($event['Event']['name'])), array('escape'=>false)); ?><br>
		<em><?php echo !empty($event['Event']['start_date']) ? $event['Event']['start_date'] : 'Sin fecha'; ?>, <?php echo !empty($event['Event']['start_time']) ? $event['Event']['start_time'] : 'Sin hora'; ?></em><br>
		<!-- <a href="#" class="button"><span>ver</span><em></em></a> --><br><br>
		<?php endforeach; ?>
	</article>
</div>