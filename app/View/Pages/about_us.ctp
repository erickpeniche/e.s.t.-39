<div class="wrapper offers">
	<article class="grid_12">
		<h2>¡Bienvenido!</h2>
		<p style="text-indent: 3em";>
			Hola estimado visitante, es muy grato para nosotros recibir la preferencia de tu atención a  este sitio. Deseamos darte a conocer la forma en que estamos tratando de cumplir con la función social que le corresponde a nuestra institución educativa, nuestros logros, los proyectos que tenemos para mejorar y al mismo tiempo ir construyendo, con las aportaciones de los que así lo deseen; la escuela que todos queremos.
		</p>
		<p style="text-indent: 3em">
			Debido a la necesidad de dar servicios educativos a una población cada vez mayor de adolescentes egresados de las escuelas primarias del municipio de Temozón, Yuc., 1980 a 1987  la E.S.T No. 6 de Valladolid pone en funcionamiento un módulo educativo en esta villa, siendo el   promotor de su  fundación el Prof.. José Rivero Loría. Durante este periodo fueron utilizados diferentes establecimientos públicos como aulas: el edificio de la primaria “Revolución”, el paradero de taxis, 	el bazar municipal y la biblioteca “Benito Juárez”.
		</p>
		<p style="text-indent: 3em">
			Debido a las gestiones  de los padres de familia y  autoridades municipales, el 12 de Octubre del 87 se decreta oficialmente la creación y funcionamiento de una nueva escuela oficial en el País; la “Escuela Secundaria Técnica No. 39”. Recibiendo ese año un edificio con 5 salones. Posteriormente en 1995, se construirían locales para la Dirección, los servicios administrativos, laboratorio de Ciencias, Trabajo social, Prefectura, biblioteca y taller de conservación e industrialización de alimentos.
		</p>
	</article>
	<article class="grid_7">
		<p>Los responsables del funcionamiento en etapa de módulo (1980-1987)</p>
		<ul class="list1">
			<li>Profr.  Juan Marrufo Alcocer</li>
			<li>Profr. Pablo Alcaraz Espinoza</li>
			<li>Profr. Prisciliano García Montoya</li>
		</ul>
		<p> Los maestros que iniciaron en  la etapa de escuela  merecen una mención especial ya que con esta labor contribuyeron conformar un perfil propio del alumno de nuestra Escuela Secundaria.</p>
		<ul class="list1">
			<li>Profr. Genaro Antonio Arjona y Herrera. Encargado de la materia de  español(q.e.p.d.)</li>
			<li>Profr. Raúl Rivero Centeno. Matemáticas. Actual maestro de la asignatura en esta escuela</li>
			<li>Profr. Ángel Gutiérrez Mena, impartiendo la especialidad de Ciencias Naturales,  y Director de la Escuela Primaria”Revolución”</li>
			<li>Profr. Pablo Alcaraz Espinoza (Ciencias Sociales)</li>
			<li>Profr. Felipe Arjona Herrera. (Inglés) el cual sigue prestando sus servicios en esta escuela</li>
			<li>Profr. Filiberto Iuit. (Educación Física)</li>
			<li>Prof. Santiago Góngora Centeno. Tecnologías, pecuarias(q.e.p.d.)</li>
		</ul>
	</article>
	<article class="grid_5 last-col">
		<div class="offer offer4">
			<div class="title">Directores de la E.S.T. No.39(1987-2013)</div>
			<ul class="list1">
				<li>Profr. Juan Diego Pérez Salazar</li>
				<li>Profr. Jaime Peniche Chavo</li>
				<li>Profr. Vicente Vázquez Espinoza</li>
				<li>Profr. Edgardo Baeza Castillo</li>
				<li>Profr. Pablo Alcaraz Espinoza</li>
				<li>Profr. Miguel Carrillo González</li>
				<li>Profr. Ramón Jacinto Pérez Escobedo</li>
				<li>Dr. Hilario Vélez Merino</li>
				<li>Profr. Demetrio Aguilar Ayala (Actual)</li>
			</ul>
		</div>
	</article>
	<article class="grid_12">
		<p>Gracias a la labor de todos estos maestros, el sueño de esta Villa se pudo   hacer realidad.</p>
		<p style="text-indent: 3em">Actualmente nos encontramos ubicados en la Villa de Temozón, carretera Valladolid-Tizimín, en la región oriente del Estado de Yucatán, en un edificio propio construido sobre la calle 11, ofreciendo las asignaturas del plan de estudios 2011 y las tecnologías de Agricultura, Pecuarias y Conservación e industrialización de alimentos. </p>
		<p style="text-indent: 3em">
			Las labores productivas que sostienen la economía de las familias de nuestros alumnos son las de agricultura y ganadería, pero también se presenta la carpintería en madera fina y la elaboración de productos cárnicos artesanales, destacando por su sabor la carne ahumada.
		</p>
		<p style="text-indent: 3em">
			En este curso escolar cumplimos 25 años de servicio, con una población estudiantil de 345alumnos distribuidos en  9 grupos atendidos por 20 maestros y 14 elementos más de personal administrativo, servicios educativos complementarios, de intendencia y directivos.
		</p>
		<p style="text-indent: 3em">
			Agradecemos a toda la población en general, a los padres de familia y a  las autoridades municipales por el apoyo en los eventos que hemos organizado y  por haber tenido la confianza de depositar en nuestras manos la educación de sus hijos; nos compromete a seguir brindando nuestro mejor esfuerzo y a poner en práctica las acciones que se contemplan en los nuevos planes y programas de estudio 2011 a fin de mantener el nivel de servicio y calidad que caracterizan a esta institución educativa.
		</p>
	</article>
	<article class="grid_3">
		<div class="offer offer1">
			<div class="title">Plantel</div>
			<ul class="list1">
				<li><?php echo $this->Html->link('Baños', array('controller'=>'pages', 'action'=>'bathrooms')); ?></li>
				<li><?php echo $this->Html->link('Canchas deportivas', array('controller'=>'pages', 'action'=>'sports_courts')); ?></li>
				<li><?php echo $this->Html->link('Conservación e industrialización de alimentos', array('controller'=>'pages', 'action'=>'food_conservation')); ?></li>
				<li><?php echo $this->Html->link('Construyendo jardín temático', array('controller'=>'pages', 'action'=>'theme_garden')); ?></li>
				<li><?php echo $this->Html->link('Cooperativa escolar', array('controller'=>'pages', 'action'=>'school_shop')); ?></li>
				<li><?php echo $this->Html->link('Entrada', array('controller'=>'pages', 'action'=>'entrance')); ?></li>
				<li><?php echo $this->Html->link('Escudo escolar', array('controller'=>'pages', 'action'=>'school_emblem')); ?></li>
				<li><?php echo $this->Html->link('Jardines entrada', array('controller'=>'pages', 'action'=>'entry_garden')); ?></li>
				<li><?php echo $this->Html->link('Jardines externos', array('controller'=>'pages', 'action'=>'outside_garden')); ?></li>
				<li><?php echo $this->Html->link('Jardines y salones', array('controller'=>'pages', 'action'=>'classrooms')); ?></li>
				<li><?php echo $this->Html->link('Personal de la escuela', array('controller'=>'pages', 'action'=>'school_staff')); ?></li>
				<li><?php echo $this->Html->link('Plaza cívica y dirección', array('controller'=>'pages', 'action'=>'civic_plaza')); ?></li>
				<li><?php echo $this->Html->link('Prefectura y trabajo social', array('controller'=>'pages', 'action'=>'social_work')); ?></li>
			</ul>
		</div>
	</article>
	<article class="grid_3">
		<div class="offer offer2">
			<div class="title">Servicio Social</div>
			<ul class="list1">
				<li><?php echo $this->Html->link('Trabajo Social', array('controller'=>'pages', 'action'=>'social_service')); ?></li>
			</ul>
		</div>
	</article>
	<article class="grid_3">
		<div class="offer offer3">
			<div class="title">Tecnologías</div>
			<ul class="list1">
				<li><?php echo $this->Html->link('Sector Agrícola', array('controller'=>'pages', 'action'=>'agriculture')); ?></li>
				<li><?php echo $this->Html->link('Sector Pecuario', array('controller'=>'pages', 'action'=>'#')); ?></li>
			</ul>
		</div>
	</article>
	<article class="grid_3 last-col">
		<div class="offer offer4">
			<div class="title">Sociedad de alumnos</div>
			<ul class="list1">
				<li><?php echo $this->Html->link('Nuestra sociedad', array('controller'=>'pages', 'action'=>'student_society')); ?></li>
			</ul>
		</div>
	</article>
</div>