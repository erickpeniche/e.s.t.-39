<?php
	echo $this->Html->css(
		array(
			//'admin/bootstrap/css/bootstrap.min',
			'admin/bootstrap/css/bootstrap-responsive.min',
			'admin/fancybox/source/jquery.fancybox',
			'admin/font-awesome/css/font-awesome',
			'gallery',
			),
		null,
		array('inline' => false)
	);

	echo $this->Html->script(
		array(
			'admin/jquery-slimscroll/jquery-ui-1.9.2.custom.min',
			'admin/jquery-slimscroll/jquery.slimscroll.min.js',
			'admin/fancybox/source/jquery.fancybox.pack'
			),
		array('inline' => false)
	);
?>
<div class="wrapper">
	<article class="grid_12">
		<h2>Sector Agrícola</h2>
		<p style="text-indent: 3em;">
			Este sector agrícola tiene como propósito principal que el alumno adquiera los conocimientos básicos sobre las técnicas para cultivar la tierra y su fundamento de dicho sector es la explotación de los recursos que la tierra origina, favorecida por la acción del hombre.
		</p>
		<p>Actualmente se cuenta  con la producción de los siguientes cultivos:</p>
		<ul class="list1">
			<li>Rábano</li>
			<li>Cilantro</li>
			<li>Chile Habanero</li>
			<li>Cítricos</li>
			<li>Elote</li>
		</ul><br>
		<p style="text-indent: 3em;">Estas actividades son realizadas en su totalidad por los alumnos para finalmente ser comercializadas en la población a un costo menor del que se vende en la comunidad, así de esta forma aportamos a la comunidad productos frescos y más económicos.
		</p>
		<p style="text-indent: 3em;">Al egresar el alumno del nivel secundaria cuenta con los conocimientos básicos que le podrán servir en un futuro ya que tiene la comprensión sobre la importancia de la agricultura de manera laboral y alimenticia.
		</p>
	</article>
	<!-- <article class="grid_12"> -->
		<?php
			echo $this->element(
				'static_gallery',
				array(
					'imageFolder' => 'agriculture',
					'dataTitle' => 'Imágenes',
					'totalImages' => 7
					)
				);
		?>
	<!-- </article> -->
</div>