<?php
   echo $this->Html->css(array(
	  'admin/bootstrap-fileupload/bootstrap-fileupload',
	  'admin/gritter/css/jquery.gritter',
	  'admin/jquery-tags-input/jquery.tagsinput',
	  'admin/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons',
	  'admin/data-tables/DT_bootstrap',
	  ), null, array('inline' => false)
   );

   echo $this->Html->script(array(
	  //'admin/js/jquery-1.8.3.min',
	  'admin/bootstrap-fileupload/bootstrap-fileupload',
	  'admin/jquery-tags-input/jquery.tagsinput.min',
	  'admin/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons'
	  ), array('inline' => false)
   );

   $this->set('activeMenu', 'defaultImages');

   $this->Html->addCrumb('Galería de imágenes',
	  array(
		 'action' => 'index',
		 'controller' => 'default_images',
		 'admin' => true
		 )
	  );

   $this->Html->addCrumb('Nueva',
	  array(
		 'action' => 'add',
		 'controller' => 'default_images',
		 'admin' => true
		 )
	  );

   	$model = '';
   	if(!empty($this->params['pass'])){
		switch ($this->params['pass'][0]) {
			case 'news':
				$model = 'Noticias';
				   $this->Html->addCrumb($model,
					  array(
						 'action' => 'index',
						 'controller' => $this->params['pass'][0],
						 'admin' => true
						 )
					  );
				    $this->Html->addCrumb($news[$this->params['pass'][1]],
				   	  array(
				   		 'action' => 'view',
				   		 'controller' => $this->params['pass'][0],
				   		 $this->params['pass'][1],
				   		 'admin' => true
				   		 )
				   	  );

				break;
			case 'events':
				$model = 'Eventos';
				   $this->Html->addCrumb($model,
					  array(
						 'action' => 'index',
						 'controller' => $this->params['pass'][0],
						 'admin' => true
						 )
					  );
				    $this->Html->addCrumb($events[$this->params['pass'][1]],
				   	  array(
				   		 'action' => 'view',
				   		 'controller' => $this->params['pass'][0],
				   		 $this->params['pass'][1],
				   		 'admin' => true
				   		 )
				   	  );

				break;

			/*case '':
				$model = '';
				break;

			case '':
				$model = '';
				break;

			case '':
				$model = '';
				break;*/
		}
   	}
?>
<div class="portlet box blue">
   <div class="portlet-title">
	  <h4><i class="icon-picture"></i>Nueva imagen</h4>
	  <div class="actions">
		 <?php echo $this->Html->link('<i class="icon-th-large"></i> Galería de Imágenes</a>',
			array(
			   'action' => 'index',
			   'controller' => 'default_images',
			   'admin' => true
			   ),
			array(
			   'class' => 'btn blue',
			   'escape' => false)
			);
		 ?>
		 <!-- <a href="#" class="btn green"> -->
	  </div>
   </div>
   <div class="portlet-body form">
	  <!-- BEGIN FORM-->
	  <?php echo $this->Form->create('DefaultImage',
		 array(
			'inputDefaults' => array(
			   'div' => array('class' => 'control-group'),
			   'class' => array('class' => 'm-wrap span12')
			   ),
			'class' => 'form-horizontal form-row-seperated',
			'enctype' => 'multipart/form-data',
			'novalidate' => true
			)
		 );
	  ?>
	  <?php
	  	$prefix = $this->params['pass'][0] != 'news' ? substr($this->params['pass'][0], 0, -1) : $this->params['pass'][0];
	  	 echo $this->Form->hidden('model', array('value' => $this->params['pass'][0]));
	  	 echo $this->Form->hidden($prefix.'_id', array('value' => $this->params['pass'][1]));

		 $div = $this->Html->tag('div', null, array('class'=>'controls'));
		 $help = $this->Html->tag('span', '*Opcional - Es una palabra u oración corta que describa la imagen', array('class'=>'help-inline'));
		 echo $this->Form->input('description',
			array(
			   'between' => $div,
			   'placeholder'=>'Descripción breve de la imagen',
			   'label' => array('text'=>'Descripción', 'class'=>'control-label'),
			   'after' => $help
			   )
			);
		 echo '</div>';

		 $div = $this->Html->tag('div', null, array('class'=>'controls'));
		 $help = $this->Html->tag('span', '*Opcional', array('class'=>'help-inline'));
		 echo $this->Form->input('link',
			array(
			   'between' => $div,
			   'placeholder'=>'http://www.ejemplo.com',
			   'label' => array('text'=>'URL', 'class'=>'control-label'),
			   'after' => $help
			   )
			);
		 echo '</div>';
	  ?>

		 <div class="control-group">
			<label class="control-label">Imagen</label>
			<div class="controls">
			   <div class="fileupload fileupload-new" data-provides="fileupload">
				  <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
					 <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=sin+imagen" alt="" />
				  </div>
				  <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">
				  </div>
				  <div>
					 <span class="btn btn-file"><span class="fileupload-new">Seleccionar</span>
					 <span class="fileupload-exists">Cambiar</span>
					 <input type="file" name="data[DefaultImage][image]" class="default" id="DefaultImageImage"></span>
					 <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Eliminar</a>
				  </div>
			   </div>
			   <span class="label label-important">NOTA!</span>
			   <span>La imagen debe pesar menos de 20 MB</span>
			</div>
		 </div>

	  <?php
		 echo '<div class="form-actions">';
		 echo $this->Form->button('<i class="icon-ok"></i> Guardar',
			array('type' => 'submit', 'class' => 'btn green')
			);
		 echo '&nbsp;';

		 $cancelOptions = array('action' => 'index', 'controller' => 'default_images', 'admin' => true);
		 echo $this->Form->button('Cancelar',
			array(
			   'type' => 'button',
			   'class' => 'btn red',
			   'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
			   )
			);
		 echo '</div>';
	  ?>

	  <?php echo $this->Form->end(); ?>
	  <!-- END FORM-->
   </div>
</div>