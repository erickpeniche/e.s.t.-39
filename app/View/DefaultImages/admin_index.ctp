<?php
	echo $this->Html->css(array(
		'admin/fancybox/source/jquery.fancybox',
		'admin/data-tables/DT_bootstrap',
		), null, array('inline' => false)
	);

	$this->set('activeMenu', 'defaultImages');

	$this->Html->addCrumb('Galería de imágenes',
		array(
			'action' => 'index',
			'controller' => 'default_images',
			'admin' => true
			)
		);
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
	<div class="span12">
		<!--BEGIN TABS-->
		<div class="tabbable tabbable-custom tabs-left">
			<!-- Only required for left/right tabs -->
			<ul class="nav nav-tabs tabs-left">
				<li class="active"><a href="#tab_3_1" data-toggle="tab">Noticias</a></li>
				<li><a href="#tab_3_2" data-toggle="tab">Eventos</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="tab_3_1">
				<?php if(!empty($defaultImages['news'])): ?>
					<?php $i = 0; ?>
					<?php foreach ($defaultImages['news'] as $image): ?>
						<?php $i++ ?>

						<?php if ($i%4 == 1) : ?>
						<div class="row-fluid">
						<?php endif ?>
							<div class="span3">
								<div class="item">
									<a class="fancybox-button" data-rel="fancybox-button" title="<?php echo $image['DefaultImage']['description']; ?>" href="<?php echo '/img/images/news_extra_img_'.$image['DefaultImage']['file']; ?>">
										<div class="zoom">
											<?php echo $this->Html->image('images/news_extra_img_'.$image['DefaultImage']['file']); ?>
											<div class="zoom-icon"></div>
										</div>
									</a>
								</div>
							</div>
						<?php if ($i%4 == 0 || sizeof($defaultImages['news']) == $i) : ?>
						</div>
						<?php endif; ?>
					<?php endforeach; ?>
				<?php else: ?>
					<div class="alert alert-block alert-info fade in">
						<button type="button" class="close" data-dismiss="alert"></button>
						<h4 class="alert-heading">No hay imágenes!</h4>
						<p>
							Aún no se han agregado imágenes a la sección de Noticias. Agrega imágenes en la sección de noticias.
						</p>
					</div>
				<?php endif; ?>
				</div>
				<div class="tab-pane" id="tab_3_2">
					<?php if(!empty($defaultImages['events'])): ?>
						<?php $i = 0; ?>
						<?php foreach ($defaultImages['events'] as $image): ?>
							<?php $i++ ?>

							<?php if ($i%4 == 1) : ?>
							<div class="row-fluid">
							<?php endif ?>
								<div class="span3">
									<div class="item">
										<a class="fancybox-button" data-rel="fancybox-button" title="<?php echo $image['DefaultImage']['description']; ?>" href="<?php echo '/img/images/events_extra_img_'.$image['DefaultImage']['file']; ?>">
											<div class="zoom">
												<?php echo $this->Html->image('images/events_extra_img_'.$image['DefaultImage']['file']); ?>
												<div class="zoom-icon"></div>
											</div>
										</a>
									</div>
								</div>
							<?php if ($i%4 == 0 || sizeof($defaultImages['news']) == $i) : ?>
							</div>
							<?php endif; ?>
						<?php endforeach; ?>
					<?php else: ?>
						<div class="alert alert-block alert-info fade in">
							<button type="button" class="close" data-dismiss="alert"></button>
							<h4 class="alert-heading">No hay imágenes!</h4>
							<p>
								Aún no se han agregado imágenes a la sección de Eventos. Agrega imágenes en la sección de noticias.
							</p>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<!--END TABS-->
	</div>
</div>
<!-- END PAGE CONTENT-->