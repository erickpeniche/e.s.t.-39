<div class="defaultImages view">
<h2><?php  echo __('Default Image'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($defaultImage['DefaultImage']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('File'); ?></dt>
		<dd>
			<?php echo h($defaultImage['DefaultImage']['file']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Model'); ?></dt>
		<dd>
			<?php echo h($defaultImage['DefaultImage']['model']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($defaultImage['DefaultImage']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Size'); ?></dt>
		<dd>
			<?php echo h($defaultImage['DefaultImage']['size']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Link'); ?></dt>
		<dd>
			<?php echo h($defaultImage['DefaultImage']['link']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($defaultImage['DefaultImage']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($defaultImage['DefaultImage']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('News'); ?></dt>
		<dd>
			<?php echo $this->Html->link($defaultImage['News']['title'], array('controller' => 'news', 'action' => 'view', $defaultImage['News']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Plant'); ?></dt>
		<dd>
			<?php echo $this->Html->link($defaultImage['Plant']['name'], array('controller' => 'plants', 'action' => 'view', $defaultImage['Plant']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Technology'); ?></dt>
		<dd>
			<?php echo $this->Html->link($defaultImage['Technology']['name'], array('controller' => 'technologies', 'action' => 'view', $defaultImage['Technology']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Educational Plan'); ?></dt>
		<dd>
			<?php echo $this->Html->link($defaultImage['EducationalPlan']['name'], array('controller' => 'educational_plans', 'action' => 'view', $defaultImage['EducationalPlan']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Educational Program'); ?></dt>
		<dd>
			<?php echo $this->Html->link($defaultImage['EducationalProgram']['name'], array('controller' => 'educational_programs', 'action' => 'view', $defaultImage['EducationalProgram']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project'); ?></dt>
		<dd>
			<?php echo $this->Html->link($defaultImage['Project']['name'], array('controller' => 'projects', 'action' => 'view', $defaultImage['Project']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Improvement Plan'); ?></dt>
		<dd>
			<?php echo $this->Html->link($defaultImage['ImprovementPlan']['name'], array('controller' => 'improvement_plans', 'action' => 'view', $defaultImage['ImprovementPlan']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Event'); ?></dt>
		<dd>
			<?php echo $this->Html->link($defaultImage['Event']['name'], array('controller' => 'events', 'action' => 'view', $defaultImage['Event']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Default Image'), array('action' => 'edit', $defaultImage['DefaultImage']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Default Image'), array('action' => 'delete', $defaultImage['DefaultImage']['id']), null, __('Are you sure you want to delete # %s?', $defaultImage['DefaultImage']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Default Images'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Default Image'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List News'), array('controller' => 'news', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New News'), array('controller' => 'news', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Plants'), array('controller' => 'plants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Plant'), array('controller' => 'plants', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Technologies'), array('controller' => 'technologies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Technology'), array('controller' => 'technologies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Educational Plans'), array('controller' => 'educational_plans', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Educational Plan'), array('controller' => 'educational_plans', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Educational Programs'), array('controller' => 'educational_programs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Educational Program'), array('controller' => 'educational_programs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects'), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project'), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Improvement Plans'), array('controller' => 'improvement_plans', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Improvement Plan'), array('controller' => 'improvement_plans', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Events'), array('controller' => 'events', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Event'), array('controller' => 'events', 'action' => 'add')); ?> </li>
	</ul>
</div>
