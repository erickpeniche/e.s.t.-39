<div class="defaultImages form">
<?php echo $this->Form->create('DefaultImage'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Default Image'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('file');
		echo $this->Form->input('model');
		echo $this->Form->input('description');
		echo $this->Form->input('size');
		echo $this->Form->input('link');
		echo $this->Form->input('news_id');
		echo $this->Form->input('plant_id');
		echo $this->Form->input('technology_id');
		echo $this->Form->input('educational_plan_id');
		echo $this->Form->input('educational_program_id');
		echo $this->Form->input('project_id');
		echo $this->Form->input('improvement_plan_id');
		echo $this->Form->input('event_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('DefaultImage.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('DefaultImage.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Default Images'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List News'), array('controller' => 'news', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New News'), array('controller' => 'news', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Plants'), array('controller' => 'plants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Plant'), array('controller' => 'plants', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Technologies'), array('controller' => 'technologies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Technology'), array('controller' => 'technologies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Educational Plans'), array('controller' => 'educational_plans', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Educational Plan'), array('controller' => 'educational_plans', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Educational Programs'), array('controller' => 'educational_programs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Educational Program'), array('controller' => 'educational_programs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects'), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project'), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Improvement Plans'), array('controller' => 'improvement_plans', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Improvement Plan'), array('controller' => 'improvement_plans', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Events'), array('controller' => 'events', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Event'), array('controller' => 'events', 'action' => 'add')); ?> </li>
	</ul>
</div>
