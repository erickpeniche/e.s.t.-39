$(function(){
// IPad/IPhone
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
	// Menu Android
	var userag = navigator.userAgent.toLowerCase();
	var isAndroid = userag.indexOf("android") > -1; 
	if(isAndroid) {
		$('.sf-menu').responsiveMenu({autoArrows:true});
	}
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')
$(window).load(function() {
	$(".sf-menu>li>a").append("<strong></strong><em></em>");
  $("a.img_wrap1, .button").append("<em></em>");
  $().UItoTop({ easingType: 'easeOutQuart' });
});

var App = function () {

	var handleFancyBox = function () {

		if (!jQuery.fancybox) {
			return;
		}

		if (jQuery(".fancybox-button").size() > 0) {
			jQuery(".fancybox-button").fancybox({
				groupAttr: 'data-rel',
				prevEffect: 'none',
				nextEffect: 'none',
				closeBtn: true,
				helpers: {
					title: {
						type: 'inside'
					}
				}
			});
		}
	}

	var handleScrollers = function () {

		var setPageScroller = function () {
			$('.main').slimScroll({
				size: '12px',
				color: '#a1b2bd',
				height: $(window).height(),
				allowPageScroll: true,
				alwaysVisible: true,
				railVisible: true
			});
		}

		/*
		//if (isIE8 == false) {
			$(window).resize(function(){
			   setPageScroller();
			});
			setPageScroller();
		//} else {
			$('.main').removeClass("main");
		//}
		*/

		$('.scroller').each(function () {
			$(this).slimScroll({
				//start: $('.blah:eq(1)'),
				size: '7px',
				color: '#a1b2bd',
				height: $(this).attr("data-height"),
				alwaysVisible: ($(this).attr("data-always-visible") == "1" ? true : false),
				railVisible: ($(this).attr("data-rail-visible") == "1" ? true : false),
				disableFadeOut: true
			});
		});

	}

	return {

		//main function to initiate template pages
		init: function () {
			// global handlers
			handleScrollers(); // handles slim scrolling contents
			handleFancyBox(); // handles fancy box image previews
		},

		// public method to initialize uniform inputs
		initFancybox: function () {
			handleFancyBox();
		}

	};

}();