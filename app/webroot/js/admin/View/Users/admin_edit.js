var adminEdit = {
	init: function() {
		$(document).ajaxStart(function() {
			$('.submit-btn .save').toggleClass('green disabled');
			$('.submit-btn .save').html('<img src="/img/admin/ajax-loader" alt=""> Guardando');
		});
		$(document).ajaxComplete(function() {
			$('.submit-btn .save').toggleClass('green disabled');
			$('.submit-btn .save').html('<i class="icon-ok"></i> Guardar');
		});
	}
}

$(document).ready(function () {
	adminEdit.init();
});