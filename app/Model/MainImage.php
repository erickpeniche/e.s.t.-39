<?php
App::uses('AppModel', 'Model');
/**
 * MainImage Model
 *
 */
class MainImage extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message'=>'<span class="input-error tooltips" data-original-title="Introduzca un nombre para la imagen"><i class="icon-exclamation-sign"></i></span>'
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'link' => array(
			'website' => array(
				'rule' => 'url',
				'message'=>'<span class="input-error tooltips" data-original-title="Introduzca una URL válida"><i class="icon-exclamation-sign"></i></span>',
				'allowEmpty' => true
			)
		)
	);

	function beforeSave() {
		$return = false;
		$image = null;
		$erase = false;
		$mainImage = null;

		if (!empty($this->id)) {
			$mainImage = $this->find('first', array('conditions' => array('MainImage.id' => $this->data['MainImage']['id'])));
			$image = $mainImage['MainImage']['image'];
		}

		//File upload
		$file = isset($this->data['MainImage']['image']) ? $this->data['MainImage']['image'] : null;
		$fileDir = WWW_ROOT . 'img' . DS . 'images';
		$fileName = '';
		$fileExtension = '';

		//A file was uploaded
		if (!empty($file) && !empty($file['tmp_name']) && $file['error'] != 4) {
			//There is no error, the file was uploaded with success
			if ($file['error'] == 0) {

				//Tells whether the file was uploaded via HTTP POST
				if (is_uploaded_file($file['tmp_name'])) {
					App::import('Vendor', 'Bitmap');
					App::import('Vendor', 'BitmapException');

					if (Bitmap::isImage($file['tmp_name'])) {

						try {
							$fileName = md5($file['tmp_name'] . (rand() * 100000));
							$imageSize = getimagesize($file['tmp_name']);
							$width = $imageSize[0];
							$height = $imageSize[1];

							$set['height'] = 450;
							$set['width'] = 920;
							$set['align'] = 'center';
							$set['valign'] = 'middle';

							$img = new Bitmap($file['tmp_name']);
							$img->open();
							$img->resizeAndCrop2($imageSize, $set);
							$img->save($fileDir, 'main_img_' . $fileName);
							$img->dispose();
							$fileExtension = $img->getExtension();

							$thn = new Bitmap($file['tmp_name']);
							$thn->open();
							$thn->resizeAndCrop(373, 249);
							$thn->save($fileDir, 'main_thn_' . $fileName);
							$thn->dispose();

							$this->data['MainImage']['size'] = $file['size'];
							$this->data['MainImage']['image'] = $fileName . '.' .  $fileExtension;
							$return = true;
							$erase = true;
						} catch (BitmapException $e) {
							$this->invalidate('image', 'bitmapError');
						}
					} else {
						$this->invalidate('image', 'unsupportedType');
					}
				} else {
					$this->invalidate('image', 'maliciousUpload');
				}
			} else {
				//File upload error. See http://us3.php.net/manual/en/features.file-upload.errors.php
				$this->invalidate('image', 'uploadError');
			}
		} else {
			if (isset($this->data['MainImage']['erase']) && $this->data['MainImage']['erase']) {
				$this->data['MainImage']['image'] = null;
			} else {
				unset($this->data['MainImage']['image']);
			}
			$return = true;
		}

		// REMOVE PREVIOUS LOGO
		if (!empty($image) && ((isset($this->data['MainImage']['erase']) && $this->data['MainImage']['erase']) || $erase)) {
			unlink($fileDir . DS . 'main_thn_' . $image);
			unlink($fileDir . DS . 'main_img_' . $image);
		}


		// Link value
        if (isset($this->data['MainImage']['link'])) {
             $protocols = array('http', 'https', 'ftp');
             $length = count($protocols);
             for ($j = 0; $j < $length; $j++) {
                  if (stripos($this->data['MainImage']['link'], $protocols[$j]) !== false) {
                       $this->data['MainImage']['link'] = $this->data['MainImage']['link'];
                       break;
                  }

                  if ($j == $length - 1) {
                  		if(!empty($this->data['MainImage']['link'])){
                       		$this->data['MainImage']['link'] = 'http://' . $this->data['MainImage']['link'];
                  		} else{
                  			$this->data['MainImage']['link'] = null;
                  		}
                  }
             }
        }

		return $return;
	}

	function beforeDelete() {
		$mainImage = $this->find('first', array('conditions' => array('MainImage.id' => $this->id), 'contain' => false));

		if (!empty($mainImage['MainImage']['image'])) {
			$imageDirectory = WWW_ROOT . 'img' . DS . 'images' . DS;
			unlink($imageDirectory . 'main_img_' . $mainImage['MainImage']['image']);
			unlink($imageDirectory . 'main_thn_' . $mainImage['MainImage']['image']);
		}

		return true;
	}
}
