<?php
App::uses('AppModel', 'Model');
/**
 * DefaultImage Model
 *
 * @property News $News
 * @property Plant $Plant
 * @property Technology $Technology
 * @property EducationalPlan $EducationalPlan
 * @property EducationalProgram $EducationalProgram
 * @property Project $Project
 * @property ImprovementPlan $ImprovementPlan
 * @property Event $Event
 */
class DefaultImage extends AppModel {

	public $validate = array(
		'image' => array(
			'rule' => array('fileSize', '>', '0MB'),
			'required' => true
		),
	);
/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'News' => array(
			'className' => 'News',
			'foreignKey' => 'news_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Event' => array(
			'className' => 'Event',
			'foreignKey' => 'event_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	function beforeSave() {
		$return = false;
		$image = null;
		$erase = false;
		$defaultImage = null;

		if (!empty($this->id)) {
			$defaultImage = $this->find('first', array('conditions' => array('DefaultImage.id' => $this->data['DefaultImage']['id'])));
			$image = $defaultImage['DefaultImage']['image'];
		}

		//File upload
		$file = isset($this->data['DefaultImage']['image']) ? $this->data['DefaultImage']['image'] : null;
		$fileDir = WWW_ROOT . 'img' . DS . 'images';
		$fileName = '';
		$fileExtension = '';

		//A file was uploaded
		if (!empty($file) && !empty($file['tmp_name']) && $file['error'] != 4) {
			//There is no error, the file was uploaded with success
			if ($file['error'] == 0) {

				//Tells whether the file was uploaded via HTTP POST
				if (is_uploaded_file($file['tmp_name'])) {
					App::import('Vendor', 'Bitmap');
					App::import('Vendor', 'BitmapException');

					if (Bitmap::isImage($file['tmp_name'])) {

						try {
							$fileName = md5($file['tmp_name'] . (rand() * 100000));
							$imageSize = getimagesize($file['tmp_name']);
							$width = $imageSize[0];
							$height = $imageSize[1];

							/*$img_prev = new Bitmap($file['tmp_name']);
							$img_prev->open();
							$img_prev->resizeAndCrop(100, 100);
							$img_prev->save($fileDir, $this->data['DefaultImage']['model'].'_extra_img_prev_'.$fileName);
							$img_prev->dispose();
							$fileExtension = $img_prev->getExtension();*/

							$img = new Bitmap($file['tmp_name']);
							$img->open();
							$img->save($fileDir, $this->data['DefaultImage']['model'].'_extra_img_'.$fileName);
							$img->dispose();
							$fileExtension = $img->getExtension();

							$this->data['DefaultImage']['size'] = $file['size'];
							$this->data['DefaultImage']['file'] = $fileName . '.' .  $fileExtension;

							$return = true;
							$erase = true;
						} catch (BitmapException $e) {
							$this->invalidate('image', 'bitmapError');
						}
					} else {
						$this->invalidate('image', 'unsupportedType');
					}
				} else {
					$this->invalidate('image', 'maliciousUpload');
				}
			} else {
				//File upload error. See http://us3.php.net/manual/en/features.file-upload.errors.php
				$this->invalidate('image', 'uploadError');
			}
		} else {
			if (isset($this->data['DefaultImage']['erase']) && $this->data['DefaultImage']['erase']) {
				$this->data['DefaultImage']['image'] = null;
			} else {
				unset($this->data['DefaultImage']['image']);
			}
			$return = true;
		}

		// REMOVE PREVIOUS LOGO
		if (!empty($image) && ((isset($this->data['DefaultImage']['erase']) && $this->data['DefaultImage']['erase']) || $erase)) {
			unlink($fileDir . DS . $this->data['DefaultImage']['model'].'_extra_img_' . $image);
		}


		// Link value
		if (isset($this->data['DefaultImage']['link'])) {
			 $protocols = array('http', 'https', 'ftp');
			 $length = count($protocols);
			 for ($j = 0; $j < $length; $j++) {
				  if (stripos($this->data['DefaultImage']['link'], $protocols[$j]) !== false) {
					   $this->data['DefaultImage']['link'] = $this->data['DefaultImage']['link'];
					   break;
				  }

				  if ($j == $length - 1) {
						if(!empty($this->data['DefaultImage']['link'])){
							$this->data['DefaultImage']['link'] = 'http://' . $this->data['DefaultImage']['link'];
						} else{
							$this->data['DefaultImage']['link'] = null;
						}
				  }
			 }
		}

		return $return;
	}

	function beforeDelete() {
		$defaultImage = $this->find('first', array('conditions' => array('DefaultImage.id' => $this->id), 'contain' => false));

		if (!empty($defaultImage['DefaultImage']['file'])) {
			$imageDirectory = WWW_ROOT . 'img' . DS . 'images' . DS;
			unlink($imageDirectory . $defaultImage['DefaultImage']['model'].'_extra_img_' . $defaultImage['DefaultImage']['file']);
		}

		return true;
	}
}
