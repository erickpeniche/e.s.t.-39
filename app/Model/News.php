<?php
App::uses('AppModel', 'Model');
/**
 * News Model
 *
 * @property DefaultImage $DefaultImage
 */
class News extends AppModel {
	var $name = "News";

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message'=>'<span class="input-error tooltips" data-original-title="Introduzca un nombre para la noticia"><i class="icon-exclamation-sign"></i></span>'
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'link' => array(
			'website' => array(
				'rule' => 'url',
				'message'=>'<span class="input-error tooltips" data-original-title="Introduzca una URL válida"><i class="icon-exclamation-sign"></i></span>',
				'allowEmpty' => true
			)
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'DefaultImage' => array(
			'className' => 'DefaultImage',
			'foreignKey' => 'news_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	function beforeSave() {
		$return = false;
		$image = null;
		$erase = false;
		$news = null;

		if (!empty($this->id)) {
			$news = $this->find('first', array('conditions' => array('News.id' => $this->data['News']['id'])));
			$image = $news['News']['image'];
		}
		//File upload
		$file = isset($this->data['News']['image']) ? $this->data['News']['image'] : null;
		$fileDir = WWW_ROOT . 'img' . DS . 'images';
		$fileName = '';
		$fileExtension = '';

		//A file was uploaded
		if (!empty($file) && !empty($file['tmp_name']) && $file['error'] != 4) {
			//There is no error, the file was uploaded with success
			if ($file['error'] == 0) {

				//Tells whether the file was uploaded via HTTP POST
				if (is_uploaded_file($file['tmp_name'])) {
					App::import('Vendor', 'Bitmap');
					App::import('Vendor', 'BitmapException');

					if (Bitmap::isImage($file['tmp_name'])) {
						try {
							$fileName = md5($file['tmp_name'] . (rand() * 100000));
							$imageSize = getimagesize($file['tmp_name']);
							$width = $imageSize[0];
							$height = $imageSize[1];

							$img = new Bitmap($file['tmp_name']);
							$img->open();
							$img->save($fileDir, 'news_img_' . $fileName);
							$img->dispose();
							$fileExtension = $img->getExtension();

							$img_edt = new Bitmap($file['tmp_name']);
							$img_edt->open();
							$img_edt->resizeToWidthHeight(200, 150);
							$img_edt->save($fileDir, 'news_img_edt_' . $fileName);
							$img_edt->dispose();
							$fileExtension = $img_edt->getExtension();

							$tablePrev = new Bitmap($file['tmp_name']);
							$tablePrev->open();
							$tablePrev->resizeToWidth(145);
							$tablePrev->save($fileDir, 'news_tbl_prev_' . $fileName);
							$tablePrev->dispose();

							$mainPrev = new Bitmap($file['tmp_name']);
							$mainPrev->open();
							$mainPrev->resizeAndCrop(210, 229);
							$mainPrev->save($fileDir, 'news_main_prev_' . $fileName);
							$mainPrev->dispose();

							$this->data['News']['size'] = $file['size'];
							$this->data['News']['image'] = $fileName . '.' .  $fileExtension;
							$return = true;
							$erase = true;
						} catch (BitmapException $e) {
							$this->invalidate('image', 'bitmapError');
						}
					} else {
						$this->invalidate('image', 'unsupportedType');
					}
				} else {
					$this->invalidate('image', 'maliciousUpload');
				}
			} else {
				//File upload error. See http://us3.php.net/manual/en/features.file-upload.errors.php
				$this->invalidate('image', 'uploadError');
			}
		} else {
			if (isset($this->data['News']['erase']) && $this->data['News']['erase']) {
				$this->data['News']['image'] = null;
			} else {
				unset($this->data['News']['image']);
			}
			$return = true;
		}

		// REMOVE PREVIOUS LOGO
		if (!empty($image) && ((isset($this->data['News']['erase']) && $this->data['News']['erase']) || $erase)) {
			unlink($fileDir . DS . 'news_main_prev_' . $image);
			unlink($fileDir . DS . 'news_tbl_prev_' . $image);
			unlink($fileDir . DS . 'news_img_' . $image);
			unlink($fileDir . DS . 'news_img_edt_' . $image);
		}


		// Link value
        if (isset($this->data['News']['link'])) {
             $protocols = array('http', 'https', 'ftp');
             $length = count($protocols);
             for ($j = 0; $j < $length; $j++) {
                  if (stripos($this->data['News']['link'], $protocols[$j]) !== false) {
                       $this->data['News']['link'] = $this->data['News']['link'];
                       break;
                  }

                  if ($j == $length - 1) {
                  		if(!empty($this->data['News']['link'])){
                       		$this->data['News']['link'] = 'http://' . $this->data['News']['link'];
                  		} else{
                  			$this->data['News']['link'] = null;
                  		}
                  }
             }
        }

		return $return;
	}

	function beforeDelete() {
		$news = $this->find('first', array('conditions' => array('News.id' => $this->id), 'contain' => false));
		$imageDirectory = WWW_ROOT . 'img' . DS . 'images' . DS;

		if (!empty($news['News']['image'])) {
			unlink($imageDirectory . 'news_img_' . $news['News']['image']);
			unlink($imageDirectory . 'news_main_prev_' . $news['News']['image']);
			unlink($imageDirectory . 'news_tbl_prev_' . $news['News']['image']);
			unlink($imageDirectory . 'news_img_edt_' . $news['News']['image']);
		}

		if(!empty($news['DefaultImage'])){
			foreach ($news['DefaultImage'] as $image) {
				unlink($imageDirectory . 'news_extra_img_' . $image['image']);
			}
		}

		return true;
	}

}
