<?php
App::uses('AppModel', 'Model');
/**
 * Comment Model
 *
 * @property EducationalPlan $EducationalPlan
 * @property EducationalProgram $EducationalProgram
 * @property Project $Project
 * @property ImprovementPlan $ImprovementPlan
 * @property Event $Event
 */
class Comment extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'model' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'content' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'EducationalPlan' => array(
			'className' => 'EducationalPlan',
			'foreignKey' => 'educational_plan_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'EducationalProgram' => array(
			'className' => 'EducationalProgram',
			'foreignKey' => 'educational_program_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Project' => array(
			'className' => 'Project',
			'foreignKey' => 'project_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ImprovementPlan' => array(
			'className' => 'ImprovementPlan',
			'foreignKey' => 'improvement_plan_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Event' => array(
			'className' => 'Event',
			'foreignKey' => 'event_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
