<?php
App::uses('AppModel', 'Model');
/**
 * Event Model
 *
 * @property Comment $Comment
 * @property DefaultImage $DefaultImage
 */
class Event extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message'=>'<span class="input-error tooltips" data-original-title="Introduzca un nombre para el evento"><i class="icon-exclamation-sign"></i></span>'
			),
		),
		'description' => array(
			'notempty' => array(
				'rule' => array('notempty')
			),
		),
		'link' => array(
			'website' => array(
				'rule' => 'url',
				'message'=>'<span class="input-error tooltips" data-original-title="Introduzca una URL válida"><i class="icon-exclamation-sign"></i></span>',
				'allowEmpty' => true
			)
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'DefaultImage' => array(
			'className' => 'DefaultImage',
			'foreignKey' => 'event_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	function beforeSave() {
		$return = false;
		$image = null;
		$erase = false;
		$events = null;

		if (!empty($this->id)) {
			$events = $this->find('first', array('conditions' => array('Event.id' => $this->data['Event']['id'])));
			$image = $events['Event']['image'];
		}
		//File upload
		$file = isset($this->data['Event']['image']) ? $this->data['Event']['image'] : null;
		$fileDir = WWW_ROOT . 'img' . DS . 'images';
		$fileName = '';
		$fileExtension = '';

		//A file was uploaded
		if (!empty($file) && !empty($file['tmp_name']) && $file['error'] != 4) {
			//There is no error, the file was uploaded with success
			if ($file['error'] == 0) {

				//Tells whether the file was uploaded via HTTP POST
				if (is_uploaded_file($file['tmp_name'])) {
					App::import('Vendor', 'Bitmap');
					App::import('Vendor', 'BitmapException');

					if (Bitmap::isImage($file['tmp_name'])) {
						try {
							$fileName = md5($file['tmp_name'] . (rand() * 100000));
							$imageSize = getimagesize($file['tmp_name']);
							$width = $imageSize[0];
							$height = $imageSize[1];

							// Original image
							$img = new Bitmap($file['tmp_name']);
							$img->open();
							$img->save($fileDir, 'events_img_' . $fileName);
							$img->dispose();
							$fileExtension = $img->getExtension();

							$img_edt = new Bitmap($file['tmp_name']);
							$img_edt->open();
							$img_edt->resizeToWidthHeight(200, 150);
							$img_edt->save($fileDir, 'events_img_edt_' . $fileName);
							$img_edt->dispose();
							$fileExtension = $img_edt->getExtension();

							// Admin index table preview
							$tablePrev = new Bitmap($file['tmp_name']);
							$tablePrev->open();
							$tablePrev->resizeToWidth(145);
							$tablePrev->save($fileDir, 'events_tbl_prev_' . $fileName);
							$tablePrev->dispose();

							// Portal Index preview
							$mainPrev = new Bitmap($file['tmp_name']);
							$mainPrev->open();
							$mainPrev->resizeToWidthHeight(290, 141);
							$mainPrev->save($fileDir, 'events_main_prev_' . $fileName);
							$mainPrev->dispose();

							$this->data['Event']['size'] = $file['size'];
							$this->data['Event']['image'] = $fileName . '.' .  $fileExtension;
							$return = true;
							$erase = true;
						} catch (BitmapException $e) {
							$this->invalidate('image', 'bitmapError');
						}
					} else {
						$this->invalidate('image', 'unsupportedType');
					}
				} else {
					$this->invalidate('image', 'maliciousUpload');
				}
			} else {
				//File upload error. See http://us3.php.net/manual/en/features.file-upload.errors.php
				$this->invalidate('image', 'uploadError');
			}
		} else {
			if (isset($this->data['Event']['erase']) && $this->data['Event']['erase']) {
				$this->data['Event']['image'] = null;
			} else {
				unset($this->data['Event']['image']);
			}
			$return = true;
		}

		// REMOVE PREVIOUS LOGO
		if (!empty($image) && ((isset($this->data['Event']['erase']) && $this->data['Event']['erase']) || $erase)) {
			unlink($fileDir . DS . 'events_main_prev_' . $image);
			unlink($fileDir . DS . 'events_tbl_prev_' . $image);
			unlink($fileDir . DS . 'events_img_' . $image);
			unlink($fileDir . DS . 'events_img_edt_' . $image);
		}


		// Link value
        if (isset($this->data['Event']['link'])) {
             $protocols = array('http', 'https', 'ftp');
             $length = count($protocols);
             for ($j = 0; $j < $length; $j++) {
                  if (stripos($this->data['Event']['link'], $protocols[$j]) !== false) {
                       $this->data['Event']['link'] = $this->data['Event']['link'];
                       break;
                  }

                  if ($j == $length - 1) {
                  		if(!empty($this->data['Event']['link'])){
                       		$this->data['Event']['link'] = 'http://' . $this->data['Event']['link'];
                  		} else{
                  			$this->data['Event']['link'] = null;
                  		}
                  }
             }
        }

		return $return;
	}

	function beforeDelete() {
		$events = $this->find('first', array('conditions' => array('Event.id' => $this->id), 'contain' => false));
		$imageDirectory = WWW_ROOT . 'img' . DS . 'images' . DS;

		if (!empty($events['Event']['image'])) {
			unlink($imageDirectory . 'events_img_' . $events['Event']['image']);
			unlink($imageDirectory . 'events_main_prev_' . $events['Event']['image']);
			unlink($imageDirectory . 'events_tbl_prev_' . $events['Event']['image']);
			unlink($imageDirectory . 'events_img_edt_' . $events['Event']['image']);
		}

		if(!empty($events['DefaultImage'])){
			foreach ($events['DefaultImage'] as $image) {
				unlink($imageDirectory . 'events_extra_img_' . $image['image']);
			}
		}

		return true;
	}
}
