<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 * @property Group $Group
 */
class User extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'first_name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				'message'=>'<span class="input-error tooltips" data-original-title="Introduzca una dirección de correo electrónico válida"><i class="icon-exclamation-sign"></i></span>'
			),
			'notempty' => array(
				'rule' => array('notempty'),
				'message'=>'<span class="input-error tooltips" data-original-title="Introduzca un correo electrónico"><i class="icon-exclamation-sign"></i></span>'
			),
		),
		'passwd' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message'=>'<span class="input-error tooltips" data-original-title="Introduzca una contraseña"><i class="icon-exclamation-sign"></i></span>'
			),
			'maxlength' => array(
				'rule' => array('maxlength', 15),
				'message' => '<span class="input-error tooltips" data-original-title="La contraseña debe contener hasta 15 caracteres"><i class="icon-exclamation-sign"></i></span>'
			),
			'minlength' => array(
				'rule' => array('minlength', 8),
				'message' => '<span class="input-error tooltips" data-original-title="La contraseña debe contener al menos 8 caracteres"><i class="icon-exclamation-sign"></i></span>'
			),
		),
		'pass' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message'=>'<span class="input-error tooltips" data-original-title="Introduzca su contraseña actual"><i class="icon-exclamation-sign"></i></span>'
			),
			'match'=>array(
				'rule' => 'validateCurrentPassword',
				'message' => '<span class="input-error tooltips" data-original-title="La contraseña actual no coincide."><i class="icon-exclamation-sign"></i></span>'
			)
		),
	    'passwd_confirm' => array(
			'match'=>array(
				'rule' => 'validatePasswdConfirm',
				'message' => '<span class="input-error tooltips" data-original-title="La contraseña nueva debe coincidir en los 2 campos"><i class="icon-exclamation-sign"></i></span>'
			)
	    ),
		'first_name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => '<span class="input-error tooltips" data-original-title="Introduzca su nombre"><i class="icon-exclamation-sign"></i></span>'
			),
		),
		'last_name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => '<span class="input-error tooltips" data-original-title="Introduzca su apellido"><i class="icon-exclamation-sign"></i></span>'
			),
		),
		'role' => array(
			'notempty' => array(
				'rule' => array('notempty')
			),
		),
		'active' => array(
			'notempty' => array(
				'rule' => array('notempty')
			),
		)
	);

	function validatePasswdConfirm($data) {
		if ($this->data['User']['passwd'] !== $data['passwd_confirm']){
		  return false;
		}
		return true;
	}

	function validateCurrentPassword($data) {
		$user = $this->find('first',
			array(
				'fields' => array('password'),
				'conditions' => array('User.id' => $this->data['User']['id'])
			)
		);
		$originalPassword = $user['User']['password'];
		if (AuthComponent::password($data['pass'], null, true) == $originalPassword) {
			return true;
		} else {
			return false;
		}
	}

	function beforeSave() {
		if (isset($this->data['User']['passwd'])) {
	    	$this->data['User']['password'] = AuthComponent::password($this->data['User']['passwd'], null, true);
	    	unset($this->data['User']['passwd']);
	    }

	    if (isset($this->data['User']['passwd_confirm'])) {
	    	unset($this->data['User']['passwd_confirm']);
	    }

	    $return = false;
		$image = null;
		$erase = false;
		$user = null;

		if (!empty($this->id)) {
			$user = $this->find('first', array('conditions' => array('User.id' => $this->data['User']['id'])));
			$image = $user['User']['image'];
		}

		//File upload
		$file = isset($this->data['User']['image']) ? $this->data['User']['image'] : null;
		$fileDir = WWW_ROOT . 'img' . DS . 'images';
		$fileName = '';
		$fileExtension = '';

		//A file was uploaded
		if (!empty($file) && !empty($file['tmp_name']) && $file['error'] != 4) {
			//There is no error, the file was uploaded with success
			if ($file['error'] == 0) {

				//Tells whether the file was uploaded via HTTP POST
				if (is_uploaded_file($file['tmp_name'])) {
					App::import('Vendor', 'Bitmap');
					App::import('Vendor', 'BitmapException');

					if (Bitmap::isImage($file['tmp_name'])) {

						try {
							$fileName = md5($file['tmp_name'] . (rand() * 100000));
							$imageSize = getimagesize($file['tmp_name']);
							$width = $imageSize[0];
							$height = $imageSize[1];

							$img = new Bitmap($file['tmp_name']);
							$img->open();
							$img->save($fileDir, 'user_img_' . $fileName);
							$img->dispose();
							$fileExtension = $img->getExtension();

							$set['height'] = 170;
							$set['width'] = 291;
							$set['align'] = 'center';
							$set['valign'] = 'middle';

							$img_edt = new Bitmap($file['tmp_name']);
							$img_edt->open();
							$img_edt->resizeToWidthHeight(291, 170);
							$img_edt->save($fileDir, 'user_img_edt_' . $fileName);
							$img_edt->dispose();
							$fileExtension = $img_edt->getExtension();

							$set['height'] = 29;
							$set['width'] = 29;
							$set['align'] = 'center';
							$set['valign'] = 'middle';

							$avatar = new Bitmap($file['tmp_name']);
							$avatar->open();
							$avatar->resizeAndCrop2($imageSize, $set);
							$avatar->save($fileDir, 'avatar_' . $fileName);
							$avatar->dispose();

							$this->data['User']['size'] = $file['size'];
							$this->data['User']['image'] = $fileName . '.' .  $fileExtension;
							$return = true;
							$erase = true;
						} catch (BitmapException $e) {
							$this->invalidate('image', 'bitmapError');
						}
					} else {
						$this->invalidate('image', 'unsupportedType');
					}
				} else {
					$this->invalidate('image', 'maliciousUpload');
				}
			} else {
				//File upload error. See http://us3.php.net/manual/en/features.file-upload.errors.php
				$this->invalidate('image', 'uploadError');
			}
		} else {
			if (isset($this->data['User']['erase']) && $this->data['User']['erase']) {
				$this->data['User']['image'] = null;
			} else {
				unset($this->data['User']['image']);
			}
			$return = true;
		}

		// REMOVE PREVIOUS LOGO
		if (!empty($image) && ((isset($this->data['User']['erase']) && $this->data['User']['erase']) || $erase)) {
			unlink($fileDir . DS . 'avatar_' . $image);
			unlink($fileDir . DS . 'user_img_' . $image);
			unlink($fileDir . DS . 'user_img_edt_' . $image);
		}

		return $return;
	}

	function beforeDelete() {
		$user = $this->find('first', array('conditions' => array('User.id' => $this->id), 'contain' => false));

		if (!empty($user['User']['image'])) {
			$imageDirectory = WWW_ROOT . 'img' . DS . 'images' . DS;
			unlink($imageDirectory. 'avatar_' . $user['User']['image']);
			unlink($imageDirectory. 'user_img_' . $user['User']['image']);
			unlink($imageDirectory. 'user_img_edt_' . $user['User']['image']);
		}

		return true;
	}
}
