<?php
App::uses('AppModel', 'Model');
/**
 * Document Model
 */
class Document extends AppModel {

	public $validate = array(
		'file' => array(
			'checksizeedit' => array(
				'rule' => array('checkSize',false),
				'message' => false,
				'on' => 'update'
				),
			'checktypeedit' =>array(
				'rule' => array('checkType',false),
				'message' => false,
				'on' => 'update'
				),
			'checkuploadedit' =>array(
				'rule' => array('checkUpload', false),
				'message' => false,
				'on' => 'update'
				),
			'checksize' => array(
				'rule' => array('checkSize',true),
				'message' => false,
				'on' => 'create'
				),
			'checktype' =>array(
				'rule' => array('checkType',true),
				'message' => false,
				'on' => 'create'
				),
			'checkupload' =>array(
				'rule' => array('checkUpload', true),
				'message' => false,
				'on' => 'create'
				),
		),
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message'=>'<span class="input-error tooltips" data-original-title="Introduzca un nombre para el evento"><i class="icon-exclamation-sign"></i></span>'
			),
		),
	);

	function checkUpload($data, $required = false){
		$data = array_shift($data);
		if(!$required && $data['error'] == 4){
			return true;
		}
		if($required && $data['error'] !== 0){
			return false;
		}
		if($data['size'] == 0){
			return false;
		}
		return true;
	}

	function checkType($data, $required = false){
		$data = array_shift($data);
		if(!$required && $data['error'] == 4){
			return true;
		}
		$allowedExtensions = array('doc', 'docx', 'pdf', 'ppt', 'pptx', 'txt', 'xls');
		$extension = pathinfo($data['name'], PATHINFO_EXTENSION);

		if(!in_array($extension, $allowedExtensions)){
			return false;
		}
		return true;
	}

	function checkSize($data, $required = false){
		$data = array_shift($data);
		if(!$required && $data['error'] == 4){
			return true;
		}
		//||$data['size']/1024 > 2050
		if($data['size'] == 0){
			return false;
		}
		return true;
	}

	function beforeSave() {
		$return = false;
		$document = null;
		$erase = false;
		$planProgram = null;
		$supportedExtensions = array('pdf', 'doc', 'docx', 'ppt', 'pptx', 'txt', 'xls');

		if (!empty($this->id)) {
			$planProgram = $this->find('first', array('conditions' => array('Document.id' => $this->data['Document']['id'])));
			$document = $planProgram['Document']['file'];
		}

		//File upload
		$file = isset($this->data['Document']['file']) ? $this->data['Document']['file'] : null;
		$fileDir = WWW_ROOT . 'files' . DS . 'documents';
		$fileName = '';
		$fileExtension = '';

		//A file was uploaded
		if (!empty($file) && !empty($file['tmp_name']) && $file['error'] != 4) {
			//There is no error, the file was uploaded with success
			if ($file['error'] == 0) {

				//Tells whether the file was uploaded via HTTP POST
				if (is_uploaded_file($file['tmp_name'])) {
					try {
						$fileName = md5($file['tmp_name'] . (rand() * 100000));
						$fileSize = filesize($file['tmp_name']);
						$documentType = $this->data['Document']['type']=='Plan' ? 'plan_' : 'programa_';
						$fileExtension = pathinfo($file['name'], PATHINFO_EXTENSION);

						if (move_uploaded_file($file['tmp_name'], $fileDir . DS . $documentType . $fileName .'.'. $fileExtension)) {
							$return = true;
						} else {
							$return = false;
						}

						$this->data['Document']['size'] = $fileSize;
						$this->data['Document']['extension'] = $fileExtension;
						$this->data['Document']['file'] = $fileName . '.' .  $fileExtension;

						$erase = true;
					} catch (BitmapException $e) {
						$this->invalidate('image', 'bitmapError');
					}
				} else {
					$this->invalidate('image', 'maliciousUpload');
				}
			} else {
				//File upload error. See http://us3.php.net/manual/en/features.file-upload.errors.php
				$this->invalidate('file', 'uploadError');
			}
		} else {
			if (isset($this->data['Document']['erase']) && $this->data['Document']['erase']) {
				$this->data['Document']['file'] = null;
			} else {
				unset($this->data['Document']['file']);
			}
			$return = true;
		}

		// REMOVE PREVIOUS LOGO
		$documentType = $this->data['Document']['type']=='Plan' ? 'plan_' : 'programa_';
		if (!empty($document) && ((isset($this->data['Document']['erase']) && $this->data['Document']['erase']) || $erase)) {
			unlink($fileDir . DS . $documentType.$document);
		}

		return $return;
	}

	function beforeDelete() {
		$planProgram = $this->find('first', array('conditions' => array('Document.id' => $this->id), 'contain' => false));
		$documentType = $planProgram['Document']['type']=='Plan' ? 'plan_' : 'programa_';

		if (!empty($planProgram['Document']['file'])) {
			$imageDirectory = WWW_ROOT . 'files' . DS . 'documents' . DS;
			unlink($imageDirectory . $documentType . $planProgram['Document']['file']);
		}

		return true;
	}
}
