<?php
App::uses('AppController', 'Controller');
/**
 * News Controller
 *
 * @property News $News
 */
class NewsController extends AppController {
	var $name = 'News';
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->News->recursive = 0;
		$this->set('news', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null, $name = null) {
		$viewTitle = 'Noticias';
		if (!$this->News->exists($id)) {
			throw new NotFoundException(__('Invalid news'));
		}
		$options = array('conditions' => array('News.' . $this->News->primaryKey => $id));
		$this->set('news', $this->News->find('first', $options));
		$this->set(compact('viewTitle'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->layout = 'admin/index';
		$viewTitle = 'Noticias';
		$this->News->recursive = 0;
		$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));
		$this->set('news', $this->paginate());

		$this->set(compact('isSuperUser', 'viewTitle'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->layout = 'admin/index';
		$viewTitle = 'Noticias';
		$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));
		if (!$this->News->exists($id)) {
			throw new NotFoundException(__('Invalid news'));
		}
		$options = array('conditions' => array('News.' . $this->News->primaryKey => $id));
		$this->set('news', $this->News->find('first', $options));
		$this->set('model', $this->params['controller']);
		$this->set(compact('isSuperUser', 'viewTitle'));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$this->layout = 'admin/index';
		$viewTitle = 'Noticias';
		$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));
		if ($this->request->is('post')) {
			$this->News->create();
			if ($this->News->save($this->request->data)) {
				$this->Session->setFlash('La noticia <strong>'.$this->request->data['News']['title']. '</strong> ha sido creada.', 'admin/custom_flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('No se pudo crear la noticia.', 'admin/custom_flash_error');
			}
		}
		$this->set(compact('isSuperUser', 'viewTitle'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->layout = 'admin/index';
		$viewTitle = 'Noticias';
		$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));
		if (!$this->News->exists($id)) {
			throw new NotFoundException(__('Invalid news'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->News->save($this->request->data)) {
				$this->Session->setFlash('La noticia ha sido editada.', 'admin/custom_flash_success');
				$this->redirect(array('action' => 'view', $this->request->data['News']['id'], Format::clean($this->request->data['News']['title'])));
			} else {
				$this->Session->setFlash('No se pudo editar la noticia.', 'admin/custom_flash_error');
				$options = array('conditions' => array('News.' . $this->News->primaryKey => $id));
				$this->request->data = $this->News->find('first', $options);
			}
		} else {
			$options = array('conditions' => array('News.' . $this->News->primaryKey => $id));
			$this->request->data = $this->News->find('first', $options);
		}
		$this->set(compact('isSuperUser', 'viewTitle'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->News->id = $id;
		if (!$this->News->exists()) {
			throw new NotFoundException(__('Invalid news'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->News->delete()) {
			$this->Session->setFlash('La Noticia se ha eliminado', 'admin/custom_flash_alert');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('No se pudo eliminar la noticia.', 'admin/custom_flash_error');
		$this->redirect(array('action' => 'index'));
	}

	public function isSuperUser($user) {
		if (isset($user['role']) && $user['role'] === 'Super User') {
			return true;
		}
		return false;
	}
}
