<?php
App::uses('AppController', 'Controller');
/**
 * Events Controller
 *
 * @property Event $Event
 */
class EventsController extends AppController {

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->layout = 'admin/index';
		$viewTitle = 'Eventos';
		$this->Event->recursive = 0;
		$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));
		$this->set('events', $this->paginate());

		$this->set(compact('isSuperUser', 'viewTitle'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->layout = 'admin/index';
		$viewTitle = 'Eventos';
		$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));
		if (!$this->Event->exists($id)) {
			throw new NotFoundException(__('Invalid event'));
		}
		$options = array('conditions' => array('Event.' . $this->Event->primaryKey => $id));
		$this->set('event', $this->Event->find('first', $options));
		$this->set('model', $this->params['controller']);
		$this->set(compact('isSuperUser', 'viewTitle'));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$this->layout = 'admin/index';
		$viewTitle = 'Noticias';
		$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));
		if ($this->request->is('post')) {
			$this->Event->create();
			if ($this->Event->save($this->request->data)) {
				$this->Session->setFlash('El evento <strong>'.$this->request->data['Event']['name'].'</strong> ha sido creado.', 'admin/custom_flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('No se pudo crear el evento.', 'admin/custom_flash_error');
			}
		}
		$this->set(compact('isSuperUser', 'viewTitle'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->layout = 'admin/index';
		$viewTitle = 'Eventos';
		$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));
		if (!$this->Event->exists($id)) {
			throw new NotFoundException(__('Invalid event'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Event->save($this->request->data)) {
				$this->Session->setFlash('El evento <strong>'.$this->request->data['Event']['name'].'</strong> ha sido editado.', 'admin/custom_flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('No se pudo editar el evento.', 'admin/custom_flash_error');
				$options = array('conditions' => array('Event.' . $this->Event->primaryKey => $id));
				$this->request->data = $this->Event->find('first', $options);
			}
		} else {
			$options = array('conditions' => array('Event.' . $this->Event->primaryKey => $id));
			$this->request->data = $this->Event->find('first', $options);
		}
		$this->set(compact('isSuperUser', 'viewTitle'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Event->id = $id;
		if (!$this->Event->exists()) {
			throw new NotFoundException(__('Invalid event'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Event->delete()) {
			$this->Session->setFlash('El evento se ha eliminado', 'admin/custom_flash_alert');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('No se pudo eliminar el evento.', 'admin/custom_flash_error');
		$this->redirect(array('action' => 'index'));
	}

	public function isSuperUser($user) {
		if (isset($user['role']) && $user['role'] === 'Super User') {
			return true;
		}
		return false;
	}

	public function index(){
		$viewTitle = 'Eventos';
		$this->set('events_menu', 'current');
		$this->Event->recursive = 0;
		$this->set('events', $this->paginate());
		$this->set(compact('viewTitle'));
	}

	public function view($id = null, $name = null){
		$viewTitle = 'Eventos';
		$this->set('events_menu', 'current');
		if (!$this->Event->exists($id)) {
			throw new NotFoundException(__('Invalid event'));
		}
		$options = array('conditions' => array('Event.' . $this->Event->primaryKey => $id));
		$this->set('event', $this->Event->find('first', $options));
		$this->set(compact('viewTitle'));
	}
}
