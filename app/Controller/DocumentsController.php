<?php
App::uses('AppController', 'Controller');
/**
 * Documents Controller
 *
 * @property Document $Document
 */
class DocumentsController extends AppController {

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->layout = 'admin/index';
		$viewTitle = 'Planes y Programas';
		$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));
		$this->Document->recursive = 0;
		$this->set('documents', $this->paginate());
		$this->set(compact('isSuperUser', 'viewTitle'));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$this->layout = 'admin/index';
		$viewTitle = 'Planes y Programas';
		$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));
		if ($this->request->is('post')) {
			$this->Document->create();
			if ($this->Document->save($this->request->data)) {
				$this->Session->setFlash('Se ha guardado el plan o programa', 'admin/custom_flash_success');
				if(!empty($this->params['pass'])){
					$this->redirect(array('controller' => $this->params['pass'][0], 'action' => 'view', $this->params['pass'][1]));
				}
				$this->redirect(array('action' => 'index'));
			} else {
				if(!empty($this->Document->validationErrors)){
					if(isset($this->Document->validationErrors['file'][0])){
						$this->Session->setFlash('Debes seleccionar un documento', 'admin/custom_flash_error');
					}
				}else{
					$this->Session->setFlash('El plan o programa no se pudo guardar.', 'admin/custom_flash_error');
				}
			}
		}
		$this->set(compact('isSuperUser', 'viewTitle'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->layout = 'admin/index';
		$viewTitle = 'Planes y Programas';
		$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));
		if (!$this->Document->exists($id)) {
			throw new NotFoundException(__('Invalid document'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Document->save($this->request->data)) {
				$this->Session->setFlash('El documento ha sido editada.', 'admin/custom_flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('El documento no se pudo editar.', 'admin/custom_flash_error');
			}
		} else {
			$options = array('conditions' => array('Document.' . $this->Document->primaryKey => $id));
			$this->request->data = $this->Document->find('first', $options);
		}

		$this->set(compact('isSuperUser', 'viewTitle'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Document->id = $id;
		if (!$this->Document->exists()) {
			throw new NotFoundException(__('Invalid document'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Document->delete()) {
			$this->Session->setFlash('El documento ha sido eliminado.', 'admin/custom_flash_alert');

			if(isset($this->request->data) && !empty($this->request->data)){
				$this->redirect(array('controller' => $this->request->data['controller'], 'action' => 'view', $this->request->data[0]));
			}
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('No se pudo eliminar el documento.', 'admin/custom_flash_error');
		$this->redirect(array('action' => 'index'));
	}

	public function index() {
		$viewTitle = 'Planes y Programas';
		$this->set('plans_programs', 'current');
		$this->Document->recursive = 0;
		$this->set('documents', $this->paginate());
		$this->set(compact('viewTitle'));
	}

	public function isSuperUser($user) {
		if (isset($user['role']) && $user['role'] === 'Super User') {
			return true;
		}
		return false;
	}

	public function download($id) {
		$options = array('conditions' => array('Document.' . $this->Document->primaryKey => $id));
		$document = $this->Document->find('first', $options);
		$documentType = $document['Document']['type']=='Plan' ? 'plan_' : 'programa_';

		$this->response->file(WWW_ROOT . 'files' . DS . 'documents' . DS . $documentType . $document['Document']['file'], array('name'=>$document['Document']['name']));
		return $this->response;
	}
}
