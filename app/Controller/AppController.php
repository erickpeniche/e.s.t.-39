<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $components = array(
		'Session',
		'Auth' => array(
			'loginAction' => array(
				'controller' => 'users',
				'action' => 'login',
				'admin' => false
			),
			'authError' => 'Debes iniciar sesión para ver este contenido',
			'loginRedirect' => array('controller' => 'pages', 'action' => 'home', 'admin' => true),
			'logoutRedirect' => array('controller' => 'pages', 'action' => 'home'),
			'authorize' => array('Controller'),
			'authenticate' => array(
				'Form' => array(
					'fields' => array('username' => 'email'),
					'scope' => array('User.active' => 1)
				)
			)
		),
		'RequestHandler'
	);

	public $helpers = array('Js' => array('Jquery'));

	public function beforeFilter() {
		$this->Auth->allow(
			'about_us', 'contact', 'display', 'home', 'login', 'logout', 'view', 'bathrooms', 'sports_courts',
			'food_conservation', 'theme_garden', 'school_shop', 'entrance', 'school_emblem', 'entry_garden',
			'outside_garden', 'classrooms', 'school_staff', 'civic_plaza', 'social_work', 'social_service',
			'technologies', 'agriculture', 'student_society', 'vision', 'proverbs', 'lecture', 'travel_bags',
			'index', 'view', 'download'
			);
		if ($this->Session->check('Config.language')) {
			Configure::write('Config.language', $this->Session->read('Config.language'));
		}

		// DB Configurations
		$this->loadModel('Configuration');
		$config['configurations'] = $this->Configuration->find('list', array(
			'fields' => array('key', 'value')
		));
		Configure::write('App.configurations', $config['configurations']);

		$this->set('config', $config);
	}

	public function isAuthorized($user) {
		// Admin can access every action
		if (isset($user['role']) && $user['role'] != 'Usuario') {
			return true;
		}

		// Default deny
		return false;
	}

	public function isSuperUser($user) {
		if (isset($user['role']) && $user['role'] === 'Super User') {
			return true;
		}
		return false;
	}
}
