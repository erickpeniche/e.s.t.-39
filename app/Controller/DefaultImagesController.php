<?php
App::uses('AppController', 'Controller');
/**
 * DefaultImages Controller
 *
 * @property DefaultImage $DefaultImage
 */
class DefaultImagesController extends AppController {

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->layout = 'admin/index';
		$viewTitle = 'Imágenes';
		$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));
		$fields = array('DefaultImage.file', 'DefaultImage.description');
		$defaultImages['news'] = $this->DefaultImage->find('all', array('fields' => $fields, 'conditions' => array('DefaultImage.model' => 'news')));
		$defaultImages['events'] = $this->DefaultImage->find('all', array('fields' => $fields, 'conditions' => array('DefaultImage.model' => 'events')));
		$this->set(compact('defaultImages', 'isSuperUser', 'viewTitle'));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$this->layout = 'admin/index';
		$viewTitle = 'Imágenes';
		$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));
		if ($this->request->is('post')) {
			$this->DefaultImage->create();
			if ($this->DefaultImage->save($this->request->data)) {
				$this->Session->setFlash('Se ha guardado la imagen', 'admin/custom_flash_success');
				if(!empty($this->params['pass'])){
					$this->redirect(array('controller' => $this->params['pass'][0], 'action' => 'view', $this->params['pass'][1]));
				}
				$this->redirect(array('action' => 'index'));
			} else {
				if(!empty($this->DefaultImage->validationErrors)){
					if(isset($this->DefaultImage->validationErrors['file'][0])){
						$this->Session->setFlash('Debes seleccionar una imagen', 'admin/custom_flash_error');
					}
				}else{
					$this->Session->setFlash('La imagen no se pudo guardar.', 'admin/custom_flash_error');
				}
			}
		}
		$news = $this->DefaultImage->News->find('list');
		$events = $this->DefaultImage->Event->find('list');
		$this->set(compact('news', 'events', 'isSuperUser', 'viewTitle'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->layout = 'admin/index';
		if (!$this->DefaultImage->exists($id)) {
			throw new NotFoundException(__('Invalid default image'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->DefaultImage->save($this->request->data)) {
				$this->Session->setFlash('La imagen ha sido editada.', 'admin/custom_flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('La imagen no se pudo editar.', 'admin/custom_flash_error');
			}
		} else {
			$options = array('conditions' => array('DefaultImage.' . $this->DefaultImage->primaryKey => $id));
			$this->request->data = $this->DefaultImage->find('first', $options);
		}
		$news = $this->DefaultImage->News->find('list');
		$plants = $this->DefaultImage->Plant->find('list');
		$technologies = $this->DefaultImage->Technology->find('list');
		$educationalPlans = $this->DefaultImage->EducationalPlan->find('list');
		$educationalPrograms = $this->DefaultImage->EducationalProgram->find('list');
		$projects = $this->DefaultImage->Project->find('list');
		$improvementPlans = $this->DefaultImage->ImprovementPlan->find('list');
		$events = $this->DefaultImage->Event->find('list');
		$this->set(compact('news', 'plants', 'technologies', 'educationalPlans', 'educationalPrograms', 'projects', 'improvementPlans', 'events'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->DefaultImage->id = $id;
		if (!$this->DefaultImage->exists()) {
			throw new NotFoundException(__('Invalid default image'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->DefaultImage->delete()) {
			$this->Session->setFlash('La imagen ha sido eliminada.', 'admin/custom_flash_alert');

			if(isset($this->request->data) && !empty($this->request->data)){
				$this->redirect(array('controller' => $this->request->data['controller'], 'action' => 'view', $this->request->data[0]));
			}
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('No se pudo eliminar la imagen.', 'admin/custom_flash_error');
		$this->redirect(array('action' => 'index'));
	}

	public function isSuperUser($user) {
		if (isset($user['role']) && $user['role'] === 'Super User') {
			return true;
		}
		return false;
	}
}
