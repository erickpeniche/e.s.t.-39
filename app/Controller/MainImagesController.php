<?php
App::uses('AppController', 'Controller');
/**
 * MainImages Controller
 *
 * @property MainImage $MainImage
 */
class MainImagesController extends AppController {

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->layout = 'admin/index';
		$viewTitle = 'Imágenes principales';

		$images = $this->MainImage->find('all');

		$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));

		$this->set(compact('images', 'isSuperUser', 'viewTitle'));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$this->layout = 'admin/index';
		$viewTitle = 'Imágenes principales';
		if ($this->request->is('post')) {
			$this->MainImage->create();
			if ($this->MainImage->save($this->request->data)) {
				$this->Session->setFlash('Se ha guardado la imagen con el título "'.$this->request->data['MainImage']['name']. '"', 'admin/custom_flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('La imagen no se pudo crear.', 'admin/custom_flash_error');
			}
		}
		$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));
		$this->set(compact('isSuperUser', 'viewTitle'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->layout = 'admin/index';
		$viewTitle = 'Imágenes principales';
		if (!$this->MainImage->exists($id)) {
			throw new NotFoundException(__('Invalid main image'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->MainImage->save($this->request->data)) {
				$this->Session->setFlash('La imagen ha sido editada.', 'admin/custom_flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('La imagen no se pudo editar.', 'admin/custom_flash_error');
				$options = array('conditions' => array('MainImage.' . $this->MainImage->primaryKey => $id));
				$this->request->data = $this->MainImage->find('first', $options);
			}
		} else {
			$options = array('conditions' => array('MainImage.' . $this->MainImage->primaryKey => $id));
			$this->request->data = $this->MainImage->find('first', $options);
		}
		$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));
		$this->set(compact('isSuperUser', 'viewTitle'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->MainImage->id = $id;
		if (!$this->MainImage->exists()) {
			throw new NotFoundException(__('Invalid main image'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->MainImage->delete()) {
			$this->Session->setFlash('La imagen ha sido eliminada.', 'admin/custom_flash_alert');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('No se pudo eliminar la imagen.', 'admin/custom_flash_error');
		$this->redirect(array('action' => 'index'));
	}

	public function isSuperUser($user) {
		if (isset($user['role']) && $user['role'] === 'Super User') {
			return true;
		}
		return false;
	}
}
