<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * Controller name
 *
 * @var string
 */
	public $name = 'Pages';

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();

	public function home() {
		$viewTitle = 'Inicio';
		//Clase Current al menu de navegación
		$this->set('home', 'current');
		$this->loadModel('MainImage');
		$this->loadModel('News');
		$this->loadModel('Event');
		$this->loadModel('Document');

		$mainImages = $this->MainImage->find('all');
		$news = $this->News->find('all',
			array(
				'limit' => 2,
				'order' => array('News.created DESC')
				)
			);

		$events = $this->Event->find('all',
			array(
				'limit' => 3,
				'order' => array('Event.created DESC')
				)
			);

		$plans = $this->Document->find('all',
			array(
				'limit' => 5,
				'conditions' => array('Document.type' => 'Plan'),
				'order' => array('Document.created DESC')
				)
			);

		$programs = $this->Document->find('all',
			array(
				'limit' => 5,
				'conditions' => array('Document.type' => 'Program'),
				'order' => array('Document.created DESC')
				)
			);

		$this->set(compact('mainImages', 'news', 'events', 'viewTitle', 'plans', 'programs'));

	}

	public function about_us (){
		$viewTitle = 'Nosotros';
		//Clase Current al menu de navegación
		$this->set('about_us', 'current');
		$this->set(compact('viewTitle'));
	}

	public function contact (){
		$viewTitle = 'Contacto';
		//Clase Current al menu de navegación
		$confs = Configure::read('App.configurations');
		if($this->request->is('ajax')){
			Configure::write('debug', 0);
			$this->layout = 'ajax';
			$Email = new CakeEmail('temozon');
			$Email->helpers(array('Html', 'Text', 'Js'));
			$Email->template('text', 'contacto');
			$Email->emailFormat('html');
			$Email->viewVars(array(
				'contactName' => $this->request->data['name'],
				'contactMail' => $this->request->data['e-mail'],
				'contactPhone' => $this->request->data['phone'],
				'contactMessage' => $this->request->data['message'],
				)
			);
			$Email->to('contacto@est39temozon.com');
			$Email->subject('Contacto E.S.T 39');
			$Email->send();
		}elseif($this->request->is('post') || $this->request->is('put')) {
			debug('isPost o Put');
			debug($this->request->data);
			exit();
		}
		$this->set('contact', 'current');
		$this->set(compact('confs', 'viewTitle'));
	}

	public function admin_home() {
		$this->layout = 'admin/index';
		$viewTitle = 'Panel de Administración';
		$viewSubTitle = Configure::read('App.configurations.website-title');

		$this->loadModel('User');
		$this->loadModel('MainImage');
		$this->loadModel('News');
		$this->loadModel('Events');
		$this->loadModel('DefaultImage');

		$totalUsers = $this->User->find('count');
		$totalMainImages = $this->MainImage->find('count');
		$totalNews = $this->News->find('count');
		$totalEvents = $this->Events->find('count');
		$totalImages = $this->DefaultImage->find('count');

		$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));

		$this->set(compact('totalUsers', 'totalMainImages', 'totalNews', 'totalEvents', 'totalImages', 'isSuperUser', 'viewTitle', 'viewSubTitle'));
	}

/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 */
	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			$this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));
		$this->render(implode('/', $path));
	}

	public function isSuperUser($user) {
		if (isset($user['role']) && $user['role'] === 'Super User') {
			return true;
		}
		return false;
	}

	/*************** Static Views ****************/
	public function vision(){
		$viewTitle = 'Misión y Visión';
		//Clase Current al menu de navegación
		$this->set('about_us', 'current');
		$this->set(compact('viewTitle'));
	}

	public function bathrooms(){
		$viewTitle = 'Plantel - Baños';
		//Clase Current al menu de navegación
		$this->set('about_us', 'current');
		$this->set(compact('viewTitle'));
	}

	public function sports_courts(){
		$viewTitle = 'Plantel - Canchas deportivas';
		//Clase Current al menu de navegación
		$this->set('about_us', 'current');
		$this->set(compact('viewTitle'));
	}

	public function food_conservation(){
		$viewTitle = 'Plantel - Conservación e industrialización de alimentos';
		//Clase Current al menu de navegación
		$this->set('about_us', 'current');
		$this->set(compact('viewTitle'));
	}

	public function theme_garden(){
		$viewTitle = 'Plantel - Construyendo jardín temático';
		//Clase Current al menu de navegación
		$this->set('about_us', 'current');
		$this->set(compact('viewTitle'));
	}

	public function school_shop(){
		$viewTitle = 'Plantel - Cooperativa escolar';
		//Clase Current al menu de navegación
		$this->set('about_us', 'current');
		$this->set(compact('viewTitle'));
	}

	public function entrance(){
		$viewTitle = 'Plantel - Entrada';
		//Clase Current al menu de navegación
		$this->set('about_us', 'current');
		$this->set(compact('viewTitle'));
	}

	public function school_emblem(){
		$viewTitle = 'Plantel - Escudo escolar';
		//Clase Current al menu de navegación
		$this->set('about_us', 'current');
		$this->set(compact('viewTitle'));
	}

	public function entry_garden(){
		$viewTitle = 'Plantel - Jardines entrada';
		//Clase Current al menu de navegación
		$this->set('about_us', 'current');
		$this->set(compact('viewTitle'));
	}

	public function outside_garden(){
		$viewTitle = 'Plantel - Jardines externos';
		//Clase Current al menu de navegación
		$this->set('about_us', 'current');
		$this->set(compact('viewTitle'));
	}

	public function classrooms(){
		$viewTitle = 'Plantel - Jardines y Salones';
		//Clase Current al menu de navegación
		$this->set('about_us', 'current');
		$this->set(compact('viewTitle'));
	}

	public function school_staff(){
		$viewTitle = 'Plantel - Personal de la escuela';
		//Clase Current al menu de navegación
		$this->set('about_us', 'current');
		$this->set(compact('viewTitle'));
	}

	public function civic_plaza(){
		$viewTitle = 'Plantel - Plaza cívica y dirección';
		//Clase Current al menu de navegación
		$this->set('about_us', 'current');
		$this->set(compact('viewTitle'));
	}

	public function social_work(){
		$viewTitle = 'Plantel - Prefectura y trabajo social';
		//Clase Current al menu de navegación
		$this->set('about_us', 'current');
		$this->set(compact('viewTitle'));
	}

	public function social_service(){
		$viewTitle = 'Servicio Social';
		//Clase Current al menu de navegación
		$this->set('about_us', 'current');
		$this->set(compact('viewTitle'));
	}

	public function technologies(){
		$viewTitle = 'Tecnologías';
		//Clase Current al menu de navegación
		$this->set('about_us', 'current');
		$this->set(compact('viewTitle'));
	}

	public function agriculture(){
		$viewTitle = 'Tecnologías - Agricultura';
		//Clase Current al menu de navegación
		$this->set('about_us', 'current');
		$this->set(compact('viewTitle'));
	}

	public function student_society(){
		$viewTitle = 'Sociedad de Alumnos';
		//Clase Current al menu de navegación
		$this->set('about_us', 'current');
		$this->set(compact('viewTitle'));
	}

	public function proverbs(){
		$viewTitle = 'Proyectos - Refranes y Valores';
		//Clase Current al menu de navegación
		$this->set('projects_plans', 'current');
		$this->set(compact('viewTitle'));
	}

	public function lecture(){
		$viewTitle = 'Planes de mejora - Lectura: rapidez, fluidez y comprensión';
		//Clase Current al menu de navegación
		$this->set('projects_plans', 'current');
		$this->set(compact('viewTitle'));
	}

	public function travel_bags(){
		$viewTitle = 'Planes de mejora - Sabucanes viajeras';
		//Clase Current al menu de navegación
		$this->set('projects_plans', 'current');
		$this->set(compact('viewTitle'));
	}
}
