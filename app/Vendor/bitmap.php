<?php
/**
 * Bitmap
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 */
class Bitmap {    
    /**
     * Image file name (path)
     *
     * @access private
     * @var string
     */
    private $file     = false;
    /**
     * Image file original name
     *
     * @access private
     * @var string
     */
    private $fileO     = false;
    /**
     * Limits
     *
     * @access private
     * @var array
     */
    private $limit     = array();
    /**
     * Image resource
     *
     * @access private
     * @var resource
     */
    private $resource = false;
    /**
     * Image width
     *
     * @access private
     * @var integer
     */
    private $width    = null;
    /**
     * Image height
     *
     * @access private
     * @var integer
     */
    private $height   = null;
    /**
     * Image type
     *
     * @access private
     * @var string
     */
    private $type     = null;
    /**
     * Image extension (gif/jpg/png)
     *
     * @access private
     * @var string
     */
    private $extension     = null;
    /**
     * Font's directory
     *
     * @access private
     * @var string
     */
    private $fontDir     = 'fonts/';

    /**
     * Text resolution format
     *
     * @access public
     * @var string
     */
     public $textResolution  = '%x X %y PX';

    /**
     * Errors
     */
	const NoExists          = 'Image file no exists';
	const ErrorOpen         = 'Image file no opened';
	const ErrorComposeOpen  = 'Image to compose is no opened';
	const ErrorFormat       = 'Wrong file format';
	const ErrorCrop         = 'Error croping image';
	const ErrorResize       = 'Error resizing image';
	const ErrorAutoResize    = 'Error auto resizing image';
	const ErrorResizeCanvas = 'Error croping image';
	const ErrorCompose      = 'Error compose image';
	const ErrorGrayScale    = 'Error filter grayscale';
	const ErrorBrightness   = 'Error filter brightness';
	const ErrorNegative     = 'Error filter negative';
	const ErrorContrast     = 'Error filter contrast';
	const ErrorColorize     = 'Error filter colorize';
	const ErrorSave         = 'Error saving image';
	const ErrorFixed        = 'You have to specify fixed position x and y';
	const ErrorColor        = 'Wrong color';
	const ErrorText         = 'Error writing text';
	const ErrorDirFont		= "Font directory don't exist";
	const DirNoExists       = 'Directory not exists';
	const DirNoWritable     = 'Directory not writable';
	const LimitFileSize     = 'File size is over limit';
	const LimitType         = 'Image type is disabled';
	const LimitMaxWidth     = 'Image height is to large';
	const LimitMaxHeight    = 'Image width is to large';
	const LimitMinWidth     = 'Image height is to small';
	const LimitMinHeight    = 'Image width is to small';

    /**
     * Image Position
     */
	const TOP      = 0;
	const MIDDLE   = 1;
	const BOTTOM   = 2;
	const LEFT     = 4;
	const CENTER   = 8;
	const RIGHT    = 16;
	const CENTERED = 9;
	const FIXED    = 92;

    /**
     * Image Text
     */
	const Resolution = 25;
	const Size       = 52;

/**
 * Returns true if file is an image
 *
 * @param string $file File path
 * @return boolean True if file is an image
 * @access public
 * @static
 */	
	function isImage($file) {
		if ($data = getimagesize($file)) { 
			//Image type constants 1,2,3 are equivalent to gif, jpeg, png  
			if ($data[2] > 0 && $data[2] < 4) {
				return true;
			} 
		}		
		return false;				
	}
    /**
     * Constructor
     *
     * @access public
     * @param string $file path to image file
     * @return void
     */
    public function __construct($file = false) {
        $this->file = $file;
    }
    /**
     * Sets limits of the image
     *
     * @access public
     * @parm float $maxFileSize
     * @parm string $sizeFormat (KB,MB,GB)
     * @parm array|integer $imageType
     * @parm integer $maxWidth
     * @parm integer $maxHeight
     * @parm integer $minWidth
     * @parm integer $minHeight
     * @return Image
     */
    public function setLimit($maxFileSize = null, $sizeFormat = null, $imageType = null, $maxWidth = null, $maxHeight = null, $minWidth = null, $minHeight = null) {
        $maxFileSize = (float)$maxFileSize;
        $sizeArray    = array('KB', 'MB', 'GB');

        if (in_array($sizeFormat, $sizeArray)) {
            foreach ($sizeArray as $sign) {
                    $maxFileSize = $maxFileSize * 1024;
                if ($sign == $sizeFormat) {
                    break;
                } else {
                    continue;
                }
            }
        }

        $this->limit['FileSize']   = $maxFileSize;
        $this->limit['Type']       = (array)$imageType;
        $this->limit['MaxWidth']  = $maxWidth;
        $this->limit['MaxHeight'] = $maxHeight;
        $this->limit['MinWidth']  = $minWidth;
        $this->limit['MinHeight'] = $minHeight;

        return $this;
    }
    /**
     * Opens the image
     *
     * @access public
     * @return Image
     */
    public function open() {
        if (!file_exists($this->file)) {
            throw new BitmapException(self::NoExists);
        }

        $image = @getimagesize($this->file);
        if(!$image) {
            throw new BitmapException(self::ErrorFormat);
        }

        $this->width  = $image[0];
        $this->height = $image[1];
        $this->type   = $image[2];

        if (!empty($this->limit)) {
            if (!empty($this->limit['FileSize']) && ($this->limit['FileSize'] < filesize($this->file))) {
                throw new BitmapException(self::LimitFileSize);
            }

            if (!empty($this->limit['Type']) && !in_array($this->type, $this->limit['Type'])) {
                throw new BitmapException(self::LimitType);
            }

            if (!empty($this->limit['MaxWidth']) && $this->limit['MaxWidth'] < $this->width) {
                throw new BitmapException(self::LimitMaxWidth);
            }
            
            if (!empty($this->limit['MaxHeight']) && $this->limit['MaxHeight'] < $this->height) {
                throw new BitmapException(self::LimitMaxHeight);
            }
            
            if (!empty($this->limit['MinWidth']) && $this->limit['MinWidth'] > $this->width) {
                throw new BitmapException(self::LimitMinWidth);
            }
            
            if (!empty($this->limit['MinHeight']) && $this->limit['MinHeight'] > $this->height) {
                throw new BitmapException(self::LimitMinHeight);
            }
        }

        switch ($this->type) {
            case IMAGETYPE_JPEG: 
            	$this->resource = imageCreateFromJpeg($this->file); 
            	$this->extension = 'jpg'; 
            	break;
            case IMAGETYPE_PNG:  
            	$this->resource = imageCreateFromPng($this->file);  
            	$this->extension = 'png'; 
            	break;
            case IMAGETYPE_GIF:  
            	$this->resource = imageCreateFromGif($this->file);  
            	$this->extension = 'gif'; 
            	break;
            default:
                throw new BitmapException(self::ErrorFormat);
        }
        return $this;
    }

    /**
     * Loads image file from $_FILES
     *
     * @access public
     * @parm string $name Index from $_FILES array
     * @return boolean
     */
    public function upload($name = null) {
        if (!empty($name)) {
            if (isset($_FILES[$name]) && !empty($_FILES[$name])) {
                if (!is_array($_FILES[$name]['tmp_name'])) {
                    $this->file  = $_FILES[$name]['tmp_name'];
                    $this->fileO = $_FILES[$name]['name'];
                    $this->open();
                    
                    return true;
                }
            }
        }
        
        return false;
    }
    /**
     * Returns Image filename with path
     *
     * @access public
     * @parm boolean $type true - true location of image, false - temporary location if uploaded
     * @return string
     */
    public function getFile($type = false) {
        return ($this->fileO && $type == false) ? $this->fileO : (($this->file) ? $this->file : false);
    }
    /**
     * Returns Image filename with path
     *
     * @access public
     * @parm boolean $type true - true location of image, false - temporary location if uploaded
     * @return string
     */
    public function getName($type = false) {
        return ($this->file) ? basename($this->getFile($type)) : false;
    }

    /**
     * Returns Image directory
     *
     * @access public
     * @parm boolean $type true - true location of image, false - temporary location if uploaded
     * @return string
     */
    public function getDirectory($type = false) {
        return ($this->file) ? (dirname($this->getFile($type)) . '/') : false;

    }
    /**
     * Returns Image resource
     *
     * @access public
     * @return resource
     */
    public function getResource() {
        return ($this->resource) ? $this->resource : false;
    }

    /**
     * Returns Image width
     *
     * @access public
     * @return string
     */
    public function getWidth() {
        return ($this->resource) ? $this->width : false;
    }
    /**
     * Returns Image height
     *
     * @access public
     * @return string|boolean
     */
    public function getHeight() {
        return ($this->resource) ? $this->height : false;
    }

    /**
     * Returns Image file extension
     *
     * @access public
     * @return string
     */
    public function getExtension() {
        return ($this->resource) ? $this->extension : false;
    }

    /**
     * Frees any memory associated with image image
     *
     * @access public
     * @return boolean
     */
    public function dispose() {
        if (!$this->resource) {
            throw new BitmapException(self::ErrorOpen);
        }

        return imagedestroy($this->resource);
    }

    /**
     * Crops image
     *
     * @access public
     * @param integer $x1
     * @param integer $y1
     * @param integer $x2
     * @param integer $y2
     * @return Image
     */
    public function crop($x1, $y1, $x2, $y2) {
        if (!$this->resource) {
            throw new BitmapException(self::ErrorOpen);
        }

        $width  = $x2 - $x1;
        $height = $y2 - $y1;

        $image = imagecreatetruecolor($width, $height);

        if (!imagecopy($image, $this->resource, 0, 0, $x1, $y1, $width, $height)) {
            throw new BitmapException(self::ErrorCrop);
        }

        $this->resource  = $image;
        $this->width  = $width;
        $this->height = $height;

        return $this;
    }
    
    public function autoResize ($width, $height) {
        if (!$this->resource) {
            throw new BitmapException(self::ErrorOpen);
        }

		$x = $this->width;
		$y = $this->height;
		    
		if($width == $x && $height == $y) {
		    return $this;
		}
		    
		if($width > $x || $height > $y) {
		    throw new Image_Exception(self::ErrorAutoResize);
		}
		    
		$x1 = 0;
		$y1 = 0;
		    
		$xm1 = round(($y * $width) / $height);
		    
		$ym1 = round(($height * $x) / $width);
		    
		if($xm1 < $x) {
		    $x1 = floor(($x-$xm1)/2);
		    $x  = $xm1;
		} elseif ($ym1 < $y) {
		    $y1 = floor(($y - $ym1)/2);
		    $y  = $ym1;
		}
		    
		$this->crop($x1, $y1, $x, $y, $asCopy);
		$this->resize($width, $height);

        return $this;
    }    

    /**
     * Resizes image
     *
     * @access public
     * @param integer $width
     * @param integer $height
     * @return Image
     */
    public function resize($width, $height) {
        if (!$this->resource) {
            throw new BitmapException(self::ErrorOpen);
        }

        $image = imagecreatetruecolor($width, $height);

        if(!imagecopyresampled($image, $this->resource, 0, 0, 0, 0, $width, $height, $this->width, $this->height)) {
            throw new BitmapException(self::ErrorResize);
        }

        $this->resource  = $image;
        $this->width     = $width;
        $this->height    = $height;

        return $this;
    }

    /**
     * Resizes image canvas
     *
     * @access public
     * @param integer $width
     * @param integer $height
     * @param integer $place
     * @param string $background
     * @param integer $fixedX
     * @param integer $fixedY
     *
     * @return Image
     */
    public function resizeCanvas($width, $height, $place = self::CENTERED, $background = '#fff', $fixedX = null, $fixedY = null) {
        if (!$this->resource) {
            throw new BitmapException(self::ErrorOpen);
        }

        $image = imagecreatetruecolor($width, $height);

        $color = $this->__allocateColor($background, $image);

        imagefill($image, 0, 0, $color);

        $position = $this->__getPosition($width, $height, $this->width, $this->height, $place, $fixedX, $fixedY);


        if (imagecopy($image, $this->resource, $position[0], $position[1], 0, 0, $this->width, $this->height)) {
            $this->resource  = $image;
            $this->width     = $width;
            $this->height    = $height;

            return $this;
        } else {
            throw new BitmapException(self::ErrorResizeCanvas);
        }
    }

    /**
     * Resizes image keeping aspect ratio
     *
     * @access public
     * @param integer $width
     * @param integer $height
     *
     * @return Image
     */
    public function resizeToWidthHeight($width, $height) {
        if (!$this->resource) {
            throw new BitmapException(self::ErrorOpen);
        }
 
        if ($this->width < $this->height) {
			return $this->resizeToHeight($height);
        } else {
			return $this->resizeToWidth($width);
        }
    }

    /**
     * Resizes image by width aspect ratio
     *
     * @access public
     * @param integer $width
     * @return Image
     */
    public function resizeToWidth($width) {
        if(!$this->resource) {
            throw new BitmapException(self::ErrorOpen);
        }

        $height = ($width * $this->height) / $this->width;

        return $this->resize($width, $height);
    }

    /**
     * Resizes image by height aspect ratio
     *
     * @access public
     * @param integer $height
     * @return Image
     */
    public function resizeToHeight($height) {
        if (!$this->resource) {
            throw new BitmapException(self::ErrorOpen);
        }

        $width = ($this->width * $height) / $this->height;

        return $this->resize($width, $height);
    }
    /**
     * Resizes image and crop 
     */
    public function resizeAndCrop($width, $height) {		
		if ($this->width < $this->height) {									
			$this->resizeToWidth($width);
			
			$x = 0;
			$y = ($this->height - $height)/2;																		
		} else {
			$this->resizeToHeight($height);
			
			$y = 0;
			$x = ($this->width - $width)/2;
		}
		
		return $this->crop($x, $y, $x + $width, $y + $height);    	
    } 
	
	public function resizeAndCrop2($imageSize, $set) {
           $aspectRatio = $imageSize[1] / $imageSize[0];
           
               if ($aspectRatio < ($set['height'] / $set['width']))
               {
                       $this->resizeToHeight($set['height']);
               }
               else
               {
                       $this->resizeToWidth($set['width']);
               }
               
               if ($set['align'] == 'left') {
                       $x1 = 0;
                       $x2 = $set['width'];
               } else if ($set['align'] == 'right'){
                       $x1 = $this->getWidth() - $set['width'];
                       $x2 = $this->getWidth();
               } else {
                       $x1 = ($this->getWidth() / 2) - ($set['width'] / 2);
                       $x2 = ($this->getWidth() / 2) + ($set['width'] / 2);
               }
               
               if ($set['valign'] == 'top') {
                       $y1 = 0;
                       $y2 = $set['height'];
               } else if ($set['valign'] == 'bottom'){
                       $y1 = $this->getHeight() - $set['height'];
                       $y2 = $$this->getHeight();
               } else {
                       $y1 = ($this->getHeight() / 2) - ($set['height'] / 2);
                       $y2 = ($this->getHeight() / 2) + ($set['height'] / 2);
               }
               
               return $this->crop($x1, $y1, $x2, $y2);
   }
    
    /**
     * Composes two images as one
     *
     * @access public
     * @param Image $image
     * @param integer $place
     * @param integer $opacity
     * @param integer $fixedX
     * @param integer $FixedY
     * @return Image
     */
    public function compose(Image $image, $place = self::CENTERED, $opacity = 100, $fixedX = null, $fixedY = null) {
        if (!$image->getResource()) {
            throw new BitmapException(self::ErrorOpen);
        } elseif(!$this->resource) {
            throw new BitmapException(self::ComposeOpen);
        }

        $opacity = round((int) $opacity);
        $opacity = ($opacity > 100) ? 100 : (($opacity < 0) ? 0 : $opacity);

        $position = $this->__getPosition($this->width, $this->height, $image->getWidth(), $image->getHeight(), $place, $fixedX, $fixedY);

        if (imagecopymerge($this->resource, $image->getResource(), $position[0], $position[1], 0, 0, $image->getWidth(), $image->getHeight(), $opacity)) {
            return $this;
        } else {
            throw new BitmapException(self::ErrorCompose);
        }
    }

    /**
     * Write text in image
     *
     * @access public
     * @param Image $image Image file to get size and resolution
     * @param string|array $text Text to write
     * @param string $font font name
     * @param integer $border Border image
     * @param string $background Background color
     * @param string $fontColor Font color
     * @param integer $position Position of text
     * @param integer $space Space between lines
     * @param string  $separator Spearator in Resolutiotion and Size
     * @param string  $size Size image (B,KB,MB,GB)
     * @return Image
     */
    public function drawText($image = null, $text = null, $font, $border = 1, $background = '#000000', $fontColor = '#FFFFFF', $position = self::BOTTOM, $space = 3, $separator = ' ', $size = 'KB') {
		if (!$this->resource) {
            throw new BitmapException(self::ErrorOpen);
        }

        if (!file_exists($this->fontDir) || !is_dir($this->fontDir)) {
            throw new BitmapException(self::ErrorDirFont);
        } elseif (!file_exists($this->fontDir . $font) || !is_file($this->fontDir . $font)) {
            throw new BitmapException(self::ErrorFont);
        }

        if (!is_null($image)) {
            if ($image instanceof Image) {
                $imageWidth = $image->getWidth();
                $imageHeight = $image->getHeight();
                $imageSize  = filesize($image->getFile(true));
            } else {
                throw new BitmapException(self::ErrorOpen);
            }
        } else {
            $imageWidth = $this->width;
            $imageHeight = $this->height;
            $imageSize  = filesize($this->file);
        }


        $heightAdd = 0;
        $widthAdd  = $border * 2;
        $widthNew  = $this->width + $widthAdd;

        $stringWidth = $widthNew - ($space * 2);
        $sizeArray      = array('KB', 'MB', 'GB');
        $sizeFile       = $imageSize;
        if (in_array($size, $sizeArray)) {
            foreach ($sizeArray as $sign) {
                $sizeFile = $sizeFile / 1024;
                if ($sign == $size) {
                    break;
                } else {
                    continue;
                }
            }
            $sizeFile = round ($sizeFile, 2);
        }

        $textSize       = $sizeFile . ' ' . ((in_array($size, $sizeArray)) ? $size : 'B');
        $textResolution = str_replace(array('%x','%y'), array($imageWidth, $imageHeight), $this->textResolution);

        $textResolutionSize  = $textResolution . $separator . $textSize;
        $textSizeResolution  = $textSize . $separator . $textResolution;

        $font        = imageloadfont($this->fontDir . $font);
        $fontHeight  = imagefontheight($font);
        $fontWidth   = imagefontwidth($font);
        $stringBreak = floor($stringWidth / $fontWidth);

        /**
         *  Splits text to new line if lenght text > width image
         */
        if (is_array($text)) {
            $textArray = array();
            foreach ($text as $value) {

                if($value == self::Resolution) {
                    $value = $textResolution;
                } elseif($value == self::Size) {
                    $value = $textSize;
                } elseif($value == self::Size . self::Resolution) {
                    $value = $textSizeResolution;
                } elseif($value == self::Resolution . self::Size) {
                    $value = $textResolutionSize;
                }

                $value       = wordwrap($value, $stringBreak, "\n");
                $_tarray     = explode("\n", $value);
                $textArray   = array_merge($textArray, (array) $_tarray);

                $heightAdd  += ($fontHeight + $space) * count((array)$_tarray);

            }
        } else {
            $text       = wordwrap($text, $stringBreak, "\n");
            $textArray  = explode("\n", $text);

            $heightAdd  += ($fontHeight + $space) * count($textArray);
        }

        $heightNew  = $this->height + $heightAdd + $space;

        switch ($position) {
            case self::BOTTOM:
                $spaceY = $border;
                $spaceT = $this->height + $space + $border;
            	break;

            case self::TOP:
                $spaceY = $heightAdd + $space - $border;
                $spaceT = $space + $border;
            	break;
        }

        $imageNew = imagecreatetruecolor($widthNew, $heightNew);
        $background = $this->__allocateColor($background, $imageNew);
        imagefill($imageNew, 0, 0, $background);
        imagerectangle($imageNew, 0, 0, $this->width, $this->height, $background);
        imagecopyresampled($imageNew, $this->resource, $border, $spaceY, 0, 0, $this->width, $this->height, $this->width, $this->height);

        $fontColor  = $this->__allocateColor($fontColor, $imageNew);
        foreach ($textArray as $text) {
            while (strlen($text) * imagefontwidth($font) > imagesx($imageNew)) {
	            if ($font > 1) {
	                $font--;
	            } else { 
	            	break; 
	            }
            }
            imagestring($imageNew, $font, imagesx($imageNew) / 2 - strlen($text) * imagefontwidth($font) / 2, $spaceT, $text, $fontColor);
            $spaceT += $space + $fontHeight;
        }

        if ($imageNew) {
            $this->width    = $widthNew;
            $this->height   = $heightNew;
            $this->resource = $imageNew;
            return $this;
        } else {
            throw new BitmapException(self::ErrorText);
        }
    }
    /**
     * Filter: GrayScale
     *
     * @access public
     * @return Image
     */
    public function grayScale() {
        if (!$this->resource) {
            throw new BitmapException(self::ErrorOpen);
        }

        if (imagefilter($this->resource, IMG_FILTER_GRAYSCALE)) {
            return $this;
        } else {
            throw new BitmapException(self::ErrorGrayScale);
        }
    }
    /**
     * Filter: Brightness
     *
     * @access public
     * @param integer $level
     * @return Image
     */
    public function brightness($level = 0) {
        if (!$this->resource) {
            throw new BitmapException(self::ErrorOpen);
        }

        $level = round((int)$level);
        $level = ($level < -255 ) ? -255 : (($level > 255) ? 255 : $level);

        if(imagefilter($this->resource, IMG_FILTER_BRIGHTNESS, $level)) {
            return $this;
        } else {
            throw new BitmapException(self::ErrorBrightness);
        }
    }
    /**
     * Filter: Negative
     *
     * @access public
     * @return Image
     */
    public function negative() {
        if (!$this->resource) {
            throw new BitmapException(self::ErrorOpen);
        }

        if (imagefilter($this->resource, IMG_FILTER_NEGATE)) {
            return $this;
        } else {
            throw new BitmapException(self::ErrorNegative);
        }
    }
    /**
     * Filter: Contrast
     *
     * @access public
     * @param integer $level
     * @return Image
     */
    public function contrast($level = 0) {
        if (!$this->resource) {
            throw new BitmapException(self::ErrorOpen);
        }

        $level = round((int)$level);
        $level = ($level < -100 ) ? -100 : (($level > 100) ? 100 : $level);

        if (imagefilter($this->resource, IMG_FILTER_CONTRAST, $level)) {
            return $this;
        } else {
            throw new BitmapException(self::ErrorContrast);
        }
    }

    /**
     * Filter: Colorize
     *
     * @access public
     * @param integer $red
     * @param integer $green
     * @param integer $blue
     * @return Image
     */
    public function colorize($red = 0, $green = 0, $blue = 0) {
        if (!$this->resource) {
			throw new BitmapException(self::ErrorOpen);
        }

        $red   = round((int)$red);
        $green = round((int)$green);
        $blue  = round((int)$blue);

        $red   = ($red  < -255 ) ? -255 : (($red  > 255) ? 255 : $red);
        $green = ($green  < -255 ) ? -255 : (($green  > 255) ? 255 : $green);
        $red   = ($blue  < -255 ) ? -255 : (($blue  > 255) ? 255 : $blue);

        if (imagefilter($this->resource, IMG_FILTER_COLORIZE, $red, $green, $blue)) {
            return $this;
        } else {
            throw new BitmapException(self::ErrorColorize);
        }
    }

    /**
     * Saves image to disk
     *
     * @access public
     * @param string|null $name
     * @param integer $quality
     * @return Image
     */
    public function save($dir = null, $name = null, $quality = 100) {
        if (!$this->resource) {
            throw new BitmapException(self::ErrorOpen);
        }

        if (!is_null($dir)) {
            $dirName  = ((strlen($dir)-1) == strrpos($dir, '/')) ? substr($dir, 0, strrpos($dir, '/')) : $dir;
        }

        if (is_null($name)) {
            if ($this->fileO) {
                $fileName = basename($this->fileO);
                $dirName  = (is_null($dir)) ? dirname($this->fileO) : $dirName;
            } else {
                $fileName = basename($this->file);
                $dirName  = (is_null($dir)) ? dirname($this->file) : $dirName;
            }
        } else {
            $fileName = basename($name);
            $dirName  = (is_null($dir)) ? dirname($this->file) : $dirName;

            if (substr_count($fileName, '*')) {
                $_fileName = basename(($this->fileO) ? $this->fileO : $this->file);
                $_fileName = substr($_fileName, 0, strrpos($_fileName, '.'));
                $fileName = str_replace('*', $_fileName, $fileName);
            }

            $nameExt = strrpos($fileName, '.');
            if ($nameExt == false || ($nameExt == true && (strtolower(substr($fileName, ($nameExt+1)) != $this->extension)))) {
                $fileName = $fileName . '.' . $this->extension;
            }
        }

        if (!file_exists($dirName)) {
            throw new BitmapException(self::DirNoExists);
        } elseif(!is_writable($dirName)) {
            throw new BitmapException(self::DirNoWritable);
        }

        switch ($this->type) {
        	case IMAGETYPE_JPEG:
	            $quality = round((int)$quality);
	            $quality = ($quality > 100) ? 100 : (($quality < 0) ? 0 : $quality);
	            if (imagejpeg($this->resource, $dirName . '/' . $fileName , $quality)) {
	                $this->file = $dirName . '/' . $fileName;
	                return $this;
	            } else {
	                throw new BitmapException(self::ErrorSave);
	            }
	      		break;
          	case IMAGETYPE_PNG:
	            if(imagepng($this->resource, $dirName . '/' . $fileName)) {
	                $this->file = $dirName . '/' . $fileName;
	                return $this;
	            } else {
	                throw new BitmapException(self::ErrorSave);
	            }
				break;

			case IMAGETYPE_GIF:
	            if(imagegif($this->resource, $dirName . '/' . $fileName)) {
	                $this->file = $dirName . '/' . $fileName;
	                return $this;
	            } else {
	                throw new BitmapException(self::ErrorSave);
	            }
				break;
        }
    }

    /**
     * Displays image
     *
     * @access public
     * @param integer $quality
     * @return void
     */
    public function display($quality = 100) {
        if (!$this->resource) {
            throw new BitmapException(self::ErrorOpen);
        }

        switch ($this->type) {
			case IMAGETYPE_JPEG:
	            $quality = round((int)$quality);
	            $quality = ($quality > 100) ? 100 : (($quality < 0) ? 0 : $quality);
	            header('Content-Type: image/jpeg');
	            imagejpeg($this->resource, null, $quality);
				break;
	
			case IMAGETYPE_PNG:
	            header('Content-Type: image/png');
	            imagepng($this->resource);
				break;
	
			case IMAGETYPE_GIF:
				header('Content-Type: image/gif');
				imagegif($this->resource);
				break;
        }
    }

    /**
     * Returns a color identifier representing the color composed of the given RGB components
     *
     * @access public
     * @param string $color
     * @param resource $image
     * @return integer
     */
    private function __allocateColor($color, $image) {
        $color = ($color{0} == '#') ? substr($color, 1) : $color;

        if ((strlen($color) != 3 && strlen($color) != 6) || ((strlen($color) == 3) && $color{0} != $color{1} && $color{0} != $color{2})) {
            throw new BitmapException(self::ErrorColor);
        }

        if (strlen($color) == 3) {
            $color .= $color;
        }

        $red   = hexdec(substr($color, 0, 2));
        $green = hexdec(substr($color, 2, 2));
        $blue  = hexdec(substr($color, 4, 2));

        return imagecolorallocate($image, $red, $green, $blue);
    }
    /**
     * Returns a array of image position
     *
     * @access public
     * @param integer $width
     * @param integer $height
     * @param integer $place
     * @param integer $fixedX
     * @param integer $fixedY
     * @return array
     */
    private function __getPosition($width, $height, $imageWidth, $imageHeight, $place, $fixedX, $fixedY) {
        $startX = 0;
        $startY = 0;

        switch ($place) {
            case self::FIXED:
                if(!isset($fixedX) || !isset($fixedY)) {
                    throw new BitmapException(self::ErrorFixed);
                }

                if($fixedX < 0) {
                    $fixedX = ($width + $fixedX) - $imageWidth;
                }

                if($fixedY < 0) {
                    $fixedY = ($height + $fixedY) - $imageHeight;
				}

                $startX = $fixedX;
                $startY = $fixedY;
            	break;

            case self::TOP + self::LEFT:
                $startX = 0;
                $startY = 0;
            	break;

            case self::TOP + self::CENTER:
                $startX = round(($width / 2) - ($imageWidth / 2));
                $startY = 0;
            	break;

            case self::TOP + self::RIGHT:
                $startX = $width - $imageWidth;
                $startY = 0;
            	break;

            case self::MIDDLE + self::LEFT:
                $startX = 0;
                $startY = round(($height / 2) - ($imageHeight / 2));
            	break;

            case self::CENTERED:
                $startX = round(($width / 2) - ($imageWidth / 2));
                $startY = round(($height / 2) - ($imageHeight / 2));
            	break;

            case self::MIDDLE + self::RIGHT:
                $startX = $width - $imageWidth;
                $startY = round(($height / 2) - ($imageHeight / 2));
            	break;

            case self::BOTTOM + self::LEFT:
                $startX = 0;
                $startY = $height - $imageHeight;
            	break;

            case self::BOTTOM + self::CENTER:
                $startX = round(($width / 2) - ($imageWidth / 2));
                $startY = $height - $imageHeight;
            	break;

            case self::BOTTOM + self::RIGHT:
                $startX = $width - $imageWidth;
                $startY = $height - $imageHeight;
            	break;
        }

        return array($startX, $startY);
    }
}

?>
