<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'pages', 'action' => 'home'));

/**
 * About Us page
 */
	Router::connect('/nosotros', array('controller' => 'pages', 'action' => 'about_us'));

/**
 * About us static pages
 */
	Router::connect('/mision-y-vision', array('controller' => 'pages', 'action' => 'vision'));

	Router::connect('/plantel/banos', array('controller' => 'pages', 'action' => 'bathrooms'));
	Router::connect('/plantel/canchas-deportivas', array('controller' => 'pages', 'action' => 'sports_courts'));
	Router::connect('/plantel/conservacion-e-industrializacion-de-alimentos', array('controller' => 'pages', 'action' => 'food_conservation'));
	Router::connect('/plantel/construyendo-jardin-tematico', array('controller' => 'pages', 'action' => 'theme_garden'));
	Router::connect('/plantel/cooperativa-escolar', array('controller' => 'pages', 'action' => 'school_shop'));
	Router::connect('/plantel/entrada', array('controller' => 'pages', 'action' => 'entrance'));
	Router::connect('/plantel/escudo-escolar', array('controller' => 'pages', 'action' => 'school_emblem'));
	Router::connect('/plantel/jardines-entrada', array('controller' => 'pages', 'action' => 'entry_garden'));
	Router::connect('/plantel/jardines-externos', array('controller' => 'pages', 'action' => 'outside_garden'));
	Router::connect('/plantel/jardines-y-salones', array('controller' => 'pages', 'action' => 'classrooms'));
	Router::connect('/plantel/personal-de-la-escuela', array('controller' => 'pages', 'action' => 'school_staff'));
	Router::connect('/plantel/plaza-civica-y-direccion', array('controller' => 'pages', 'action' => 'civic_plaza'));
	Router::connect('/plantel/prefectura-y-trabajo-social', array('controller' => 'pages', 'action' => 'social_work'));

	Router::connect('/servicio-social', array('controller' => 'pages', 'action' => 'social_service'));

	Router::connect('/tecnologías', array('controller' => 'pages', 'action' => 'technologies'));
	Router::connect('/tecnologías/agricultura', array('controller' => 'pages', 'action' => 'agriculture'));

	Router::connect('/sociedad-de-alumnos', array('controller' => 'pages', 'action' => 'student_society'));

/**
 * Projects static pages
 */
	Router::connect('/proyectos/refranes-y-valores', array('controller' => 'pages', 'action' => 'proverbs'));

	Router::connect('/planes-de-mejora/lectura-rapidez-fluidez-y-comprension', array('controller' => 'pages', 'action' => 'lecture'));
	Router::connect('/planes-de-mejora/sabucanes-viajeras', array('controller' => 'pages', 'action' => 'travel_bags'));

/**
 * News page
 */
	Router::connect('/eventos', array('controller' => 'events', 'action' => 'index'));
	Router::connect('/eventos/ver/*', array('controller' => 'events', 'action' => 'view'));

/**
 * Programs and Plans
 */
	Router::connect('/planes-y-programas', array('controller' => 'documents', 'action' => 'index'));
	Router::connect('/descargar/*', array('controller' => 'documents', 'action' => 'download'));


/**
 * News page
 */
	Router::connect('/noticias', array('controller' => 'news', 'action' => 'index'));
	Router::connect('/noticias/ver/*', array('controller' => 'news', 'action' => 'view'));

/**
 * Contact page
 */
	Router::connect('/contacto', array('controller' => 'pages', 'action' => 'contact'));


/**
 * Admin login
 */
	Router::connect('/admin', array('controller' => 'users', 'action' => 'login'));
	Router::connect('/logout', array('controller' => 'users', 'action' => 'logout'));
	Router::connect('/admin/inicio', array('controller' => 'pages', 'action' => 'home', 'admin' => true));

/**
 * Admin Users
 */
	Router::connect('/admin/usuarios', array('controller' => 'users', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/usuarios/nuevo', array('controller' => 'users', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/usuarios/ver/*', array('controller' => 'users', 'action' => 'view', 'admin' => true));
	Router::connect('/admin/usuarios/editar/*', array('controller' => 'users', 'action' => 'edit', 'admin' => true));

/**
 * Admin Main Images
 */
	Router::connect('/admin/imagenes-principales', array('controller' => 'main_images', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/imagenes-principales/nueva', array('controller' => 'main_images', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/imagenes-principales/editar/*', array('controller' => 'main_images', 'action' => 'edit', 'admin' => true));

/**
 * Admin Main Images
 */
	Router::connect('/admin/galeria-imagenes', array('controller' => 'default_images', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/galeria-imagenes/nueva/*', array('controller' => 'default_images', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/galeria-imagenes/editar/*', array('controller' => 'default_images', 'action' => 'edit', 'admin' => true));

/**
 * Admin News
 */
	Router::connect('/admin/noticias', array('controller' => 'news', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/noticias/nueva', array('controller' => 'news', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/noticias/ver/*', array('controller' => 'news', 'action' => 'view', 'admin' => true));
	Router::connect('/admin/noticias/editar/*', array('controller' => 'news', 'action' => 'edit', 'admin' => true));

/**
 * Admin Events
 */
	Router::connect('/admin/eventos', array('controller' => 'events', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/eventos/nuevo', array('controller' => 'events', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/eventos/ver/*', array('controller' => 'events', 'action' => 'view', 'admin' => true));
	Router::connect('/admin/eventos/editar/*', array('controller' => 'events', 'action' => 'edit', 'admin' => true));

/**
 * Admin Programs and Plans
 */
	Router::connect('/admin/planes-y-programas', array('controller' => 'documents', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/planes-y-programas/nuevo', array('controller' => 'documents', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/planes-y-programas/editar/*', array('controller' => 'documents', 'action' => 'edit', 'admin' => true));
	Router::connect('/admin/planes-y-programas/descargar/*', array('controller' => 'documents', 'action' => 'download', 'admin' => true));

/**
 * Admin Campus Members
 */
	Router::connect('/admin/plantel', array('controller' => 'campus_members', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/plantel/personal/nuevo', array('controller' => 'campus_members', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/plantel/personal/ver/*', array('controller' => 'campus_members', 'action' => 'view', 'admin' => true));
	Router::connect('/admin/plantel/personal/editar/*', array('controller' => 'campus_members', 'action' => 'edit', 'admin' => true));

/**
 * Admin Educational Plans
 */
	Router::connect('/admin/planes-educacionales', array('controller' => 'educational_plans', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/planes-educacionales/nuevo', array('controller' => 'educational_plans', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/planes-educacionales/ver/*', array('controller' => 'educational_plans', 'action' => 'view', 'admin' => true));
	Router::connect('/admin/planes-educacionales/editar/*', array('controller' => 'educational_plans', 'action' => 'edit', 'admin' => true));

/**
 * Admin Educational Programs
 */
	Router::connect('/admin/programas-educacionales', array('controller' => 'educational_programs', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/programas-educacionales/nuevo', array('controller' => 'educational_programs', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/programas-educacionales/ver/*', array('controller' => 'educational_programs', 'action' => 'view', 'admin' => true));
	Router::connect('/admin/programas-educacionales/editar/*', array('controller' => 'educational_programs', 'action' => 'edit', 'admin' => true));

/**
 * Admin Configuration
 */
	Router::connect('/admin/configuracion', array('controller' => 'configurations', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/configuracion/nueva', array('controller' => 'configurations', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/configuracion/editar/*', array('controller' => 'configurations', 'action' => 'edit', 'admin' => true));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
