CREATE SCHEMA IF NOT EXISTS `est39temozon` DEFAULT CHARACTER SET utf8 ;
USE `est39temozon` ;

-- -----------------------------------------------------
-- Table `est39temozon`.`campus_members`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `est39temozon`.`campus_members` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `first_name` VARCHAR(255) NOT NULL ,
  `last_name` VARCHAR(255) NOT NULL ,
  `role` VARCHAR(255) NOT NULL ,
  `task_description` TEXT NULL DEFAULT NULL ,
  `image` VARCHAR(255) NULL DEFAULT NULL ,
  `created` DATETIME NOT NULL ,
  `modified` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `est39temozon`.`educational_plans`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `est39temozon`.`educational_plans` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NOT NULL ,
  `description` TEXT NULL DEFAULT NULL ,
  `image` VARCHAR(255) NULL DEFAULT NULL ,
  `size` INT(11) NULL DEFAULT NULL ,
  `link` VARCHAR(255) NULL DEFAULT NULL ,
  `created` DATETIME NOT NULL ,
  `modified` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `est39temozon`.`educational_programs`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `est39temozon`.`educational_programs` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NOT NULL ,
  `description` TEXT NULL DEFAULT NULL ,
  `image` VARCHAR(255) NULL DEFAULT NULL ,
  `size` INT(11) NULL DEFAULT NULL ,
  `link` VARCHAR(255) NULL DEFAULT NULL ,
  `created` DATETIME NOT NULL ,
  `modified` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `est39temozon`.`projects`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `est39temozon`.`projects` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NOT NULL ,
  `description` TEXT NULL DEFAULT NULL ,
  `image` VARCHAR(255) NULL DEFAULT NULL ,
  `size` INT(11) NULL DEFAULT NULL ,
  `link` VARCHAR(255) NULL DEFAULT NULL ,
  `created` DATETIME NOT NULL ,
  `modified` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `est39temozon`.`improvement_plans`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `est39temozon`.`improvement_plans` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NOT NULL ,
  `description` TEXT NULL DEFAULT NULL ,
  `image` VARCHAR(255) NULL DEFAULT NULL ,
  `size` INT(11) NULL DEFAULT NULL ,
  `link` VARCHAR(255) NULL DEFAULT NULL ,
  `created` DATETIME NOT NULL ,
  `modified` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `est39temozon`.`events`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `est39temozon`.`events` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NOT NULL ,
  `description` TEXT NULL DEFAULT NULL ,
  `start_time` TIME NULL DEFAULT NULL ,
  `start_date` DATE NULL DEFAULT NULL ,
  `end_time` TIME NULL DEFAULT NULL ,
  `end_date` DATE NULL DEFAULT NULL ,
  `place` VARCHAR(255) NULL DEFAULT NULL ,
  `image` VARCHAR(255) NULL DEFAULT NULL ,
  `size` INT(11) NULL DEFAULT NULL ,
  `link` VARCHAR(255) NULL DEFAULT NULL ,
  `created` DATETIME NOT NULL ,
  `modified` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `est39temozon`.`comments`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `est39temozon`.`comments` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `model` VARCHAR(255) NOT NULL ,
  `author` VARCHAR(255) NULL DEFAULT NULL ,
  `content` TEXT NULL DEFAULT NULL ,
  `created` DATETIME NOT NULL ,
  `modified` DATETIME NOT NULL ,
  `educational_plans_id` INT(11) UNSIGNED NOT NULL ,
  `educational_programs_id` INT(11) UNSIGNED NOT NULL ,
  `projects_id` INT(11) UNSIGNED NOT NULL ,
  `improvement_plans_id` INT(11) UNSIGNED NOT NULL ,
  `events_id` INT(11) UNSIGNED NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `est39temozon`.`configurations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `est39temozon`.`configurations` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `key` VARCHAR(255) NOT NULL ,
  `value` VARCHAR(255) NOT NULL ,
  `name` VARCHAR(255) NULL DEFAULT NULL ,
  `icon` VARCHAR(255) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `est39temozon`.`news`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `est39temozon`.`news` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `title` VARCHAR(255) NOT NULL ,
  `description` TEXT NULL DEFAULT NULL ,
  `author` VARCHAR(255) NULL DEFAULT NULL ,
  `date` DATE NULL DEFAULT NULL ,
  `time` TIME NULL DEFAULT NULL ,
  `image` VARCHAR(255) NULL DEFAULT NULL ,
  `size` INT(11) NULL DEFAULT NULL ,
  `link` VARCHAR(255) NULL DEFAULT NULL ,
  `created` DATETIME NOT NULL ,
  `modified` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `est39temozon`.`plants`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `est39temozon`.`plants` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NOT NULL ,
  `description` TEXT NULL DEFAULT NULL ,
  `image` VARCHAR(255) NULL DEFAULT NULL ,
  `size` INT(11) NULL DEFAULT NULL ,
  `link` VARCHAR(255) NULL DEFAULT NULL ,
  `created` DATETIME NOT NULL ,
  `modified` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `est39temozon`.`technologies`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `est39temozon`.`technologies` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NOT NULL ,
  `description` TEXT NULL DEFAULT NULL ,
  `image` VARCHAR(255) NULL DEFAULT NULL ,
  `size` INT(11) NULL DEFAULT NULL ,
  `link` VARCHAR(255) NULL DEFAULT NULL ,
  `created` DATETIME NOT NULL ,
  `modified` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `est39temozon`.`default_images`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `est39temozon`.`default_images` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `file` VARCHAR(255) NOT NULL ,
  `model` VARCHAR(255) NOT NULL ,
  `description` TEXT NULL DEFAULT NULL ,
  `size` INT(11) NOT NULL ,
  `link` VARCHAR(255) NULL DEFAULT NULL ,
  `created` DATETIME NOT NULL ,
  `modified` DATETIME NOT NULL ,
  `news_id` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `event_id` INT(11) UNSIGNED NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `est39temozon`.`main_images`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `est39temozon`.`main_images` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NOT NULL ,
  `image` VARCHAR(255) NULL DEFAULT NULL ,
  `size` INT(11) NULL DEFAULT NULL ,
  `link` VARCHAR(255) NULL DEFAULT NULL ,
  `created` DATETIME NOT NULL ,
  `modified` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `est39temozon`.`users`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `est39temozon`. `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `role` enum('Super User','Administrator') NOT NULL DEFAULT 'Administrator',
  `active` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `email`, `password`, `first_name`, `last_name`, `image`, `role`, `active`, `created`, `modified`)
VALUES ('1', 'erickpeniche@gmail.com', 'af9558431ea5f40a25bd060254bd6360dd355b98', 'Erick', 'Peniche', NULL, 'Super User', '1', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

INSERT INTO `configurations` (`id`, `key`, `value`, `name`, `icon`)
VALUES
('1', 'contact-email', 'contacto@est39temozon.com', 'Correo(s) de contacto', 'envelope'),
('2', 'contact-address', 'Calle 5 B Diagonal 213 x 20 y 20 A Juan B. Sosa Chuburná', 'Dirección', 'road'),
('3', 'contact-phone', '(999)981-4900', 'Teléfono', 'phone'),
('4', 'website-title', '', 'Título', 'pencil'),
('5', 'website-keywords', '', 'Palabras clave', 'search'),
('6', 'website-description', '', 'Descripción', 'notes_2'),
('7', 'social-networks-twitter', '', 'Twitter', 'twitter'),
('8', 'social-networks-facebook', '', 'Facebook', 'facebook'),
('9', 'social-networks-youtube', '', 'YouTube', 'youtube');