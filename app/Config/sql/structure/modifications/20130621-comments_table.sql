CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model` varchar(255) NOT NULL,
  `author` varchar(255) DEFAULT NULL,
  `content` text,
  `educational_plan_id` int(11) DEFAULT NULL,
  `educational_program_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `improvement_plan_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8