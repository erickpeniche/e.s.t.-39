CREATE TABLE `default_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `description` text,
  `size` int(11) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `news_id` int(11) DEFAULT NULL,
  `plant_id` int(11) DEFAULT NULL,
  `technology_id` int(11) DEFAULT NULL,
  `educational_plan_id` int(11) DEFAULT NULL,
  `educational_program_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `improvement_plan_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8