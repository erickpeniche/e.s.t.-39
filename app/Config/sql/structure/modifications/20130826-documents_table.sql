DROP TABLE `educational_plans`;
DROP TABLE `educational_programs`;

CREATE TABLE `documents` (id INT(11) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT) DEFAULT CHARACTER SET `utf8`;

ALTER TABLE `documents` ADD `name` VARCHAR(255)  NOT NULL;
ALTER TABLE `documents` ADD `file` VARCHAR(255)  NOT NULL;
ALTER TABLE `documents` ADD `size` INT  NULL  DEFAULT NULL;
ALTER TABLE `documents` ADD `extension` VARCHAR(50)  NOT NULL;
ALTER TABLE `documents` ADD `type` enum('Plan','Program') NOT NULL DEFAULT 'Plan';
ALTER TABLE `documents` ADD `created` DATETIME  NULL;
ALTER TABLE `documents` ADD `modified` DATETIME  NULL;
